#!/bin/bash

if [[ $# -eq 0 || $# -gt 1 ]]
  then
    echo "Correct usage ./build.sh -d=<path-to-base-dir>"
    exit 1
fi

for i in "$@"
do
case $i in
    -d=*|--dir=*)
    homedir="${i#*=}"
    gesallcore=$homedir'/gesall-core'
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    
    shift # past argument with no value
    ;;
    *)
    echo "Correct usage ./build.sh -d=<path-to-base-dir>"
    exit 1 # unknown option
    ;;
esac
done

# echo 'test '$homedir
# echo 'test 1' $gesallcore
sed -i "s@HOMEDIR@$homedir@" $gesallcore/ant-build/build_clean.xml
sed -i "s@HOMEDIR@$homedir@" $gesallcore/ant-build/build_md.xml

ant -f $gesallcore/ant-build/build_clean.xml
ant -f $gesallcore/ant-build/build_md.xml

cd $gesallcore/build/build_alignment/
jar cvf $gesallcore/dist/program_aligment.jar ./

# replace back with place holder - so that in case of a mistake, 
# the same script can be run again
sed -i "s@$homedir@HOMEDIR@" $gesallcore/ant-build/build_clean.xml
sed -i "s@$homedir@HOMEDIR@" $gesallcore/ant-build/build_md.xml
