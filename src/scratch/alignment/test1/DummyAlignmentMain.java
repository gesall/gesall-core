package scratch.alignment.test1;

import hdfs.partition.logical.MapredLogicalTextInputFormat;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.lib.NullOutputFormat;

public class DummyAlignmentMain {

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(DummyAlignmentMain.class);
        conf.setJobName("dummy");

        conf.setOutputKeyClass(NullWritable.class);
        conf.setOutputValueClass(NullWritable.class);

        conf.setMapperClass(DummyAlignmentMapper.class);
        conf.setNumReduceTasks(0);
        conf.setInputFormat(MapredLogicalTextInputFormat.class);

        conf.setOutputFormat(NullOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
    }

}
