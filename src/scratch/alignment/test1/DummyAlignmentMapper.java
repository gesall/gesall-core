package scratch.alignment.test1;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class DummyAlignmentMapper extends MapReduceBase implements
        Mapper<LongWritable, Text, NullWritable, NullWritable> {

    static enum MapCounter {
        NUM_RECORDS, TIME_ELAPSED
    }

    private long t1      = 0;
    private long counter = 0;

    @Override
    public void configure(JobConf job) {
        t1 = System.currentTimeMillis();
    }

    @Override
    public void map(LongWritable key, Text val,
            OutputCollector<NullWritable, NullWritable> output,
            Reporter reporter) throws IOException {
        String s = val.toString();
        s.length();
        ++counter;
        reporter.incrCounter(MapCounter.NUM_RECORDS, 1);
    }

    @Override
    public void close() throws IOException {
        long elapsedTime = System.currentTimeMillis() - t1;
        System.out.println("Mapper Details");
        System.out.println("Lines read: " + counter);
        System.out.println("Records read: " + (counter / 4));

        System.out.println("Time elapsed (in ms): " + elapsedTime);
        System.out.println("Time elapsed (in s): " + (elapsedTime / 1000.0));

        System.out.println("Records/s: "
                + ((counter / 4) / (elapsedTime / 1000.0)));

    }

}
