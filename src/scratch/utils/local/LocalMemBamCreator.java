package scratch.utils.local;

import hdfs.MemoryBamManager;
import htsjdk.samtools.BAMHeaderEncoder;
import htsjdk.samtools.BAMRecordEncoder;
import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.BlockCompressedOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Hex;

import com.google.common.primitives.Bytes;

@SuppressWarnings("deprecation")
public class LocalMemBamCreator {

    public void run(String inputFile) throws IOException {

        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(inputFile));

        // 1. extract header
        SAMFileHeader header = reader.getFileHeader();

        // 2. extract binary records
        SAMRecordIterator it = reader.iterator();
        SAMRecord record1 = it.next();
        System.out.println("Start pos: " + record1.getAlignmentStart());
        SAMRecord record2 = it.next();
        System.out.println("Start pos: " + record2.getAlignmentStart());

        int recordLengthEstimate = 300;
        BAMRecordEncoder encoder = new BAMRecordEncoder(record1,
                recordLengthEstimate);
        byte[] bytes1 = encoder.encode();
        encoder = new BAMRecordEncoder(record2, recordLengthEstimate);
        byte[] bytes2 = encoder.encode();
        System.out.println(bytes1.length + ", " + bytes2.length);
        // System.out.println(new String(bytes1));
        // System.out.println(new String(bytes2));

        // 3. combine to create bam file
        BAMHeaderEncoder headerEncoder = new BAMHeaderEncoder(header);
        // System.out.println(header);
        byte[] header_bytes = headerEncoder.encode();

        System.out.println("new bam header");
        System.out.println(header_bytes.length);
        // System.out.println( Hex.encodeHexString( header_bytes ) );

        // System.out.println(new String(header_bytes));

        // append record bytes

        byte[] allBytes = Bytes.concat(header_bytes, bytes1, bytes2);
        // System.out.println("All Bytes");
        // System.out.println(allBytes.length);
        // System.out.println(new String(allBytes));
        // System.out.println( Hex.encodeHexString( allBytes ) );

        // System.out.println(new String(allBytes));

        ByteArrayOutputStream baos = new ByteArrayOutputStream(allBytes.length);
        BlockCompressedOutputStream bcos = new BlockCompressedOutputStream(
                baos, null, 0);
        bcos.write(allBytes);
        bcos.close();
        byte[] binaryAllBytes = baos.toByteArray();

        InputStream is = new ByteArrayInputStream(binaryAllBytes);

        // SAMFileReader reader2 = new SAMFileReader(is);
        SamReader reader2 = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(is));

        // SAMFileHeader header2 = reader2.getFileHeader();
        // System.out.println(header2);

        for (SAMRecord record : reader2) {
            System.out.println("1");
            System.out.println("Start pos: " + record.getAlignmentStart());
        }
        reader2.close();
        System.out.println("End");

    }

    public void run2(String bamFileName) throws IOException {
        
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(bamFileName));

        // 1. extract header
        SAMFileHeader header = reader.getFileHeader();
        BAMHeaderEncoder headerEncoder = new BAMHeaderEncoder(header);
        byte[] headerBytes = headerEncoder.encode();
        MemoryBamManager memBam = new MemoryBamManager(headerBytes);

        
        // 2. extract binary records
        SAMRecordIterator it = reader.iterator();
        SAMRecord record1 = it.next();
        SAMRecord record2 = it.next();

        int recordLengthEstimate = 300;
        BAMRecordEncoder encoder = new BAMRecordEncoder(record1, recordLengthEstimate);
        byte[] bytes1 = encoder.encode();
        encoder = new BAMRecordEncoder(record2, recordLengthEstimate);
        byte[] bytes2 = encoder.encode();
        System.out.println(bytes1.length + ", "+bytes2.length);
        memBam.addRecord(bytes1);
        memBam.addRecord(bytes2);
        
        SAMFileReader memBamReader = memBam.getSamFileReader();
        for (SAMRecord record : memBamReader) {
            System.out.println("Bam record start "+record.getAlignmentStart());
        }
        memBam.clear();
        
        
    }

    public static void main(String[] args) throws IOException {
        String bamFileName = "/Users/aroy/workspace/gesall-rad/data/na12878_vsmall_nb.bam";
        LocalMemBamCreator local = new LocalMemBamCreator();
        local.run2(bamFileName);
    }

}
