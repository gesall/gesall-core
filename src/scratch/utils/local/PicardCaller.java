package scratch.utils.local;

import picard.sam.AddOrReplaceReadGroups;


public class PicardCaller {
    
    public PicardCaller() {
    }
    
    public void runAddRepl() {
        /*
        INPUT=/state/partition2/aroy/wf_runs_ea/1/Data/full.aligned.bam
        OUTPUT=/state/partition2/aroy/wf_runs_ea/1/Data/full.rg.bam
        VALIDATION_STRINGENCY=LENIENT
        RGID=gesall
        RGLB=gesall
        RGPU=dummy
        RGPL=ILLUMINA
        RGSM=full
        RGCN=GDL
        */
        String[] argv = new String[9];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "VALIDATION_STRINGENCY=LENIENT";
        argv[3] = "RGID=gesall";
        argv[4] = "RGLB=gesall";
        argv[5] = "RGPU=dummy";
        argv[6] = "RGPL=ILLUMINA";
        argv[7] = "RGSM=full";
        argv[8] = "RGCN=GDL";

        System.out.println(argv);
        
        new AddOrReplaceReadGroups().instanceMainWithoutExit(argv, null, null);
    }

    public static void main(String[] args) {
        PicardCaller pc = new PicardCaller();
        pc.runAddRepl();
    }
}
