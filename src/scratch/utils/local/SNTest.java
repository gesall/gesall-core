package scratch.utils.local;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SNTest {

    public SNTest() {
    }

    public void run() {
        String out = executeCommand("rm perm*");
        System.out.println(out);
        out = executeCommand("cat new.txt >> perm.txt");
        System.out.println(out);
        out = executeCommand("rm new.txt");
        System.out.println(out);
        
        System.out.println("exit2");
    }

    private String executeCommand(String command) {
        
        
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(new String[] { "sh", "-c", command });
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    public static void main(String[] args) {
        SNTest sn = new SNTest();
        sn.run();
    }

}
