package scratch.utils.local;

import java.io.File;
import java.io.IOException;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

/**
 * A simple rewrite of BAM file. The included writer is supposed to ensure that
 * the SAM records do not span two BAM blocks.
 * 
 * TODO: Create another option to write directly to HDFS.
 */
public class BamFileRewriter {

    private String inputFile;
    private String outputFile;

    long           counter = 0;

    public BamFileRewriter(String inputFile, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    public void rewrite() throws IOException {
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(inputFile));

        SAMFileHeader header = reader.getFileHeader();

        SAMFileWriter writer = new SAMFileWriterFactory().makeBAMWriter(header,
                true, new File(outputFile));

        System.out.println("Started...");
        for (SAMRecord record : reader) {
            ++counter;
            if (counter % 1000000 == 0) {
                System.out.println(counter + " records processed.");
            }
            writer.addAlignment(record);
        }
        writer.close();
        reader.close();
        System.out.println("Finished.");

    }

    public static void main(String[] args) throws IOException {
        /*
         * BamFileRewriter rewriter = new BamFileRewriter(
         * "/Users/aroy/workspace/gesall-rad/data/na12878_vsmall2.bam",
         * "/Users/aroy/workspace/gesall-rad/data/na12878_vsmall_nb.bam");
         */
        String inputFile = null;
        String outputFile = null;
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        try {
            options = parser.parse(args);
            inputFile = (String) options.valueOf("i");
            outputFile = (String) options.valueOf("o");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        BamFileRewriter rewriter = new BamFileRewriter(inputFile, outputFile);
        rewriter.rewrite();
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("i:o:h*");
        parser.accepts("i", "Input BAM file created by external program")
                .withRequiredArg().required().ofType(String.class);
        parser.accepts("o",
                "Output BAM file with no records overlapping compressed blocks")
                .withRequiredArg().required().ofType(String.class);
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

}
