package scratch.utils.local.counter;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;

public class LocalDupExtractor {

    String inputName;
    String outputName;

    public LocalDupExtractor(String inputFilename, String outputFilename) {
        this.inputName = inputFilename;
        this.outputName = outputFilename;
    }

    public void extractDuplicates() throws IOException {
        long total = 0;
        long duplicates = 0;
        long unmapped = 0;
        long dupAndUnmapped = 0;

        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(inputName));

        SAMFileHeader header = reader.getFileHeader();
        SAMFileWriter writer = new SAMFileWriterFactory().makeBAMWriter(header,
                true, new File(outputName));

        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            SAMRecord record = it.next();
            ++total;

            if (record.getDuplicateReadFlag()) {
                ++duplicates;
                writer.addAlignment(record);
                if (record.getReadUnmappedFlag()) {
                    ++dupAndUnmapped;
                }
            }

            if (record.getReadUnmappedFlag()) {
                ++unmapped;
            }

            if (total % 1000000 == 0) {
                System.out.println(total);
            }
        }
        reader.close();
        writer.close();

        System.out.println();
        System.out.println(inputName + " Statistics");
        System.out.println("Total reads: " + total);
        System.out.println("Unmapped reads: " + unmapped);
        System.out.println("Duplicate reads: " + duplicates);
        System.out.println("Duplicate and unmapped: " + dupAndUnmapped);
        System.out.println("Duplicate reads in new file: " + duplicates);
        System.out.println();

    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("args[0] = input filename");
            System.out.println("args[1] = duplicate output filename");
            return;
        }
        String fileName = args[0]; // args[0];
        String outputFile = args[1];
        LocalDupExtractor lde = new LocalDupExtractor(fileName, outputFile);
        lde.extractDuplicates();

    }

}
