package scratch.utils.local.counter;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;

public class LocalDupCounter {

    String fileName;

    public LocalDupCounter(String fileName) {
        this.fileName = fileName;
    }

    public void countDuplicates() throws IOException {
        long total = 0;
        long duplicates = 0;
        long unmapped = 0;
        long dupAndUnmapped = 0;
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(fileName));
        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            SAMRecord record = it.next();
            ++total;

            if (record.getDuplicateReadFlag()) {
                ++duplicates;
                if (record.getReadUnmappedFlag()) {
                    ++dupAndUnmapped;
                }
            }

            if (record.getReadUnmappedFlag()) {
                ++unmapped;
            }

//            if (total % 1000000 == 0) {
//                System.out.println(total);
//            }
        }
        reader.close();

        System.out.println();
        System.out.println(fileName + " Statistics");
        System.out.println("Total reads: " + total);
        System.out.println("Unmapped reads: " + unmapped);
        System.out.println("Duplicate reads: " + duplicates);
        System.out.println("Duplicate and unmapped: " + dupAndUnmapped);
        System.out.println();

    }

    public static void main(String[] args) throws IOException {
        for (String arg : args) {
            String fileName = arg; // args[0];
            LocalDupCounter ldc = new LocalDupCounter(fileName);
            ldc.countDuplicates();
        }
    }

}
