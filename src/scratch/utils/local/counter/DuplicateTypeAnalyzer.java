package scratch.utils.local.counter;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;

public class DuplicateTypeAnalyzer {

    String fileName;

    public DuplicateTypeAnalyzer(String fileName) {
        this.fileName = fileName;
    }

    public void analyzeDuplicates() throws IOException {
        long total = 0;
        long duplicates = 0;
        long unmapped = 0;
        long mateUnmapped = 0;
        long secOrSupp = 0;
        long paired = 0;
        long first = 0;
        long positive = 0;

        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(fileName));
        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            SAMRecord record = it.next();
            ++total;
            if (!record.getDuplicateReadFlag()) {
                continue;
            }
            ++duplicates;

            if (record.getReadUnmappedFlag()) {
                ++unmapped;
            }

            if (record.isSecondaryOrSupplementary()) {
                ++secOrSupp;
            }

            if (record.getReadPairedFlag()) {
                ++paired;
            }

            if (record.getMateUnmappedFlag()) {
                ++mateUnmapped;
            }

            if (record.getFirstOfPairFlag()) {
                ++first;
            }

            if (!record.getReadNegativeStrandFlag()) {
                ++positive;
            }

        }
        reader.close();

        System.out.println();
        System.out.println(fileName + " Statistics");
        System.out.println("Total reads: " + total);
        System.out.println("Duplicate reads: " + duplicates);
        System.out.println("Duplicate and unmapped: " + unmapped);
        System.out.println("Duplicate and sec/supp: " + secOrSupp);
        System.out.println("Duplicate and paired: " + paired);
        System.out.println("Duplicate and mate unmapped: " + mateUnmapped);

        System.out.println("Duplicate and first of pair: " + first);
        System.out.println("Duplicate and on positive strand: " + positive);

        System.out.println();
    }

    public static void main(String[] args) throws IOException {
        for (String arg : args) {
            String fileName = arg; // args[0];
            DuplicateTypeAnalyzer dta = new DuplicateTypeAnalyzer(fileName);
            dta.analyzeDuplicates();
        }
    }

}
