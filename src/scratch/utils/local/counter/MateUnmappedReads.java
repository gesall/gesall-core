package scratch.utils.local.counter;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;

public class MateUnmappedReads {

    String fileName;

    public MateUnmappedReads(String fileName) {
        this.fileName = fileName;
    }

    private void countReads() throws IOException {
        long partialMatch = 0;
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(fileName));
        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            SAMRecord record = it.next();
            if ((!record.getReadUnmappedFlag())
                    && (record.getMateUnmappedFlag())) {
                ++partialMatch;
            }
        }
        reader.close();
        System.out.println("Partial matches\t" + partialMatch);
    }

    public static void main(String[] args) throws IOException {
        for (String arg : args) {
            String fileName = arg; // args[0];
            MateUnmappedReads mur = new MateUnmappedReads(fileName);
            mur.countReads();
        }
    }

}
