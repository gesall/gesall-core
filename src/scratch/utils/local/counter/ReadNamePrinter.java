package scratch.utils.local.counter;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;

public class ReadNamePrinter {

    String inputName;

    public ReadNamePrinter(String inputFilename) {
        this.inputName = inputFilename;
    }

    public void printNames() throws IOException {
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(inputName));
        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            SAMRecord record = it.next();
            System.out.println(record.getReadName());
        }
        reader.close();
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("args[0] = input filename");
            return;
        }
        String fileName = args[0];
        ReadNamePrinter rne = new ReadNamePrinter(fileName);
        rne.printNames();
    }

}
