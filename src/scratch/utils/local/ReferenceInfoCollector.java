package scratch.utils.local;

import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReferenceInfoCollector {
    
    String filename;
    
    public ReferenceInfoCollector(String filename) {
        this.filename = filename;
    }
    
    public void run() {
        Map<Integer, String> map = new LinkedHashMap<Integer, String>();
        
        SAMFileReader reader = new SAMFileReader(new File(filename));
        for (SAMRecord record: reader) {
            Integer index = record.getReferenceIndex();
            String name = record.getReferenceName();
            if (!map.containsKey(index)) 
                map.put(index, name);
        }
        reader.close();
    
        for (Integer index : map.keySet()) {
            System.out.println(index + " -> " + map.get(index));
        }
        
    }
    
    
    public static void main(String[] args) {
        ReferenceInfoCollector ric = new ReferenceInfoCollector(args[0]);
        ric.run();
    }
    
}
