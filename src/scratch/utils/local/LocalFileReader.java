package scratch.utils.local;

import hdfs.config.CmdLineArgs;
import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import picard.sam.AddOrReplaceReadGroups;
import picard.sam.FixMateInformation;

@SuppressWarnings("deprecation")
public class LocalFileReader {

    private String bamFileName;

    public LocalFileReader(String bamFileName) {
        this.bamFileName = bamFileName;
    }

    public void readFile2() throws IOException {
        String[] argv = CmdLineArgs.getFixMateArgv();
        SAMFileReader samReaderInput1 = new SAMFileReader(new File(
                "/Users/aroy/workspace/gesall-rad/data/na12878_vvsmall.bam"));
        ByteArrayOutputStream bamWriterInput1 = new ByteArrayOutputStream();

        new FixMateInformation().instanceMainWithoutExit(argv, samReaderInput1,
                bamWriterInput1);

        bamWriterInput1.flush();
        byte[] bytes = bamWriterInput1.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        long counter = 0;
        SAMFileReader reader = new SAMFileReader(inputStream);
        String header = reader.getFileHeader().getTextHeader();
        System.out.println(header);
        for (SAMRecord record : reader) {
            ++counter;
            record.getAlignmentStart();
        }

        System.out.println("Records: " + counter);
    }

    public void readFile() throws IOException {
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(bamFileName));
        long counter = 0;
        SAMRecordIterator it = reader.iterator();
        while (it.hasNext()) {
            it.next();
            ++counter;
        }
        /*
         * for (SAMRecord record : reader) { record.getAlignmentStart();
         * ++counter; }
         */
        System.out.println(counter);
        reader.close();
    }

    public void readFile3() throws IOException {
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(new File(bamFileName));
        long counter = 0;
        SAMRecordIterator it = reader.iterator();
        
        SAMFileHeader header = reader.getFileHeader();
        SAMSequenceRecord seq = header.getSequence("X");
        System.out.println(seq.getSequenceLength());
        System.out.println(seq);
        System.out.println(reader.getFileHeader().getSequence("X").getSequenceLength()
);
        System.out.println(reader.getFileHeader().getTextHeader());
        
        if (1==1) {
        while (it.hasNext()) {
            SAMRecord record = it.next();
            if (record.getReadUnmappedFlag()) {
                System.out.println("Reference string:\t"+ record.getReferenceName());
                System.out.println("Reference index:\t"+ record.getReferenceIndex());
                System.out.println("Alignment start:\t"+ record.getAlignmentStart());
                System.out.println("Unclipped start:\t"+ record.getUnclippedStart());
                System.out.println("\n");

                ++counter;
                
            }
            if (counter > 10) {
                break;
            }
        }
        }
        /*
         * for (SAMRecord record : reader) { record.getAlignmentStart();
         * ++counter; }
         */
        System.out.println(counter);
        reader.close();
    }


    public static void main(String[] args) throws IOException {
        String bamFileName = "/Users/aroy/workspace/gesall-rad/data/na12878_vsmall_nb.bam";
        LocalFileReader reader = new LocalFileReader(bamFileName);
        reader.readFile3();
    }

}
