package scratch.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesTest {

    String filename = "config.properties";
    
    public PropertiesTest() {
    
    }
    
    public void create() throws IOException {
        Properties props = new Properties();
        props.setProperty("system.path", "/state/New\\ Documents");
        props.setProperty("system.threads", "6");
        FileOutputStream out = new FileOutputStream(filename);
        props.store(out, "System Properties");
        out.close();
    }
    
    public void read() throws IOException {
        Properties props = new Properties();
        FileInputStream in = new FileInputStream(filename);
        props.load(in);
        in.close();
        for (String key : props.stringPropertyNames()) {
            System.out.println(key + ": " + props.getProperty(key));
        }
        /*
        System.out.println(props.getProperty("local.fs.temp.dir"));
        System.out.println(props.getProperty("addrepl.RGID"));
        System.out.println(props.getProperty("absent"));
        */
    }

    public void update() {
        
    }
    
    public static void main(String[] args) throws IOException {
        PropertiesTest pt = new PropertiesTest();
        //pt.create();
        pt.read();
    }
    
}
