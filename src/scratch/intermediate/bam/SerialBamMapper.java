package scratch.intermediate.bam;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.BAMRecordEncoder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class SerialBamMapper extends
        Mapper<LongWritable, BytesWritable, IntWritable, BytesWritable> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_SAM_RECORDS
    }

    static enum ProfileEnums {
        SERIAL_TIME, MAPPER_TIME, BYTES_RECORD_SIZE
    }

    private static final Log      log = LogFactory
                                              .getLog(SerialBamMapper.class);

    private BamRecordEnumeration  samEnum;
    private String                headerFileName;
    private List<BamBlockWrapper> bamBlocks;
    Stopwatch                     serialTime;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samEnum = new BamRecordEnumeration(context.getConfiguration(),
                headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
        serialTime = Stopwatch.createUnstarted();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {

        Stopwatch timer = Stopwatch.createStarted();

        SAMFileReader reader = samEnum.getSAMFileReader(bamBlocks);

        long counter = 0;
        double runningSize = 0;
        int recordLengthEstimate = 300;

        for (SAMRecord record : reader) {
            ++counter;

            int key = record.hashCode();

            serialTime.start();
            BAMRecordEncoder encoder = new BAMRecordEncoder(record, recordLengthEstimate);
            byte[] bytes1 = encoder.encode();
            serialTime.stop();

            runningSize = runningSize
                    + ((bytes1.length - runningSize) / (double) counter);

            context.write(new IntWritable(key), new BytesWritable(bytes1));
            context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
        }
        reader.close();
        timer.stop();

        LogPrinter.printSecondsElaped(log, "SerializationTime",
                serialTime.elapsed(TimeUnit.SECONDS));
        LogPrinter.printSecondsElaped(log, "MapperTime",
                timer.elapsed(TimeUnit.SECONDS));
        LogPrinter
                .printDoubleValue(log, "BytesRecordSize", (long) runningSize);

        context.getCounter(ProfileEnums.SERIAL_TIME).setValue(
                serialTime.elapsed(TimeUnit.SECONDS));
        context.getCounter(ProfileEnums.MAPPER_TIME).setValue(
                timer.elapsed(TimeUnit.SECONDS));
        context.getCounter(ProfileEnums.BYTES_RECORD_SIZE).setValue(
                (long) runningSize);

    }

}