package scratch.intermediate.bam;

import hdfs.MemoryBamManager;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class SerialBamReducer extends
        Reducer<IntWritable, BytesWritable, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SHUFFLED_SAM_RECORDS, OUTPUT_SAM_RECORDS
    }

    static enum ProfileEnums {
        REDUCER_PROGRAM_TIME
    }

    String                   headerFileName;
    MemoryBamManager memoryBam;
    
    // records to hold in memory
    int                      recordsInMemory;
    final int                MAX_RECORDS_MEMORY = 100000;

    Stopwatch                programTimer;
    private static final Log log                = LogFactory
                                                        .getLog(SerialBamReducer.class);

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        memoryBam = new MemoryBamManager(conf, headerFileName, MAX_RECORDS_MEMORY);
        recordsInMemory = 0;
        programTimer = Stopwatch.createStarted();

    }

    @Override
    public void reduce(IntWritable key, Iterable<BytesWritable> values, Context context)
            throws IOException, InterruptedException {
        for (BytesWritable value : values) {
            byte[] record = value.copyBytes();
            memoryBam.addRecord(record);
            
            ++recordsInMemory;
            context.getCounter(ReducerRecords.SHUFFLED_SAM_RECORDS)
                    .increment(1);
        }
        if (recordsInMemory > MAX_RECORDS_MEMORY) {
            doWork(context);
        }
    }

    private void doWork(Context context) throws IOException {
        SAMFileReader input = memoryBam.getSamFileReader();
        for (SAMRecord record : input) {
            record.getAlignmentStart();
            context.getCounter(ReducerRecords.OUTPUT_SAM_RECORDS).increment(1);
        }
        memoryBam.clear();
        recordsInMemory = 0;
    }

    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (recordsInMemory > 0) {
            doWork(context);
        }

        programTimer.stop();
        LogPrinter.printSecondsElaped(log, "ReducerProgramTime",
                programTimer.elapsed(TimeUnit.SECONDS));
        context.getCounter(ProfileEnums.REDUCER_PROGRAM_TIME).setValue(
                programTimer.elapsed(TimeUnit.SECONDS));

    }

}