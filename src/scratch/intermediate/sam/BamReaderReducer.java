package scratch.intermediate.sam;

import hdfs.SamInputStream;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

// Imp: Copy by value

public class BamReaderReducer extends
        Reducer<Text, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SAM_RECORDS, PAIRS
    }

    String         headerFileName;
    SamInputStream samFile;

    int            count;
    final int      countMax = 100;
    StringBuilder  samRecords;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samFile = new SamInputStream(conf, headerFileName);
        count = 0;
        samRecords = new StringBuilder();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException {
        context.getCounter(ReducerRecords.PAIRS).increment(1);
        for (Text value : values) {
            String samRecord = new String(value.toString());
            samRecords.append(samRecord);
            ++count;
        }
        if (count > countMax) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException {
        doWork(context);
    }

    private void doWork(Context context) throws IOException {
        samFile.add(samRecords);
        SamReader reader = samFile.getSamReader();
        for (SAMRecord record : reader) {
            record.getAlignmentStart();
            context.getCounter(ReducerRecords.SAM_RECORDS).increment(1);
        }
        cleanUp();
    }

    private void cleanUp() {
        samFile.clear();
        count = 0;
        samRecords = new StringBuilder();
    }
}
