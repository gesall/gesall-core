package scratch.intermediate.sam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import htsjdk.samtools.SAMRecord;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class BamReaderMapper extends
        Mapper<LongWritable, BytesWritable, Text, Text> {

    static enum MapperRecords {
        BAM_RECORDS
    }

    BamRecordEnumeration  samEnum;
    String                headerFileName;
    List<BamBlockWrapper> bamBlocks;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samEnum = new BamRecordEnumeration(conf, headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    @Override
    public void cleanup(Context context) throws IOException, InterruptedException {
        samEnum.initializeEnumeration(bamBlocks);
        while (samEnum.hasMoreElements()) {
            SAMRecord record = samEnum.nextElement();
            context.getCounter(MapperRecords.BAM_RECORDS).increment(1);
            context.write(new Text(record.getReadName()), new Text(record.getSAMString()));
        }
        samEnum.close();
    }

}
