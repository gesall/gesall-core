package scratch.intermediate.sam;

import hdfs.BamInputFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class BamReaderMain {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("BamInput", args[0]);
        /*
         * conf.setBoolean("mapreduce.map.output.compress", true);
         * conf.setBoolean("mapreduce.output.fileoutputformat.compress", false);
         * conf.set("mapred.map.output.compress.codec",
         * "org.apache.hadoop.io.compress.SnappyCodec");
         */
        Job job = Job.getInstance(conf);
        job.setJobName("Cleaning test");
        job.setJarByClass(BamReaderMain.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setInputFormatClass(BamInputFormat.class);
        job.setMapperClass(BamReaderMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setReducerClass(BamReaderReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(15);

        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }

}
