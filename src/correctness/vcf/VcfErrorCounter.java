package correctness.vcf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import correctness.LogisticWeights;

// cat merged_single_parallel.txt | cut -f8 -d$'\t' | sort | uniq -c
public class VcfErrorCounter {

    public final int setColumn = 7;
    public final int mqIndex   = 4;

    LogisticWeights  lw;
    String           filename;

    public VcfErrorCounter(String aFilename) {
        filename = aFilename;
        lw = new LogisticWeights();
        lw.setSnpMappingQuality();
    }

    double error               = 0;
    double errorWeighted       = 0;

    double totalSingle         = 0;
    double totalSingleWeighted = 0;

    public void run() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String header = reader.readLine();
        String[] tokens = header.split("\t");
        System.out.println("Set column: " + tokens[setColumn]);
        System.out.println("MQ column: " + tokens[mqIndex]);

        String line;
        while ((line = reader.readLine()) != null) {
            String[] values = line.split("\t");
            //
            String mqStr = values[mqIndex];
            double mq = -1;
            if (!(mqStr.equalsIgnoreCase("NA") || mqStr.equalsIgnoreCase("NaN"))) {
                mq = Double.parseDouble(mqStr);
            } else {
                mq = 40; // XXX lowest will be mapped to 0
            }

            //if (mq < 40) continue; // XXX
            
            if (values[setColumn].equalsIgnoreCase("Intersection")
                    || values[setColumn].equalsIgnoreCase("single")) {
                totalSingle += 1;
                totalSingleWeighted += weightFunction(mq);
            }

            if (values[setColumn].equalsIgnoreCase("single")
                    || values[setColumn].equalsIgnoreCase("parallel")) {
                error += 1;
                errorWeighted += weightFunction(mq);
            }

        }
        reader.close();
        print();
    }

    public void print() {
        System.out.println("Absolute Error: " + error);
        System.out.println("Weighted Error: " + errorWeighted);
        System.out.println("Total (single): " + totalSingle);
        System.out.println("Weighted Total (single): " + totalSingleWeighted);
        System.out
                .println("Absolute Error %: " + ((error / totalSingle) * 100));
        System.out.println("Weighted Error %: "
                + ((errorWeighted / totalSingle) * 100));

    }

    public static void main(String[] args) throws IOException {
        String filename = "/Users/aroy/workspace/gesall-rad/data/merged23.txt";
        VcfErrorCounter counter = new VcfErrorCounter(filename);
        counter.run();
    }

    public double weightFunction(double mappingQuality) {
        if (mappingQuality == 255) {
            mappingQuality = 0;
        }
        return lw.weight(mappingQuality);
    }

    public double weightFunction2(double mappingQuality) {
        if (mappingQuality == 255) {
            mappingQuality = 0;
        }

        if (mappingQuality <= 40) {
            return 0;
        } else if (mappingQuality > 40 && mappingQuality < 55) {
            double weight = Math
                    .min(1, (mappingQuality / 15.0) - (40.0 / 15.0));
            return Math.max(0, weight);
        } else {
            return 1;
        }

    }

}
