package correctness.bamdiff;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class BamDiffValue implements WritableComparable<BamDiffValue> {
    IntWritable   nodeSource;
    Text record;

    public BamDiffValue() {
        this.nodeSource = new IntWritable();
        this.record = new Text();
    }

    public BamDiffValue(IntWritable node_src, Text record) {
        this.nodeSource = node_src;
        this.record = record;
    }

    public int getNodeSrc() {
        return nodeSource.get();
    }

    public Text getRecord() {
        return record;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        nodeSource.readFields(in);
        record.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        nodeSource.write(out);
        record.write(out);
    }

    @Override
    public int compareTo(BamDiffValue other) {
        int cmp = nodeSource.compareTo(other.nodeSource);
        if (cmp == 0) {
            cmp = record.compareTo(other.record);
        }
        return cmp;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BamDiffValue) {
            BamDiffValue other = (BamDiffValue) o;
            return nodeSource.equals(other.nodeSource)
                    && record.equals(other.record);
        }
        return false;
    }

}
