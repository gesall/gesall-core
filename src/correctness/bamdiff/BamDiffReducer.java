package correctness.bamdiff;

import hdfs.MemoryBamManager;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMLineParser;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class BamDiffReducer extends
        Reducer<Text, BamDiffValue, NullWritable, NullWritable> {

    private static final Log log = LogFactory.getLog(BamDiffReducer.class);

    static enum ReducerRecords {
        PROCESSED_SAM_RECORDS, PROCESSED_BASE_SAM_RECORDS, PROCESSED_OTHER_SAM_RECORDS, DUP_BASE, DUP_OTHER

    }

    SAMLineParser     baseParser;
    SAMLineParser     otherParser;
    BamDiffComparator diff;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();

        SAMFileHeader baseHeader = MemoryBamManager.getHeader(conf,
                conf.get("BaseBamInput"));
        SAMFileHeader otherHeader = MemoryBamManager.getHeader(conf,
                conf.get("OtherBamInput"));

        baseParser = new SAMLineParser(baseHeader);
        otherParser = new SAMLineParser(otherHeader);
        diff = new BamDiffComparator();
    }

    @Override
    // key is the read name
    // values contain records with the same read name
    public void reduce(Text key, Iterable<BamDiffValue> values, Context context)
            throws IOException, InterruptedException {
        List<SAMRecord> baseList = new ArrayList<SAMRecord>();
        List<SAMRecord> otherList = new ArrayList<SAMRecord>();
        for (BamDiffValue value : values) {
            context.getCounter(ReducerRecords.PROCESSED_SAM_RECORDS).increment(
                    1);
            int node_src = value.getNodeSrc();
            String samString = value.getRecord().toString();
            if (node_src == BamDiffResources.BASE_NODE_SRC) {
                SAMRecord samRecord = baseParser.parseLine(samString);
                baseList.add(samRecord);
                context.getCounter(ReducerRecords.PROCESSED_BASE_SAM_RECORDS)
                        .increment(1);
                if (samRecord.getDuplicateReadFlag()) {
                    context.getCounter(ReducerRecords.DUP_BASE).increment(1);
                }

            } else {
                SAMRecord samRecord = otherParser.parseLine(samString);
                otherList.add(samRecord);
                context.getCounter(ReducerRecords.PROCESSED_OTHER_SAM_RECORDS)
                        .increment(1);
                if (samRecord.getDuplicateReadFlag()) {
                    context.getCounter(ReducerRecords.DUP_OTHER).increment(1);
                }

            }
        }
        diff.compare(baseList, otherList);

    }

    public void cleanup(Context context) throws IOException,
            InterruptedException {

        LogPrinter.printDoubleValue(log, "TotalRecords",
                diff.totalRecordsInBase + diff.totalRecordsInOther);
        LogPrinter.printDoubleValue(log, "TotalRecordsWeighted",
                diff.totalRecordsInBaseWeighted
                        + diff.totalRecordsInOtherWeighted);

        LogPrinter.printDoubleValue(log, "TotalRecordsInBase",
                diff.totalRecordsInBase);
        LogPrinter.printDoubleValue(log, "TotalRecordsInBaseWeighted",
                diff.totalRecordsInBaseWeighted);

        LogPrinter.printDoubleValue(log, "ReadError", diff.readError);
        LogPrinter.printDoubleValue(log, "ReadErrorWeighted",
                diff.readErrorWeighted);

        LogPrinter.printDoubleValue(log, "DupError", diff.dupError);
        LogPrinter.printDoubleValue(log, "DupErrorWeighted",
                diff.dupErrorWeighted);

        LogPrinter.printDoubleValue(log, "DupErrorSignificant",
                diff.dupErrorFiltered);
        LogPrinter.printDoubleValue(log, "DupErrorSignificantWeighted",
                diff.dupErrorWeightedFiltered);

        LogPrinter.printDoubleValue(log, "DupPairsDifference",
                diff.dupPairsDifference);

    }

}