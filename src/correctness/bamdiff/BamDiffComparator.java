package correctness.bamdiff;

import htsjdk.samtools.SAMRecord;

import java.util.List;

import correctness.LogisticWeights;

public class BamDiffComparator {

    // error counter
    double          readError                   = 0;
    double          readErrorWeighted           = 0;

    // double baseNotInOther = 0;
    // double baseNotInOtherWeighted = 0;
    // double otherNotInBase = 0;
    // double otherNotInBaseWeighted = 0;

    double          totalRecordsInBase          = 0;
    double          totalRecordsInOther         = 0;

    double          totalRecordsInBaseWeighted  = 0;
    double          totalRecordsInOtherWeighted = 0;

    double          dupError                    = 0;
    double          dupErrorWeighted            = 0;

    double          dupErrorFiltered            = 0;
    double          dupErrorWeightedFiltered    = 0;

    double          dupPairsDifference = 0;
    // double dupsNotInOther = 0;
    // double dupsNotInOtherWeighted = 0;
    // double dupsNotInBase = 0;
    // double dupsNotInBaseWeighted = 0;

    LogisticWeights lw;

    public BamDiffComparator() {
        lw = new LogisticWeights();
        lw.setReadMappingQuality();
    }

    private boolean areReadsSame(SAMRecord base, SAMRecord other) {
        if (base.getReadUnmappedFlag() && other.getReadUnmappedFlag()) {
            return true;
        }

        if (base.getMappingQuality() == 0 || base.getMappingQuality() == 255) {
            return true;
        }
        if (other.getMappingQuality() == 0 || other.getMappingQuality() == 255) {
            return true;
        }
        if (base.getAlignmentStart() == other.getAlignmentStart()) {
            return true;
        }
        return false;
    }

    public void compare(List<SAMRecord> baseList, List<SAMRecord> otherList) {
        
        int baseDups1 = 0;
        for(SAMRecord baseRead : baseList) {
            if (baseRead.getDuplicateReadFlag()) {
                ++baseDups1;
            }
        }
        int otherDups1 = 0;
        for (SAMRecord otherRead : otherList) {
            if (otherRead.getDuplicateReadFlag()) {
                ++otherDups1;
            }
        }
        if (baseDups1 != otherDups1) {
            ++dupPairsDifference;
        }
        
        
        boolean[] baseEquality = new boolean[baseList.size()];
        boolean[] otherEquality = new boolean[otherList.size()];
        boolean[][] equalityMatrix = new boolean[baseList.size()][otherList
                .size()];

        // find equal reads
        for (int i = 0; i < baseList.size(); i++) {
            for (int j = 0; j < otherList.size(); j++) {
                boolean areEqual = areReadsSame(baseList.get(i),
                        otherList.get(j));
                if (areEqual) {
                    equalityMatrix[i][j] = true;
                    baseEquality[i] = true;
                    otherEquality[j] = true;
                }
            }
        }

        // missing reads
        for (int i = 0; i < baseEquality.length; i++) {
            if (!baseEquality[i]) {
                readError++;
                readErrorWeighted += (weightFunction(baseList.get(i)
                        .getMappingQuality()));
            }
        }

        // new reads
        for (int j = 0; j < otherEquality.length; j++) {
            if (!otherEquality[j]) {
                readError++;
                readErrorWeighted += (weightFunction(otherList.get(j)
                        .getMappingQuality()));
            }
        }

        // total records
        totalRecordsInBase += baseList.size();
        totalRecordsInOther += otherList.size();
        for (int i = 0; i < baseEquality.length; i++) {
            totalRecordsInBaseWeighted += (weightFunction(baseList.get(i)
                    .getMappingQuality()));
        }
        for (int j = 0; j < otherEquality.length; j++) {
            totalRecordsInOtherWeighted += (weightFunction(otherList.get(j)
                    .getMappingQuality()));
        }

        // XXX Duplicates
        
        double baseDups = 0;
        double weightedBaseDups = 0;
        
        double baseDupsFiltered = 0;
        double weightedBaseDupsFiltered = 0;

        for (int i = 0; i < baseEquality.length; i++) {
            
            
            if (baseList.get(i).getDuplicateReadFlag()) {
                baseDups += 1;
                weightedBaseDups += (weightFunction(baseList.get(i)
                        .getMappingQuality()));
                if (baseList.get(i).getMappingQuality() != 0 && baseList.get(i).getMappingQuality() != 255) {
                    baseDupsFiltered += 1;
                    weightedBaseDupsFiltered += (weightFunction(baseList.get(i)
                            .getMappingQuality()));
                }

            }
        }

        double otherDups = 0;
        double weightedOtherDups = 0;
        
        double otherDupsFiltered = 0;
        double weightedOtherDupsFiltered = 0;

        for (int j = 0; j < otherEquality.length; j++) {
            if (otherList.get(j).getDuplicateReadFlag()) {
                otherDups += 1;
                weightedOtherDups += (weightFunction(otherList.get(j)
                        .getMappingQuality()));
                if (otherList.get(j).getMappingQuality() != 0 && otherList.get(j).getMappingQuality() != 255) {
                    otherDupsFiltered += 1;
                    weightedOtherDupsFiltered += (weightFunction(otherList.get(j)
                            .getMappingQuality()));
                }
                
            }
        }

        dupError += Math.abs(baseDups - otherDups);
        dupErrorWeighted += Math.abs(weightedBaseDups - weightedOtherDups);

        dupErrorFiltered += Math.abs(baseDupsFiltered - otherDupsFiltered);
        dupErrorWeightedFiltered += Math.abs(weightedBaseDupsFiltered - weightedOtherDupsFiltered);

        
        /*
         * for (int i = 0; i < baseEquality.length; i++) { if
         * (baseList.get(i).getMappingQuality() == 0 ||
         * baseList.get(i).getMappingQuality() == 255) { continue; } if
         * (baseList.get(i).getDuplicateReadFlag()) { boolean equalDupExists =
         * false; for (int j = 0; j < otherList.size(); j++) { if
         * (equalityMatrix[i][j] && otherList.get(j).getDuplicateReadFlag()) {
         * equalDupExists = true; break; } } if (!equalDupExists) { dupError +=
         * 1; dupErrorWeighted += (weightFunction(baseList.get(i)
         * .getMappingQuality())); } } }
         * 
         * for (int j = 0; j < otherEquality.length; j++) { if
         * (otherList.get(j).getMappingQuality() == 0 ||
         * otherList.get(j).getMappingQuality() == 255) { continue; } if
         * (otherList.get(j).getDuplicateReadFlag()) { boolean equalDupExists =
         * false; for (int i = 0; i < baseList.size(); i++) { if
         * (equalityMatrix[i][j] && baseList.get(i).getDuplicateReadFlag()) {
         * equalDupExists = true; break; } } if (!equalDupExists) { dupError +=
         * 1; dupErrorWeighted += (weightFunction(otherList.get(j)
         * .getMappingQuality())); } } }
         */

    }

    public double weightFunction(int mappingQuality) {
        return lw.weight(mappingQuality);
    }

    private double weightFunction2(int mappingQuality) {
        if (mappingQuality == 255) {
            mappingQuality = 0;
        }

        if (mappingQuality <= 30) {
            return 0;
        } else if (mappingQuality > 30 && mappingQuality < 55) {
            double weight = Math.min(1, (mappingQuality / 25.0) - (6.0 / 5.0));
            return Math.max(0, weight);
        } else {
            return 1;
        }

    }

}
