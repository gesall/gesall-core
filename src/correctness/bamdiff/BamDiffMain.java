package correctness.bamdiff;

import hdfs.BamInputFormat;
import hdfs.utils.BamPathFilter;
import hdfs.utils.FSMethods;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class BamDiffMain extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String inputPath1 = null;
        String inputPath2 = null;
        int numReducers = -1;
        String outputPath = null;
        try {
            options = parser.parse(args);
            inputPath1 = (String) options.valueOf("i");
            inputPath2 = (String) options.valueOf("j");
            numReducers = (Integer) options.valueOf("r");
            outputPath = (String) options.valueOf("o");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        // Set configuration parameters
        Configuration conf = getConf();
        // PropertyReader properties = new PropertyReader(conf);
        // properties.setProperties(propertyFile);

        String bamFile1 = FSMethods.getAnyBamFile(conf, inputPath1);
        conf.set("BaseBamInput", bamFile1);

        String bamFile2 = FSMethods.getAnyBamFile(conf, inputPath2);
        conf.set("OtherBamInput", bamFile2);

        conf.set("OutputDir", outputPath);
        conf.set("mapreduce.job.user.classpath.first", "true");

        // Set job parameters
        Job job = Job.getInstance(conf);

        job.setJobName("Bam Diff");
        job.setJarByClass(BamDiffMain.class);

        FileSystem fs = FileSystem.get(conf);
        FileStatus[] statuses = fs.listStatus(new Path(inputPath1),
                new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }
        statuses = fs.listStatus(new Path(inputPath2), new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }

        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setInputFormatClass(BamInputFormat.class);
        job.setMapperClass(BamDiffMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(BamDiffValue.class);

        job.setReducerClass(BamDiffReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);

        job.setNumReduceTasks(numReducers);

        return job.waitForCompletion(true) ? 0 : 1;

    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("i:j:o:r:h*");
        parser.accepts("i",
                "Base (single node, single in file path) input BAM directory on HDFS")
                .withRequiredArg().required().ofType(String.class);
        parser.accepts("j",
                "Other (single not in file path) input BAM directory on HDFS")
                .withRequiredArg().required().ofType(String.class);
        parser.accepts("r", "Number of reducers").withRequiredArg().required()
                .ofType(Integer.class);
        parser.accepts("o", "Output directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new BamDiffMain(), args);
        System.exit(res);
    }

}
