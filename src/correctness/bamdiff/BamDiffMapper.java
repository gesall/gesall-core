package correctness.bamdiff;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

@SuppressWarnings("deprecation")
public class BamDiffMapper extends
        Mapper<LongWritable, BytesWritable, Text, BamDiffValue> {

    static enum MapperRecords {
        SHUFFLED_SAM_RECORDS, SHUFFLED_BASE_SAM_RECORDS, SHUFFLED_OTHER_SAM_RECORDS
    }

    private static final Log      log      = LogFactory
                                                   .getLog(BamDiffMapper.class);

    private BamRecordEnumeration  samEnum;
    private String                headerFileName;
    private List<BamBlockWrapper> bamBlocks;

    int                           node_src = -1;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        FileSplit fileSplit = (FileSplit) context.getInputSplit();
        String filePath = fileSplit.getPath().toUri().toString();

        if (filePath.contains(BamDiffResources.SINGLE_NODE_DIR_ID)) {
            node_src = BamDiffResources.BASE_NODE_SRC;
            headerFileName = conf.get("BaseBamInput");
        } else {
            node_src = BamDiffResources.OTHER_NODE_SRC;
            headerFileName = conf.get("OtherBamInput");
        }

        samEnum = new BamRecordEnumeration(context.getConfiguration(),
                headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {

        // FileSplit fileSplit = (FileSplit) context.getInputSplit();
        // String filePath = fileSplit.getPath().toUri().toString();

        // Set a global variable to check if we are processing single node
        // output file or not.
        // Compare the file name of the split to the single node filename
        // provided by the user, if they are same then we are processing single
        // node output file.
        // fileName.contains(null);

        // Keep collecting individual bam blocks in the map input split
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {

        // Read over the records in all the collected bam blocks
        SAMFileReader reader = samEnum.getSAMFileReader(bamBlocks);

        long counter = 0;
        // int recordLengthEstimate = 300;

        for (SAMRecord record : reader) {
            //if (counter > 10) {
            //    break;
            //}
            ++counter;
            // key is read name
            String key = record.getReadName();
            // TODO: Set a tag value in record depending on whether the record
            // is from a single node output file or not
            // value is the BAM record
            // BAMRecordEncoder encoder = new BAMRecordEncoder(record,
            // recordLengthEstimate);
            // byte[] bytes1 = encoder.encode();
            String samString = record.getSAMString();

            BamDiffValue value = new BamDiffValue(new IntWritable(node_src),
                    new Text(samString));

            context.write(new Text(key), value);
            context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
            
            if (node_src == BamDiffResources.BASE_NODE_SRC) {
                context.getCounter(MapperRecords.SHUFFLED_BASE_SAM_RECORDS).increment(1);
            } else {
                context.getCounter(MapperRecords.SHUFFLED_OTHER_SAM_RECORDS).increment(1);
            }
            
        }
        reader.close();
        LogPrinter.printDoubleValue(log, "MapRecords", counter);

    }

}