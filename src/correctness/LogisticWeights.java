package correctness;

public class LogisticWeights {
    double A;
    double K;
    double B;
    double Q;
    double v;
    double C;
    double M;

    public void setSnpMappingQuality() {
        A = 0;
        K = 1;
        B = 0.68;
        Q = 1.5;
        v = 0.5;
        C = 1;
        M = 45;
    }

    public void setReadMappingQuality() {
        A = 0;
        K = 1;
        B = 0.45;
        Q = 1.5;
        v = 0.5;
        C = 1;
        M = 36;
    }

    public double weight(double mq) {
        return y_func(mq);
    }

    private double y_func(double x) {
        double deno_term2 = Q * Math.pow(Math.E, -B * (x - M));
        double deno_term1 = C;
        double deno = Math.pow(deno_term1 + deno_term2, 1.0 / v);
        double num = K - A;
        double y = A + (num / deno);
        y = Math.max(0, y);
        y = Math.min(1, y);
        return y;
    }

    public static void main(String[] args) {
        LogisticWeights lw = new LogisticWeights();
        lw.setReadMappingQuality();
        System.out.println(lw.weight(30));
        System.out.println(lw.weight(35));
        System.out.println(lw.weight(40));
        System.out.println(lw.weight(45));
        System.out.println(lw.weight(55));
        System.out.println(lw.weight(60));
        
        System.out.println("\nSNP\n");
        lw.setSnpMappingQuality();
        System.out.println(lw.weight(30));
        System.out.println(lw.weight(35));
        System.out.println(lw.weight(40));
        System.out.println(lw.weight(45));
        System.out.println(lw.weight(55));
        System.out.println(lw.weight(60));

        
    }

}
