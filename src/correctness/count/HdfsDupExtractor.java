package correctness.count;

import hdfs.utils.BamPathFilter;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

@SuppressWarnings("deprecation")
public class HdfsDupExtractor {

    String dirName;
    String outputName;

    public HdfsDupExtractor(String dirName, String outputName) {
        this.dirName = dirName;
        this.outputName = outputName;
    }

    public void extractDuplicates() throws IOException, URISyntaxException {
        Configuration conf = new Configuration();

        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        conf.set("fs.file.impl",
                org.apache.hadoop.fs.LocalFileSystem.class.getName());

        conf.addResource(new Path(
                "/share/apps/hadoop-2.5.2/etc/hadoop/core-site.xml"));
        conf.addResource(new Path(
                "/share/apps/hadoop-2.5.2/etc/hadoop/hdfs-site.xml"));
        FileSystem fs = FileSystem.get(conf);

        FileStatus[] fileStatuses = fs.listStatus(new Path(dirName),
                new BamPathFilter(conf));

        long totalReads = 0;
        long totalDuplicates = 0;
        long totalDupAndUnmapped = 0;
        long totalUnmapped = 0;

        SAMFileWriter writer = null;

        long t1 = System.currentTimeMillis();
        for (FileStatus fSt : fileStatuses) {

            FSDataInputStream in = fs.open(fSt.getPath());
            SAMFileReader reader = new SAMFileReader(in);

            if (writer == null) {
                SAMFileHeader header = reader.getFileHeader();
                writer = new SAMFileWriterFactory().makeBAMWriter(header, true,
                        new File(outputName));
            }

            long localReads = 0;
            long localDuplicates = 0;
            long localDupAndUnmapped = 0;
            long localUnmapped = 0;
            for (SAMRecord record : reader) {
                ++localReads;
                if (record.getDuplicateReadFlag()) {
                    ++localDuplicates;
                    writer.addAlignment(record);

                    if (record.getReadUnmappedFlag()) {
                        ++localDupAndUnmapped;
                    }
                }

                if (record.getReadUnmappedFlag()) {
                    ++localUnmapped;
                }

            }

            reader.close();

            System.out.println(fSt.getPath().getName() + " Statistics");
            System.out.println("Local reads: " + localReads);
            System.out.println("Local unmapped reads: " + localUnmapped);
            System.out.println("Local duplicate reads: " + localDuplicates);
            System.out.println("Local duplicate and unmapped: "
                    + localDupAndUnmapped);
            System.out.println("Local duplicate reads in new file: "
                    + localDuplicates);

            totalReads += localReads;
            totalDuplicates += localDuplicates;
            totalDupAndUnmapped += localDupAndUnmapped;
            totalUnmapped += localUnmapped;
        }

        writer.close();

        System.out.println();
        System.out.println(dirName + " Statistics");
        System.out.println("Total reads: " + totalReads);
        System.out.println("Unmapped reads: " + totalUnmapped);
        System.out.println("Duplicate reads: " + totalDuplicates);
        System.out.println("Duplicate and unmapped: " + totalDupAndUnmapped);
        System.out.println("Duplicate reads in new file: " + totalDuplicates);

        System.out.println();

        fs.close();

        System.out
                .println("Time elapsed: " + (System.currentTimeMillis() - t1));
    }

    public static void main(String[] args) throws IOException,
            URISyntaxException {
        if (args.length != 2) {
            System.out.println("args[0] = HDFS BAM directory");
            System.out.println("args[1] = Output local BAM file");
        }
        String dirName = args[0];
        String outputName = args[1];
        HdfsDupExtractor hde = new HdfsDupExtractor(dirName, outputName);
        hde.extractDuplicates();

    }
}
