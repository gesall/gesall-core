package correctness.count;

import hdfs.utils.BamPathFilter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

@SuppressWarnings("deprecation")
public class HdfsDupCounter {

    String dirName;

    public HdfsDupCounter(String dirName) {
        this.dirName = dirName;
    }

    public void countDuplicates() throws IOException, URISyntaxException {
        Configuration conf = new Configuration();

        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        conf.set("fs.file.impl",
                org.apache.hadoop.fs.LocalFileSystem.class.getName());

        conf.addResource(new Path(
                "/share/apps/hadoop-2.5.2/etc/hadoop/core-site.xml"));
        conf.addResource(new Path(
                "/share/apps/hadoop-2.5.2/etc/hadoop/hdfs-site.xml"));
        FileSystem fs = FileSystem.get(conf);
        // FileSystem fs = new DistributedFileSystem();
        // fs.initialize(new URI("hdfs://yeeha.yeeha:9000/user/aroy/"), conf);

        FileStatus[] fileStatuses = fs.listStatus(new Path(dirName),
                new BamPathFilter(conf));

        long totalReads = 0;
        long totalDuplicates = 0;
        long totalDupAndUnmapped = 0;
        long totalUnmapped = 0;

        long t1 = System.currentTimeMillis();
        for (FileStatus fSt : fileStatuses) {

            FSDataInputStream in = fs.open(fSt.getPath());
            SAMFileReader reader = new SAMFileReader(in);

            long localReads = 0;
            long localDuplicates = 0;
            long localDupAndUnmapped = 0;
            long localUnmapped = 0;
            for (SAMRecord record : reader) {
                ++localReads;
                if (record.getDuplicateReadFlag()) {
                    ++localDuplicates;
                    if (record.getReadUnmappedFlag()) {
                        ++localDupAndUnmapped;
                    }
                }

                if (record.getReadUnmappedFlag()) {
                    ++localUnmapped;
                }

            }

            reader.close();

            System.out.println(fSt.getPath().getName() + " Statistics");
            System.out.println("Local reads: " + localReads);
            System.out.println("Local unmapped reads: " + localUnmapped);
            System.out.println("Local duplicate reads: " + localDuplicates);
            System.out.println("Local duplicate and unmapped: "
                    + localDupAndUnmapped);

            totalReads += localReads;
            totalDuplicates += localDuplicates;
            totalDupAndUnmapped += localDupAndUnmapped;
            totalUnmapped += localUnmapped;
        }

        System.out.println();
        System.out.println(dirName + " Statistics");
        System.out.println("Total reads: " + totalReads);
        System.out.println("Unmapped reads: " + totalUnmapped);
        System.out.println("Duplicate reads: " + totalDuplicates);
        System.out.println("Duplicate and unmapped: " + totalDupAndUnmapped);
        System.out.println();

        fs.close();

        System.out
                .println("Time elapsed: " + (System.currentTimeMillis() - t1));
    }

    public static void main(String[] args) throws IOException,
            URISyntaxException {
        for (String arg : args) {
            String dirName = arg; // args[0];
            HdfsDupCounter hdc = new HdfsDupCounter(dirName);
            hdc.countDuplicates();
        }
    }
}
