package program.alignment.latest;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.streaming.io.IdentifierResolver;
import org.apache.hadoop.streaming.io.TextInputWriter;

/**
 * <p>
 * IdentifierResolver that knows about "justbytes".
 * </p>
 * 
 * <p>
 * To use justbytes, give the following options to streaming:
 * 
 * <pre>
 * -D stream.io.identifier.resolver.class=org.apache.hadoop.streaming.io.JustBytesIdentifierResolver
 * -io justbytes
 * -inputformat org.apache.hadoop.mapred.JustBytesInputFormat
 * -outputformat org.apache.hadoop.mapred.JustBytesOutputFormat
 * </pre>
 * 
 * </p>
 * 
 * <p>
 * <tt>-io justbytes</tt> sets the following:
 * 
 * <ul>
 * <li>Input writer class:
 * {@link org.apache.hadoop.streaming.io.JustBytesInputWriter}</li>
 * <li>Output writer class:
 * {@link org.apache.hadoop.streaming.io.JustBytesOutputReader}</li>
 * <li>Output key class: {@link JustBytesWritable}</li>
 * <li>Output value class: {@link NullWritable} (nothing written)</li>
 * </ul>
 * </p>
 * 
 */
public class JustBytesIdentifierResolver extends IdentifierResolver {

    /** Identifier for a key of unmodified, unaugmented bytes and no value. */
    public static final String JUST_BYTES_ID = "justbytes";

    @Override
    public void resolve(String identifier) {
        if (identifier.equalsIgnoreCase(JUST_BYTES_ID)) {
            setInputWriterClass(TextInputWriter.class);
            setOutputReaderClass(JustBytesOutputReader.class);
            setOutputKeyClass(JustBytesWritable.class);
            setOutputValueClass(NullWritable.class);
        } else {
            super.resolve(identifier);
        }
    }
}
