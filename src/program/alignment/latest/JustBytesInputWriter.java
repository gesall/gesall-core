/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package program.alignment.latest;

import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.streaming.PipeMapRed;
import org.apache.hadoop.streaming.io.InputWriter;

/**
 * InputWriter that writes unmodified, unaugmented input bytes as key, and no
 * value.
 */
public class JustBytesInputWriter extends
    InputWriter<JustBytesWritable, NullWritable> {

  private DataOutput output;

  @Override
  public void initialize(PipeMapRed pipeMapRed) throws IOException {
    super.initialize(pipeMapRed);
    output = pipeMapRed.getClientOutput();
  }

  @Override
  public void writeKey(JustBytesWritable key) throws IOException {
    output.write(key.getBytes(), 0, key.getLength());
  }

  @Override
  public void writeValue(NullWritable value) throws IOException {
    // No-op
  }
}
