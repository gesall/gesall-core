package program.alignment.latest;

import java.io.DataInput;
import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.streaming.PipeMapRed;
import org.apache.hadoop.streaming.io.OutputReader;

/**
 * OutputReader that reads unmodified, unaugmented bytes from the client as key,
 * and no value.
 */
public class JustBytesOutputReader extends
        OutputReader<JustBytesWritable, NullWritable> {
    private DataInput         input;
    private JustBytesWritable key;

    @Override
    public void initialize(PipeMapRed pipeMapRed) throws IOException {
        super.initialize(pipeMapRed);
        input = pipeMapRed.getClientInput();
        key = new JustBytesWritable();
    }

    @Override
    public boolean readKeyValue() throws IOException {
        key.readFields(input);
        return key.getLength() > 0; // Should only be zero on EOF
    }

    @Override
    public JustBytesWritable getCurrentKey() throws IOException {
        return key;
    }

    @Override
    public NullWritable getCurrentValue() throws IOException {
        return NullWritable.get();
    }

    @Override
    public String getLastOutput() {
        throw new UnsupportedOperationException(
                "Use new String(bytes, encoding) to make a String from bytes");
    }
}
