package program.alignment.preprocess.fastq;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/*
 * External java dependencies: 
 * JOpt Simple 4.8
 * Apache Commons Logging 
 */
public class FqInterleaver {

    String                   reads1;
    String                   reads2;
    String                   mergedReads;
    int                      splits;

    private static final Log log = LogFactory.getLog(FqInterleaver.class);

    public FqInterleaver(String reads1, String reads2, String mergedReads,
            int splits) {
        this.reads1 = reads1;
        this.reads2 = reads2;
        this.mergedReads = mergedReads;
        this.splits = splits;
    }

    public void run() throws IOException {
        rwRecords();
    }

    // TODO: Add constant for FASTQ record length = 4
    private void rwRecords() throws IOException {
        // lines in each split
        long totalLines = 2 * getLineCount(reads1);
        long maxSplitLines = getSplitLineCount(totalLines, splits);

        // open fastq files
        BufferedReader file1 = new BufferedReader(new FileReader(reads1));
        BufferedReader file2 = new BufferedReader(new FileReader(reads2));

        // setup writer
        int splitCounter = 0;
        long currSplitLines = 0;
        BufferedWriter writer = null;

        String line1;
        while ((line1 = file1.readLine()) != null) {
            if (writer == null) {
                writer = getNextWriter(splitCounter);
            }
            // fastq 1
            writer.write(line1 + "\n");
            for (int i = 0; i < 3; ++i) {
                line1 = file1.readLine();
                writer.write(line1 + "\n");
            }
            // fastq 2
            for (int i = 0; i < 4; ++i) {
                String line2 = file2.readLine();
                writer.write(line2 + "\n");
            }

            currSplitLines += 8;
            if (currSplitLines == maxSplitLines) {
                writer.close();
                log.info("Split " + splitCounter + "has "
                        + getPrintString(currSplitLines) + ".");
                splitCounter += 1;
                currSplitLines = 0;
                writer = null;
            }
        }

        if (writer != null) {
            writer.close();
            log.info("Split " + splitCounter + "has "
                    + getPrintString(currSplitLines) + ".");
        }
        file2.close();
        file1.close();
    }

    private BufferedWriter getNextWriter(int splitCounter) throws IOException {
        String newFileName = this.mergedReads + "-" + splitCounter
                + ".in.fastq";
        BufferedWriter writer = new BufferedWriter(new FileWriter(newFileName));
        return writer;
    }

    private long getLineCount(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        long totalLines = 0;
        while (reader.readLine() != null) {
            ++totalLines;
        }
        log.info("Each input file has " + getPrintString(totalLines) + ".");
        reader.close();
        return totalLines;
    }

    /*
     * Returns line count for each split, ensuring that line count is divisible
     * by record size. (split line count * number of splits) can be greater than
     * total line count.
     */
    private long getSplitLineCount(long lines, int splits) {
        int recordSize = 8;
        long maxSplitLines = recordSize
                * ((long) Math.ceil(lines / (splits * recordSize * 1.0)));
        log.info("Each split (except maybe last) will have "
                + getPrintString(maxSplitLines) + ".");
        return maxSplitLines;
    }

    private String getPrintString(long lines) {
        return lines + " lines/" + (lines / 4) + " records";
    }

    public static void main(String[] args) throws IOException {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String reads1 = null;
        String reads2 = null;
        String mergedReads = null;
        int splits = -1;
        try {
            options = parser.parse(args);
            reads1 = (String) options.valueOf("i");
            reads2 = (String) options.valueOf("j");
            mergedReads = (String) options.valueOf("o");
            splits = (Integer) options.valueOf("n");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        FqInterleaver worker = new FqInterleaver(reads1, reads2, mergedReads,
                splits);
        worker.run();
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("i:j:o:n:h*");
        parser.accepts("i", "Input FASTQ1 on local disk").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("j", "Input FASTQ2 on local disk").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("o", "Output interleaved FASTQ on local disk")
                .withRequiredArg().required().ofType(String.class);
        parser.accepts("n", "Number of desired splits of interleaved FASTQ")
                .withRequiredArg().required().ofType(Integer.class);
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

}
