package program.md.preprocess;

import hdfs.bloom.GenomicPositionKey;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import com.google.common.hash.BloomFilter;

@SuppressWarnings("deprecation")
public class AlignSplitMapper extends
        Mapper<Text, Text, NullWritable, NullWritable> {

    final long         MAX_RECORDS_FILE = 5000000;
    FSDataOutputStream bamOutputStream;
    SAMFileWriter      bamWriter;
    int                fileCount        = 0;

    private String     bamFilePath;

    BloomFilter<GenomicPositionKey> bloomFilter;
    
    static enum MapperRecords {
        BAM_RECORDS_READ, BAM_RECORDS_WRITTEN, BLOOM_INSERTS
    }

    @Override
    public void setup(Context context) throws IOException {
        int expectedInsertions = 25*1000000; //(20 * 1000000) / context.getNumReduceTasks();
        bloomFilter = BloomFilter.create(new GenomicPositionKey(),
                expectedInsertions);
    }
    
    @Override
    public void map(Text key, Text value, Context context) {
        this.bamFilePath = value.toString();
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
        flushBloomFilter(context);
    }

    public void doWork(Context context) throws IOException,
            InterruptedException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path path = new Path(bamFilePath);
        FSDataInputStream in = fs.open(path);
        SAMFileReader reader = new SAMFileReader(in);
        SAMFileHeader header = reader.getFileHeader();
        PeekingIterator<SAMRecord> it = Iterators.peekingIterator(reader
                .iterator());
        long counter = 0;
        if (bamWriter == null) {
            changeWriter(context, header);
        }
        while (it.hasNext()) {
            SAMRecord current = it.next();
            
            writeToBloomFilter(current, context);
            
            context.getCounter(MapperRecords.BAM_RECORDS_READ).increment(1);
            bamWriter.addAlignment(current);
            context.getCounter(MapperRecords.BAM_RECORDS_WRITTEN).increment(1);
            ++counter;
            if (counter > MAX_RECORDS_FILE) {
                String currentName = current.getReadName();
                SAMRecord next = it.peek();
                String nextName = next.getReadName();
                // if next record belongs to a different pair
                if (!nextName.equalsIgnoreCase(currentName)) {
                    bamWriter.close();
                    bamOutputStream.close();
                    changeWriter(context, header);
                    counter = 0;
                }
            }
        }

        bamWriter.close();
        bamOutputStream.close();
        counter = 0;
        reader.close();
    }

    private void writeToBloomFilter(SAMRecord record, Context context) throws IOException {
        if (record.getMateUnmappedFlag()) {
            String reference = record.getReferenceName();
            int unclippedPos = getCoordinate(record);
            GenomicPositionKey key = new GenomicPositionKey(reference,
                    unclippedPos);
            bloomFilter.put(key);
            context.getCounter(MapperRecords.BLOOM_INSERTS).increment(1);

        }

    }
    
    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }

    private void changeWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        String outputBamName = getNextFileName(context, fileCount);
        Path path = new Path(outputBamName);
        bamOutputStream = fs.create(path);
        bamWriter = new SAMFileWriterFactory().makeBAMWriter(header, false,
                bamOutputStream);
        ++fileCount;
    }

    private String getNextFileName(Context context, int fileCount) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "map-split-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + fileCount + ".bam";
        return outputDir + "/" + fileName;
    }

    private void flushBloomFilter(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path bloomFilePath = new Path(getBloomFilterFileName(context));
        FSDataOutputStream bloomOutStream = fs.create(bloomFilePath);
        bloomFilter.writeTo(bloomOutStream);
        IOUtils.closeStream(bloomOutStream);
    }
    
    private String getBloomFilterFileName(Context context) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "b-" + context.getTaskAttemptID().getTaskID().getId()
                + "-" + context.getTaskAttemptID().getId() + ".bloom";
        return outputDir + "/" + fileName;
    }

}
