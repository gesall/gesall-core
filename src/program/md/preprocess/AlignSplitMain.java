package program.md.preprocess;

import hdfs.LogicalCompleteBamInputFormat;
import hdfs.utils.BamPathFilter;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class AlignSplitMain extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String inputPath = null;
        String outputPath = null;
        boolean debugMode = false;
        try {
            options = parser.parse(args);
            inputPath = (String) options.valueOf("i");
            outputPath = (String) options.valueOf("o");
            debugMode = options.has("d");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        // Set configuration parameters
        Configuration conf = getConf();
        conf.set("OutputDir", outputPath);
        conf.setBoolean("DebugMode", debugMode);
        conf.set("mapreduce.job.user.classpath.first", "true");

        // Set job parameters
        Job job = Job.getInstance(conf);
        job.setJobName("Alignment Output Splitter");
        job.setJarByClass(AlignSplitMain.class);
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] statuses = fs.listStatus(new Path(inputPath),
                new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setInputFormatClass(LogicalCompleteBamInputFormat.class);

        job.setMapperClass(AlignSplitMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(0);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("i:o:dh*");
        parser.accepts("i", "Input BAM directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("o", "Output directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("d", "Debug mode");
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new AlignSplitMain(),
                args);
        System.exit(res);
    }

}
