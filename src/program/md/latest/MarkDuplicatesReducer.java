package program.md.latest;

import hdfs.MemoryBamManager;
import hdfs.config.CmdLineArgs;
import hdfs.config.PropertyReader;
import hdfs.utils.LogPrinter;
import hdfs.utils.MarkDuplicatesShuffleKey;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

//import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class MarkDuplicatesReducer extends
        Reducer<MarkDuplicatesShuffleKey, BytesWritable, NullWritable, NullWritable> {

    static enum ReducerRecords {
        REDUCE_KEYS, SHUFFLED_SAM_RECORDS, MAPPED_SAM_RECORDS, MAPPED_SAM_WRITTEN, UNMAPPED_SAM_RECORDS, UNMAPPED_SAM_WRITTEN, READONLY_SAM_RECORDS, OUTPUT_SAM_RECORDS, DUPLICATES
    }

    private static final Log log               = LogFactory
                                                       .getLog(MarkDuplicatesReducer.class);

    String                   headerFileName;
    String                   markDupLocalLogDir;

    FSDataOutputStream       reducerOutput;
    SAMFileWriter            reducerWriter;

    FSDataOutputStream       pipedOutput;
    SAMFileWriter            pipedWriter;

    int                      mapMemRecordCount;
    // final int countMax = 1;
    final int                MAX_MAPPED_MEMORY = 1000000;                                   // 1,00,000
    // int SBSize = countMax * 300;

    int                      flushCount;

    int                      unmapMemRecordCount;
    final int                MAX_UNMAP_MEMORY  = 10000;                                     // 10,000
    // int PRSize = pipedCountMax * 300;

    // time
    //Stopwatch                reducerTimer;
    //Stopwatch                sortTimer;
    //Stopwatch                mdupTimer;

    MemoryBamManager         mapMemoryBam;
    MemoryBamManager         unmapMemoryBam;

    @Override
    public void setup(Context context) throws IOException {
        // deleteReducerLogs(context); // XXX
        //reducerTimer = Stopwatch.createUnstarted();
        //sortTimer = Stopwatch.createUnstarted();
        //mdupTimer = Stopwatch.createUnstarted();

        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");
        headerFileName = conf.get("BamInput");
        mapMemoryBam = new MemoryBamManager(conf, headerFileName,
                MAX_MAPPED_MEMORY + 1000);

        mapMemRecordCount = 0;
        flushCount = 0;

        unmapMemoryBam = new MemoryBamManager(conf, headerFileName,
                MAX_UNMAP_MEMORY + 1000);
        unmapMemRecordCount = 0;
    }

    @Override
    public void reduce(MarkDuplicatesShuffleKey key, Iterable<BytesWritable> values,
            Context context) throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.REDUCE_KEYS).increment(1);

        /*
         * boolean isUnmapped = false; if (key.getCoordinate() == 0) {
         * isUnmapped = true; }
         */
        boolean bothUnmapped = key.areBothUnmapped();

        for (BytesWritable value : values) {
            context.getCounter(ReducerRecords.SHUFFLED_SAM_RECORDS)
                    .increment(1);

            byte[] record = value.copyBytes();

            if (bothUnmapped) {
                context.getCounter(ReducerRecords.UNMAPPED_SAM_RECORDS)
                        .increment(1);

                unmapMemoryBam.addRecord(record);
                ++unmapMemRecordCount;
                if (unmapMemRecordCount > MAX_UNMAP_MEMORY) {
                    doPipe(context);
                }

            } else {
                context.getCounter(ReducerRecords.MAPPED_SAM_RECORDS)
                        .increment(1);
                mapMemoryBam.addRecord(record);
                ++mapMemRecordCount;
            }

        }
        if (unmapMemRecordCount > MAX_UNMAP_MEMORY) {
            doPipe(context);
        }

        if (mapMemRecordCount > MAX_MAPPED_MEMORY) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (mapMemRecordCount > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }

        if (unmapMemRecordCount > 0) {
            doPipe(context);
        }
        if (pipedWriter != null) {
            pipedWriter.close();
        }
        if (pipedOutput != null) {
            pipedOutput.close();
        }

        log.info("[CPRO] [ReduceTask] "
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId());
        
        //LogPrinter.printSecondsElaped(log, "SortSam",
        //        sortTimer.elapsed(TimeUnit.SECONDS));
        //LogPrinter.printSecondsElaped(log, "MarkDuplicates",
        //        mdupTimer.elapsed(TimeUnit.SECONDS));
        //LogPrinter.printSecondsElaped(log, "MarkDupQuickReducer",
        //        reducerTimer.elapsed(TimeUnit.SECONDS));
    }

    // Writes unmapped reads directly to HDFS
    private void doPipe(Context context) throws IOException {
        //reducerTimer.start();

        SAMFileReader pipedReader = unmapMemoryBam.getSamFileReader();
        if (pipedWriter == null) {
            SAMFileHeader header = pipedReader.getFileHeader();
            header.setSortOrder(SortOrder.unsorted);
            createPipedWriter(context, header);
        }

        for (SAMRecord record : pipedReader) {
            // XXX New addition
            if (record.getIntegerAttribute("ro") == 1) {
                context.getCounter(ReducerRecords.READONLY_SAM_RECORDS)
                        .increment(1);
                continue;
            }

            // XXX TODO To remove
            if (record.getDuplicateReadFlag()) {
                context.getCounter(ReducerRecords.DUPLICATES).increment(1);
            }
            // End XXX TODO To remove

            record.setAttribute("ro", null);

            context.getCounter(ReducerRecords.UNMAPPED_SAM_WRITTEN)
                    .increment(1);
            context.getCounter(ReducerRecords.OUTPUT_SAM_RECORDS).increment(1);
            pipedWriter.addAlignment(record);
        }
        pipedReader.close();

        unmapMemoryBam.clear();
        unmapMemRecordCount = 0;
        //reducerTimer.stop();
    }

    private void doWork(Context context) throws IOException {
        //reducerTimer.start();
        flushCount++;

        // Run SortSam
        SAMFileReader sortSamInput = mapMemoryBam.getSamFileReader();
        byte[] markDupInput = runSortSam(sortSamInput);

        // **GC**
        mapMemoryBam.clear();
        // System.gc();

        // Run MarkDuplicates
        byte[] markDupOutBytes = runMarkDuplicates(markDupInput, context);

        // Write output to HDFS
        InputStream markDupOutStream = new ByteArrayInputStream(markDupOutBytes);
        SAMFileReader markDupOutReader = new SAMFileReader(markDupOutStream);

        if (reducerWriter == null) {
            SAMFileHeader reducerOutHeader = markDupOutReader.getFileHeader();
            reducerOutHeader.setSortOrder(SortOrder.unsorted);
            createSingletonWriter(context, reducerOutHeader);
        }

        for (SAMRecord record : markDupOutReader) {
            // XXX New addition
            if (record.getIntegerAttribute("ro") == 1) {
                context.getCounter(ReducerRecords.READONLY_SAM_RECORDS)
                        .increment(1);
                continue;
            }
            record.setAttribute("ro", null);
            context.getCounter(ReducerRecords.MAPPED_SAM_WRITTEN).increment(1);
            context.getCounter(ReducerRecords.OUTPUT_SAM_RECORDS).increment(1);

            // XXX TODO To remove
            if (record.getDuplicateReadFlag()) {
                context.getCounter(ReducerRecords.DUPLICATES).increment(1);
            }
            // End XXX TODO To remove

            reducerWriter.addAlignment(record);
        }
        markDupOutReader.close();

        mapMemoryBam.clear();
        mapMemRecordCount = 0;
        //reducerTimer.stop();
    }

    private byte[] runSortSam(SAMFileReader input) throws IOException {
        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        ByteArrayOutputStream sortSamOutWriter = new ByteArrayOutputStream();
        //sortTimer.start();
        new SortSam().instanceMainWithoutExit(sortSamArgv, input,
                sortSamOutWriter);
        sortSamOutWriter.flush();
        //sortTimer.stop();
        byte[] output = sortSamOutWriter.toByteArray();
        return output;
    }

    private byte[] runMarkDuplicates(byte[] input, Context context)
            throws IOException {
        String[] markDupArgv = CmdLineArgs.getMarkDupArgv();

        markDupArgv[4] = "METRICS_FILE=" + markDupLocalLogDir + "/markdup-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".log";

        // markDupArgv[4] = "METRICS_FILE=" + getLogFileName(context);
        ByteArrayOutputStream markDupOutWriter = new ByteArrayOutputStream();
        //mdupTimer.start();
        new MarkDuplicates().instanceMainWithoutExitWithMulReader(markDupArgv,
                input, markDupOutWriter);
        markDupOutWriter.flush();
        //mdupTimer.stop();
        byte[] output = markDupOutWriter.toByteArray();
        return output;
    }

    // XXX
    private String getLogFileName(Context context) {
        String tempLog = markDupLocalLogDir + "/temp-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + ".log";
        String reducerLog = markDupLocalLogDir + "/markdup-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + ".log";
        String appendCommand = "cat " + tempLog
                + " | grep gesall | grep -v picard.sam.MarkDuplicates >> "
                + reducerLog;
        executeCommand(appendCommand);
        String deleteCommand = "rm " + tempLog;
        executeCommand(deleteCommand);
        return tempLog;
    }

    private void deleteReducerLogs(Context context) {
        String deleteCommand = "rm " + markDupLocalLogDir + "/markdup*";
        executeCommand(deleteCommand);
    }

    private void createSingletonWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        // FileSystem fs = FileSystem.newInstance(conf);
        String outputBamName = getNextFileName(context, "map");
        Path path = new Path(outputBamName);
        reducerOutput = fs.create(path);
        reducerWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                reducerOutput);
    }

    private void createPipedWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        // FileSystem fs = FileSystem.newInstance(conf);
        String outputBamName = getNextFileName(context, "unmap");
        Path path = new Path(outputBamName);
        pipedOutput = fs.create(path);
        pipedWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                pipedOutput);
    }

    private String getNextFileName(Context context, String suffix) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        return outputDir + "/" + fileName;
    }

    private String executeCommand(String command) {
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(new String[] { "sh", "-c", command });
            //
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

}
