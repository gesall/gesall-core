package program.clean.rangepart;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.bloom.GenomicPositionKey;
import hdfs.config.CmdLineArgs;
import hdfs.partition.range.KeyRangePartitioner;
import hdfs.partition.range.KeyValuePair;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Stopwatch;

import picard.sam.AddOrReplaceReadGroups;
import picard.sam.CleanSam;

@SuppressWarnings("deprecation")
public class CleanMapper extends
        Mapper<LongWritable, BytesWritable, GenomicPositionKey, Text> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_SAM_RECORDS
    }

    private static final Log      log = LogFactory.getLog(CleanMapper.class);

    @SuppressWarnings("unused")
    private boolean               debugMode;

    private BamRecordEnumeration  samEnum;
    private String                headerFileName;
    private List<BamBlockWrapper> bamBlocks;

    String[]                      addRplArgs;
    String[]                      cleanSamArgs;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        addRplArgs = CmdLineArgs.getAddRplArgs(conf);
        cleanSamArgs = CmdLineArgs.getCleanSamArgv();
        headerFileName = conf.get("BamInput");
        debugMode = conf.getBoolean("DebugMode", false);
        samEnum = new BamRecordEnumeration(context.getConfiguration(),
                headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    // TODO: Make functions for each step

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {

        Stopwatch timer = Stopwatch.createStarted();

        SAMFileReader addReplInput = samEnum.getSAMFileReader(bamBlocks);
        /*
         * if (debugMode) { SAMRecordIterator it = samReaderInput1.iterator();
         * while (it.hasNext()) {
         * context.getCounter(MapperRecords.INPUT_BAM_RECORDS) .increment(1);
         * it.next(); } it.close(); }
         */

        byte[] addReplOutput = runAddOrReplaceReadGroups(addReplInput);

        SAMFileReader cleanSamInput = new SAMFileReader(
                new ByteArrayInputStream(addReplOutput));

        byte[] cleanSamOutput = runCleanSam(cleanSamInput);

        // shuffle records
        InputStream inputStream = new ByteArrayInputStream(cleanSamOutput);
        SAMFileReader samFileReader = new SAMFileReader(inputStream);
        KeyRangePartitioner rangePartitoner = new KeyRangePartitioner();
        for (SAMRecord record : samFileReader) {
            // Old partitioning scheme
            // context.write(new Text(record.getReadName()),
            //        new Text(record.getSAMString()));
            KeyValuePair<GenomicPositionKey, Text> kvPair = rangePartitoner.getKVPair(record);
            if (kvPair != null) {
                // TODO: Temporary condition
                GenomicPositionKey key = kvPair.getKey();
                Text value = kvPair.getValue();
                context.write(key, value);            
                context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
            }
        }
        samFileReader.close();

        timer.stop();
        LogPrinter.printSecondsElaped(log, "CleanMapper",
                timer.elapsed(TimeUnit.SECONDS));
    }

    private byte[] runAddOrReplaceReadGroups(SAMFileReader input)
            throws IOException {
        ByteArrayOutputStream outputWriter = new ByteArrayOutputStream();

        Stopwatch subTimer = Stopwatch.createStarted();
        new AddOrReplaceReadGroups().instanceMainWithoutExit(addRplArgs, input,
                outputWriter);
        subTimer.stop();
        LogPrinter.printSecondsElaped(log, "AddOrReplaceReadGroups",
                subTimer.elapsed(TimeUnit.SECONDS));

        outputWriter.flush();
        byte[] output = outputWriter.toByteArray();
        return output;
    }

    private byte[] runCleanSam(SAMFileReader input) throws IOException {
        ByteArrayOutputStream outputWriter = new ByteArrayOutputStream();

        Stopwatch subTimer = Stopwatch.createStarted();
        new CleanSam().instanceMainWithoutExit(cleanSamArgs, input,
                outputWriter);
        subTimer.stop();
        LogPrinter.printSecondsElaped(log, "CleanSam",
                subTimer.elapsed(TimeUnit.SECONDS));

        outputWriter.flush();
        byte[] output = outputWriter.toByteArray();
        return output;
    }

}
