package vintage.program.md.md1;

import java.io.IOException;

import hdfs.DupSamWritable;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IdentityReducer extends
        Reducer<DupSamWritable, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        COUNTER
    }

    @SuppressWarnings("unused")
    @Override
    public void reduce(DupSamWritable key, Iterable<Text> values,
            Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            context.getCounter(ReducerRecords.COUNTER).increment(1);
            String samRecord = new String(value.toString());
            samRecord.length();
        }
    }

}