package vintage.program.md.md1;

import java.io.IOException;

import hdfs.DupSamWritable;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MarkDupReducingCounter extends
        Reducer<DupSamWritable, Text, Text, LongWritable> {

    static enum ReducerRecords {
        REDUCE_KEYS, SAM_RECORDS, SAM_ACTUAL
    }

    @Override
    public void reduce(DupSamWritable key, Iterable<Text> values,
            Context context) throws IOException, InterruptedException {

        context.getCounter(ReducerRecords.REDUCE_KEYS).increment(1);

        long counter = 0;
        for (Text value : values) {
            context.getCounter(ReducerRecords.SAM_RECORDS).increment(1);
            ++counter;
        }
        // System.out.println(key + "\t" + counter);
        context.write(new Text(key.toString()), new LongWritable(counter));
    }

}
