package vintage.program.md.md1;

import hdfs.DupSamWritable;
import hdfs.SamInputStream;
import hdfs.config.CmdLineArgs;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.environment.EnvironmentUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

@SuppressWarnings("deprecation")
public class MarkDupDiskReducer extends
        Reducer<DupSamWritable, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        REDUCE_KEYS, SAM_RECORDS, MAP_SAM_RECORDS, MAP_SAM_WRITTEN, UNMAP_SAM_RECORDS, UNMAP_SAM_WRITTEN
    }

    static enum TimeProfile {
        REDUCE_TIME, SORT_TIME, MDUP_TIME
    }

    String             headerFileName;
    SamInputStream     samFile;
    SamInputStream     pipedSamFile;

    FSDataOutputStream reducerOutput;
    SAMFileWriter      reducerWriter;

    FSDataOutputStream pipedOutput;
    SAMFileWriter      pipedWriter;

    int                count;
    final int          countMax      = 100000; // 1,00,000 XXX
    StringBuilder      samRecords;

    int                flushCount;

    int                pipedCount;
    final int          pipedCountMax = 10000;
    StringBuilder      pipedRecords;

    // time
    long               reducer_t     = 0;
    long               sort_t        = 0;
    long               mdup_t        = 0;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samFile = new SamInputStream(conf, headerFileName);

        count = 0;
        flushCount = 0;
        samRecords = new StringBuilder();

        pipedSamFile = new SamInputStream(conf, headerFileName);
        pipedCount = 0;
        pipedRecords = new StringBuilder();

        reducer_t = System.currentTimeMillis();

    }

    @Override
    public void reduce(DupSamWritable key, Iterable<Text> values,
            Context context) throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.REDUCE_KEYS).increment(1);

        boolean isUnmapped = false;
        if (key.getCoordinate() == 0) {
            isUnmapped = true;
        }

        /*
         * Iterator<Text> itPairTest = values.iterator(); boolean paired =
         * false; int counter = 0; for (Text value : values) { ++counter; if
         * (counter > 2) { break; } } if (counter == 2) {
         * 
         * }
         */

        for (Text value : values) {
            context.getCounter(ReducerRecords.SAM_RECORDS).increment(1);
            String samRecord = new String(value.toString());

            if (isUnmapped) {
                context.getCounter(ReducerRecords.UNMAP_SAM_RECORDS).increment(
                        1);

                pipedRecords.append(samRecord);
                ++pipedCount;
                if (pipedCount > pipedCountMax) {
                    doPipe(context);
                }

            } else {
                context.getCounter(ReducerRecords.MAP_SAM_RECORDS).increment(1);
                samRecords.append(samRecord);
                ++count;
            }

        }
        if (pipedCount > pipedCountMax) {
            doPipe(context);
        }

        if (count > countMax) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (count > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }

        if (pipedCount > 0) {
            doPipe(context);
        }
        if (pipedWriter != null) {
            pipedWriter.close();
        }
        if (pipedOutput != null) {
            pipedOutput.close();
        }

        reducer_t = System.currentTimeMillis() - reducer_t;

        System.out.println("TIMEPROFILE: ");
        System.out.println("Reducer:\t"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId());
        System.out.println("Total time:\t" + reducer_t);
        System.out.println("Sort time:\t" + sort_t);
        System.out.println("MarkDup time:\t" + mdup_t);
        System.out.println("-----");

    }

    private void doPipe(Context context) throws IOException {
        pipedSamFile.add(pipedRecords);

        SAMFileReader pipedReader = pipedSamFile.getSamFileReader();
        if (pipedWriter == null) {
            SAMFileHeader header = pipedReader.getFileHeader();
            header.setSortOrder(SortOrder.unsorted);
            createPipedWriter(context, header);
        }

        for (SAMRecord record : pipedReader) {
            context.getCounter(ReducerRecords.UNMAP_SAM_WRITTEN).increment(1);

            pipedWriter.addAlignment(record);

        }
        pipedReader.close();

        pipedSamFile.clear();
        pipedCount = 0;
        pipedRecords = new StringBuilder();

    }

    private void doWork(Context context) throws IOException {
        flushCount++;
        samFile.add(samRecords);

        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        SAMFileReader sortSamInReader = samFile.getSamFileReader();
        ByteArrayOutputStream sortSamOutWriter = new ByteArrayOutputStream();

        long st1 = System.currentTimeMillis();
        new SortSam().instanceMainWithoutExit(sortSamArgv, sortSamInReader,
                sortSamOutWriter);

        sort_t += +System.currentTimeMillis() - st1;

        sortSamOutWriter.flush();

        // **GC**
        samFile.clear();
        samRecords = new StringBuilder();
        // System.gc();
        // System.gc();

        byte[] markDupInBytes = sortSamOutWriter.toByteArray();

        // 1. Write to disk

        InputStream inputStream = new ByteArrayInputStream(markDupInBytes);
        SAMFileReader reader = new SAMFileReader(inputStream);

        File inFile = new File("/state/partition2/gesall/scratch/mdIn-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".bam");
        SAMFileWriter writer = new SAMFileWriterFactory().makeBAMWriter(
                reader.getFileHeader(), true, inFile);

        SAMRecordIterator iter = reader.iterator();
        while (iter.hasNext()) {
            SAMRecord record = iter.next();
            writer.addAlignment(record);
        }
        reader.close();
        writer.close();

        // 2. System call to MD
        long mt1 = System.currentTimeMillis();
        runCommand(context);
        mdup_t += +System.currentTimeMillis() - mt1;

        // 3. Write to HDFS optional

        // 4. Delete from local disk

        /*
        byte[] markDupOutBytes = markDupOutWriter.toByteArray();
        InputStream markDupOutStream = new ByteArrayInputStream(markDupOutBytes);
        SAMFileReader markDupOutReader = new SAMFileReader(markDupOutStream);

        if (reducerWriter == null) {
            SAMFileHeader reducerOutHeader = markDupOutReader.getFileHeader();
            reducerOutHeader.setSortOrder(SortOrder.unsorted);
            createSingletonWriter(context, reducerOutHeader);
        }

        for (SAMRecord record : markDupOutReader) {
            context.getCounter(ReducerRecords.MAP_SAM_WRITTEN).increment(1);

            reducerWriter.addAlignment(record);
        }
        markDupOutReader.close();
        */
        
        samFile.clear();
        count = 0;
        samRecords = new StringBuilder();

    }

    private void createSingletonWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = new Configuration();
        // FileSystem fs = FileSystem.get(conf);
        FileSystem fs = FileSystem.newInstance(conf);
        Random r = new Random();

        String outputBamName = "hdfs://yeeha.yeeha:9000/user/aroy/md_out3/reduce-"
                + context.getTaskAttemptID().getTaskID().getId()
                + "-"
                + context.getTaskAttemptID().getId()
                + "-"
                + Math.abs(r.nextInt()) + ".bam";
        Path path = new Path(outputBamName);
        reducerOutput = fs.create(path);

        reducerWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                reducerOutput);

    }

    private void createPipedWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = new Configuration();
        // FileSystem fs = FileSystem.get(conf);
        FileSystem fs = FileSystem.newInstance(conf);

        Random r = new Random();
        String outputBamName = "hdfs://yeeha.yeeha:9000/user/aroy/md_out3/reduce-"
                + context.getTaskAttemptID().getTaskID().getId()
                + "-"
                + context.getTaskAttemptID().getId()
                + "-"
                + Math.abs(r.nextInt()) + "-unmap.bam";
        Path path = new Path(outputBamName);
        pipedOutput = fs.create(path);

        pipedWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                pipedOutput);

    }

    public int runCommand(Context context) {

        String inputFile = "/state/partition2/gesall/scratch/mdIn-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".bam";

        String outputFile = "/state/partition2/gesall/scratch/mdOut-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".bam";

        String metricsFile = "/state/partition2/gesall/scratch/mdLog-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".log";
        
        CommandLine comm = new CommandLine("java");
        comm.addArgument("-Xmx10g");
        comm.addArgument("-Djava.io.tmpdir=/state/partition2/gesall/scratch");
        comm.addArgument("-jar");
        comm.addArgument("/state/partition2/gesall/software/picard/MarkDuplicates.jar");
        comm.addArgument("INPUT=" + inputFile);
        comm.addArgument("OUTPUT=" + outputFile);
        comm.addArgument("VALIDATION_STRINGENCY=LENIENT");
        comm.addArgument("ASSUME_SORTED=TRUE");
        comm.addArgument("METRICS_FILE=" + metricsFile);

        DefaultExecutor executor = new DefaultExecutor();
        // ExecuteWatchdog watchdog = new ExecuteWatchdog(INFINITE_TIMEOUT);
        // executor.setWatchdog(watchdog);
        int exitValue = 1;
        try {
            exitValue = executor.execute(comm,
                    EnvironmentUtils.getProcEnvironment());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return exitValue;
    }

}
