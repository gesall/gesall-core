package vintage.program.md.md1;

import hdfs.utils.LogPrinter;
import hdfs.utils.MarkDuplicatesShuffleKey;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

@SuppressWarnings("deprecation")
public class MDFixMapper extends Mapper<Text, Text, MarkDuplicatesShuffleKey, Text> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_SAM_RECORDS, ARE_RECORDS_PAIRED, EXTRA_SHUFFLED_SAM_RECORDS
    }

    private static final Log log = LogFactory.getLog(MDFixMapper.class);

    private String           bamFilePath;

    @Override
    public void map(Text key, Text value, Context context) {
        this.bamFilePath = value.toString();
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
    }

    Set<NewMDKey> roRecords = new HashSet<NewMDKey>();

    public void doWork(Context context) throws IOException,
            InterruptedException {
        Stopwatch timer = Stopwatch.createStarted();
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path path = new Path(bamFilePath);
        FSDataInputStream in = fs.open(path);
        SAMFileReader reader = new SAMFileReader(in);

        PeekingIterator<SAMRecord> it = Iterators.peekingIterator(reader
                .iterator());
        while (it.hasNext()) {
            SAMRecord curr = it.next();
            context.getCounter(MapperRecords.INPUT_BAM_RECORDS).increment(1);
            String currName = curr.getReadName();

            if (curr.getReadUnmappedFlag()) {
                writeFragmentRecord(context, curr);
                continue;
            }

            if (curr.isSecondaryOrSupplementary()) {
                writeFragmentRecord(context, curr);
                continue;
            }

            if (curr.getMateUnmappedFlag()) {
                writeFragmentRecord(context, curr);
                continue;
            }

            if (!it.hasNext()) {
                writeFragmentRecord(context, curr);
                continue;
            }

            SAMRecord next = it.peek();
            String nextName = next.getReadName();

            if (!currName.equalsIgnoreCase(nextName)) {
                writeFragmentRecord(context, curr);
                continue;
            }

            while ((next.getReadUnmappedFlag() || next
                    .isSecondaryOrSupplementary())
                    && currName.equalsIgnoreCase(nextName)) {

                SAMRecord actualNext = it.next();
                context.getCounter(MapperRecords.INPUT_BAM_RECORDS)
                        .increment(1);
                writeFragmentRecord(context, actualNext);
                if (it.hasNext()) {
                    next = it.peek();
                    nextName = next.getReadName();
                } else {
                    next = null;
                    break;
                }

            }

            if (next == null) {
                writeFragmentRecord(context, curr);
                continue;
            }

            if (currName.equalsIgnoreCase(nextName)) {
                SAMRecord actualNext = it.next();
                context.getCounter(MapperRecords.INPUT_BAM_RECORDS)
                        .increment(1);

                writeROPairedRecord(context, curr);
                writeROPairedRecord(context, actualNext);
                writePairedRecords(context, curr, actualNext);
            }

            if (!currName.equalsIgnoreCase(nextName)) {
                writeFragmentRecord(context, curr);
                continue;
            }

        }

        reader.close();
        timer.stop();
        LogPrinter.printSecondsElaped(log, "MarkDupMapper",
                timer.elapsed(TimeUnit.SECONDS));
    }

    private void writeFragmentRecord(Context context, SAMRecord record)
            throws IOException, InterruptedException {
        boolean forPairedDups = false;
        MarkDuplicatesShuffleKey key = new MarkDuplicatesShuffleKey(record.getReferenceIndex(),
                getCoordinate(record), record.getReadUnmappedFlag(),
                forPairedDups);
        record.setAttribute("ro", null);
        record.setAttribute("ro", 0);

        String value = new String(record.getSAMString());
        context.write(key, new Text(value));
        context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);

    }

    private void writeROPairedRecord(Context context, SAMRecord record)
            throws IOException, InterruptedException {
        
        NewMDKey hashKey = new NewMDKey(record);
        if (roRecords.contains(hashKey)) {
            return;
        } else {
            roRecords.add(hashKey);
        }

        
        
        boolean forPairedDups = false;
        MarkDuplicatesShuffleKey key = new MarkDuplicatesShuffleKey(record.getReferenceIndex(),
                getCoordinate(record), record.getReadUnmappedFlag(),
                forPairedDups);

        record.setAttribute("ro", null);
        record.setAttribute("ro", 1);

        String value = new String(record.getSAMString());
        context.write(key, new Text(value));
        context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
        context.getCounter(MapperRecords.EXTRA_SHUFFLED_SAM_RECORDS).increment(
                1);

    }

    private void writePairedRecords(Context context, SAMRecord one,
            SAMRecord two) throws IOException, InterruptedException {
        int oneCoordinate = getCoordinate(one);
        int twoCoordinate = getCoordinate(two);

        int sequence;
        int coordinate;

        int oneReferenceIndex = one.getReferenceIndex();
        int twoReferenceIndex = two.getReferenceIndex();

        // WHAT ABOUT STRAND>>
        if (twoReferenceIndex > oneReferenceIndex
                || (twoReferenceIndex == oneReferenceIndex && twoCoordinate >= oneCoordinate)) {
            sequence = oneReferenceIndex;
            coordinate = oneCoordinate;
        } else {
            sequence = twoReferenceIndex;
            coordinate = twoCoordinate;
        }
        boolean bothUnmapped = one.getReadUnmappedFlag()
                && two.getReadUnmappedFlag();

        // sequence, coordinate, bothUnmapped, forPaired
        boolean forPairedDups = true;

        writePairedRecordsHelper(context, sequence, coordinate, bothUnmapped,
                forPairedDups, one);
        writePairedRecordsHelper(context, sequence, coordinate, bothUnmapped,
                forPairedDups, two);
        context.getCounter(MapperRecords.ARE_RECORDS_PAIRED).increment(2);
    }

    private void writePairedRecordsHelper(Context context, int sequence,
            int coordinate, boolean bothUnmapped, boolean forPairedDups,
            SAMRecord record) throws IOException, InterruptedException {
        MarkDuplicatesShuffleKey key = new MarkDuplicatesShuffleKey(sequence, coordinate, bothUnmapped,
                forPairedDups);

        record.setAttribute("ro", null);
        record.setAttribute("ro", 0);
        String value = new String(record.getSAMString());
        context.write(key, new Text(value));
        context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
    }

    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }
}

class NewMDKey {

    Integer referenceIndex;
    Integer coordinate;
    Boolean negStrand;

    // XXX ignores Library id which requires header, but we 
    // can use String readGroupId = (String) rec.getAttribute("RG");
    
    public NewMDKey(SAMRecord record) {
        this.referenceIndex = record.getReferenceIndex();
        this.coordinate = getCoordinate(record);
        this.negStrand = record.getReadNegativeStrandFlag();
    }

    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof NewMDKey) {
            NewMDKey newMdKey = (NewMDKey) o;
            return referenceIndex.equals(newMdKey.referenceIndex)
                    && coordinate.equals(newMdKey.coordinate)
                    && negStrand.equals(newMdKey.negStrand);
        }
        return false;
    }

    @Override
    public int hashCode() {
        
        return (referenceIndex.hashCode() * 163) + (coordinate.hashCode() * 17)
                + negStrand.hashCode();
    }

}
