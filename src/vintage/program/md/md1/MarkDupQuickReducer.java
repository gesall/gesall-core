package vintage.program.md.md1;

import hdfs.DupSamWritable;
import hdfs.SamInputStream;
import hdfs.config.CmdLineArgs;
import hdfs.config.PropertyReader;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import com.google.common.base.Stopwatch;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

// TODO : Check Snappy Loader
@SuppressWarnings("deprecation")
public class MarkDupQuickReducer extends
        Reducer<DupSamWritable, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        REDUCE_KEYS, SAM_RECORDS, MAPPED_SAM_RECORDS, MAP_SAM_WRITTEN, UNMAPPED_SAM_RECORDS, UNMAP_SAM_WRITTEN
    }

    static enum TimeProfile {
        REDUCE_TIME, SORT_TIME, MDUP_TIME
    }

    private static final Log log           = LogFactory
                                                   .getLog(MarkDupQuickReducer.class);

    String                   headerFileName;
    String                   markDupLocalLogDir;
    SamInputStream           samFile;
    SamInputStream           pipedSamFile;

    FSDataOutputStream       reducerOutput;
    SAMFileWriter            reducerWriter;

    FSDataOutputStream       pipedOutput;
    SAMFileWriter            pipedWriter;

    int                      count;
    final int                countMax      = 1000000;                                 // 1,00,000
    StringBuilder            samRecords;
    // int SBSize = countMax * 300;

    int                      flushCount;

    int                      pipedCount;
    final int                pipedCountMax = 10000;                                   // 10,000
    StringBuilder            pipedRecords;
    // int PRSize = pipedCountMax * 300;

    // time
    Stopwatch                reducerTimer;
    Stopwatch                sortTimer;
    Stopwatch                mdupTimer;

    @Override
    public void setup(Context context) throws IOException {
        reducerTimer = Stopwatch.createUnstarted();
        sortTimer = Stopwatch.createUnstarted();
        mdupTimer = Stopwatch.createUnstarted();

        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");
        headerFileName = conf.get("BamInput");
        samFile = new SamInputStream(conf, headerFileName);

        count = 0;
        flushCount = 0;
        samRecords = new StringBuilder();

        pipedSamFile = new SamInputStream(conf, headerFileName);
        pipedCount = 0;
        pipedRecords = new StringBuilder();
    }

    @Override
    public void reduce(DupSamWritable key, Iterable<Text> values,
            Context context) throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.REDUCE_KEYS).increment(1);

        /*
        boolean isUnmapped = false;
        if (key.getCoordinate() == 0) {
            isUnmapped = true;
        }*/
        boolean bothUnmapped = key.areBothUnmapped();
        
        for (Text value : values) {
            context.getCounter(ReducerRecords.SAM_RECORDS).increment(1);
            String samRecord = new String(value.toString());

            if (bothUnmapped) {
                context.getCounter(ReducerRecords.UNMAPPED_SAM_RECORDS)
                        .increment(1);

                pipedRecords.append(samRecord);
                ++pipedCount;
                if (pipedCount > pipedCountMax) {
                    doPipe(context);
                }

            } else {
                context.getCounter(ReducerRecords.MAPPED_SAM_RECORDS)
                        .increment(1);
                samRecords.append(samRecord);
                ++count;
            }

        }
        if (pipedCount > pipedCountMax) {
            doPipe(context);
        }

        if (count > countMax) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (count > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }

        if (pipedCount > 0) {
            doPipe(context);
        }
        if (pipedWriter != null) {
            pipedWriter.close();
        }
        if (pipedOutput != null) {
            pipedOutput.close();
        }

        log.info("[CPRO] [ReduceTask] "
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId());
        LogPrinter.printSecondsElaped(log, "SortSam",
                sortTimer.elapsed(TimeUnit.SECONDS));
        LogPrinter.printSecondsElaped(log, "MarkDuplicates",
                mdupTimer.elapsed(TimeUnit.SECONDS));
        LogPrinter.printSecondsElaped(log, "MarkDupQuickReducer",
                reducerTimer.elapsed(TimeUnit.SECONDS));
    }

    // Writes unmapped reads directly to HDFS
    private void doPipe(Context context) throws IOException {
        reducerTimer.start();
        pipedSamFile.add(pipedRecords);

        SAMFileReader pipedReader = pipedSamFile.getSamFileReader();
        if (pipedWriter == null) {
            SAMFileHeader header = pipedReader.getFileHeader();
            header.setSortOrder(SortOrder.unsorted);
            createPipedWriter(context, header);
        }

        for (SAMRecord record : pipedReader) {
            context.getCounter(ReducerRecords.UNMAP_SAM_WRITTEN).increment(1);
            pipedWriter.addAlignment(record);
        }
        pipedReader.close();

        pipedSamFile.clear();
        pipedCount = 0;
        pipedRecords = new StringBuilder();
        reducerTimer.stop();
    }

    private void doWork(Context context) throws IOException {
        reducerTimer.start();
        flushCount++;
        samFile.add(samRecords);

        // Run SortSam
        SAMFileReader sortSamInput = samFile.getSamFileReader();
        byte[] markDupInput = runSortSam(sortSamInput);

        // **GC**
        samFile.clear();
        samRecords = new StringBuilder();
        // System.gc();

        // Run MarkDuplicates
        byte[] markDupOutBytes = runMarkDuplicates(markDupInput, context);

        // Write output to HDFS
        InputStream markDupOutStream = new ByteArrayInputStream(markDupOutBytes);
        SAMFileReader markDupOutReader = new SAMFileReader(markDupOutStream);

        if (reducerWriter == null) {
            SAMFileHeader reducerOutHeader = markDupOutReader.getFileHeader();
            reducerOutHeader.setSortOrder(SortOrder.unsorted);
            createSingletonWriter(context, reducerOutHeader);
        }

        for (SAMRecord record : markDupOutReader) {
            context.getCounter(ReducerRecords.MAP_SAM_WRITTEN).increment(1);
            reducerWriter.addAlignment(record);
        }
        markDupOutReader.close();

        samFile.clear();
        count = 0;
        samRecords = new StringBuilder();
        reducerTimer.stop();
    }

    private byte[] runSortSam(SAMFileReader input) throws IOException {
        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        ByteArrayOutputStream sortSamOutWriter = new ByteArrayOutputStream();
        sortTimer.start();
        new SortSam().instanceMainWithoutExit(sortSamArgv, input,
                sortSamOutWriter);
        sortSamOutWriter.flush();
        sortTimer.stop();
        byte[] output = sortSamOutWriter.toByteArray();
        return output;
    }

    private byte[] runMarkDuplicates(byte[] input, Context context)
            throws IOException {
        String[] markDupArgv = CmdLineArgs.getMarkDupArgv();
        markDupArgv[4] = "METRICS_FILE=" + markDupLocalLogDir + "/markdup-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + flushCount
                + ".log";
        ByteArrayOutputStream markDupOutWriter = new ByteArrayOutputStream();
        mdupTimer.start();
        new MarkDuplicates().instanceMainWithoutExitWithMulReader(markDupArgv,
                input, markDupOutWriter);
        markDupOutWriter.flush();
        mdupTimer.stop();
        byte[] output = markDupOutWriter.toByteArray();
        return output;
    }

    private void createSingletonWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        // FileSystem fs = FileSystem.newInstance(conf);
        String outputBamName = getNextFileName(context, "map");
        Path path = new Path(outputBamName);
        reducerOutput = fs.create(path);
        reducerWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                reducerOutput);
    }

    private void createPipedWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        // FileSystem fs = FileSystem.newInstance(conf);
        String outputBamName = getNextFileName(context, "unmap");
        Path path = new Path(outputBamName);
        pipedOutput = fs.create(path);
        pipedWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                pipedOutput);
    }

    private String getNextFileName(Context context, String suffix) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        return outputDir + "/" + fileName;
    }

}
