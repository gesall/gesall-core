package vintage.program.md.md1;

import hdfs.DupSamWritable;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

@SuppressWarnings("deprecation")
public class FinalFixMDMapper extends Mapper<Text, Text, DupSamWritable, Text> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_SAM_RECORDS, ARE_RECORDS_PAIRED, IS_NEW
    }

    private static final Log log = LogFactory.getLog(MarkDupMapper.class);

    private String           bamFilePath;

    @Override
    public void map(Text key, Text value, Context context) {
        this.bamFilePath = value.toString();
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
    }

    static enum ReadCounters {
        TOTAL_RECORDS, PAIRED_RECORDS, UNPAIRED_RECORDS, PROPER_PAIRED_RECORDS, SEC_OR_SUPP_PAIRED, SEC_OR_SUPP_UNPAIRED, UNMAP_PAIRED, UNMAP_UNPAIRED, FRAG_SORT_EXCLUSIVE, PAIRED_MATE_UNMAPPED
    }

    public void doWork(Context context) throws IOException,
            InterruptedException {
        Stopwatch timer = Stopwatch.createStarted();
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path path = new Path(bamFilePath);
        FSDataInputStream in = fs.open(path);
        SAMFileReader reader = new SAMFileReader(in);

        /*
        context.getCounter(MapperRecords.IS_NEW).increment(1);

        SAMRecordIterator counterIt = reader.iterator();
        while (counterIt.hasNext()) {
            SAMRecord record = counterIt.next();

            context.getCounter(ReadCounters.TOTAL_RECORDS).increment(1);
            if (record.getReadPairedFlag()) {
                context.getCounter(ReadCounters.PAIRED_RECORDS).increment(1);
            } else {
                context.getCounter(ReadCounters.UNPAIRED_RECORDS).increment(1);
            }

            if (record.getProperPairFlag()) {
                context.getCounter(ReadCounters.PROPER_PAIRED_RECORDS)
                        .increment(1);
            }

            if (record.isSecondaryOrSupplementary()) {
                if (record.getReadPairedFlag()) {
                    // Secondary / Supplementary paired
                    context.getCounter(ReadCounters.SEC_OR_SUPP_PAIRED)
                            .increment(1);
                } else {
                    // Secondary / Supplementary unpaired
                    context.getCounter(ReadCounters.SEC_OR_SUPP_UNPAIRED)
                            .increment(1);
                }
            }

            if (record.getReadUnmappedFlag()) {
                if (record.getReadPairedFlag()) {
                    // Paired but unmapped
                    context.getCounter(ReadCounters.UNMAP_PAIRED).increment(1);
                } else {
                    // Unpaired and unmapped
                    context.getCounter(ReadCounters.UNMAP_UNPAIRED)
                            .increment(1);
                }
            }

            if (!record.getReadUnmappedFlag()) {
                if (!record.isSecondaryOrSupplementary()) {
                    if (!(record.getReadPairedFlag() && !record
                            .getMateUnmappedFlag())) {
                        context.getCounter(ReadCounters.FRAG_SORT_EXCLUSIVE)
                                .increment(1);
                    }
                }
            }

            if (record.getReadPairedFlag() && record.getMateUnmappedFlag()) {
                context.getCounter(ReadCounters.PAIRED_MATE_UNMAPPED)
                        .increment(1);
            }

            
        }

        counterIt.close();

        if (1 == 1) {
            reader.close();
            return;
        }
*/
        PeekingIterator<SAMRecord> it = Iterators.peekingIterator(reader
                .iterator());
        while (it.hasNext()) {
            SAMRecord curr = it.next();
            String currName = curr.getReadName();

            context.getCounter(MapperRecords.INPUT_BAM_RECORDS).increment(1);

            if (curr.getReadUnmappedFlag()) {
                writeSingleRecord(context, curr);
                continue;
            }

            if (curr.isSecondaryOrSupplementary()) {
                writeSingleRecord(context, curr);
                continue;
            }

            if (curr.getMateUnmappedFlag()) {
                writeSingleRecord(context, curr);
                continue;
            }
            
            if (!it.hasNext()) {
                writeSingleRecord(context, curr);
                continue;
            }

            SAMRecord next = it.peek();
            String nextName = next.getReadName();

            if (!currName.equalsIgnoreCase(nextName)) {
                writeSingleRecord(context, curr);
                continue;
            }

            while ((next.getReadUnmappedFlag() || next
                    .isSecondaryOrSupplementary())
                    && currName.equalsIgnoreCase(nextName)) {

                SAMRecord actualNext = it.next();
                context.getCounter(MapperRecords.INPUT_BAM_RECORDS)
                        .increment(1);
                writeSingleRecord(context, actualNext);
                if (it.hasNext()) {
                    next = it.peek();
                    nextName = next.getReadName();
                } else {
                    next = null;
                    break;
                }

            }

            if (next == null) {
                writeSingleRecord(context, curr);
                continue;
            }

            // Something wrong here and at while loop
            if (currName.equalsIgnoreCase(nextName)) {
                SAMRecord actualNext = it.next();
                context.getCounter(MapperRecords.INPUT_BAM_RECORDS)
                        .increment(1);
                writeReadEnds(context, curr, actualNext);
            }

            if (!currName.equalsIgnoreCase(nextName)) {
                writeSingleRecord(context, curr);
                continue;
            }

            /*
             * if (curr.getReadUnmappedFlag() ||
             * curr.isSecondaryOrSupplementary() || !it.hasNext()) {
             * writeSingleRecord(context, curr); } else { String prevName =
             * curr.getReadName(); SAMRecord peek = it.peek(); String nextName =
             * peek.getReadName(); if (prevName.equalsIgnoreCase(nextName)) {
             * SAMRecord next2 = it.next();
             * context.getCounter(MapperRecords.INPUT_BAM_RECORDS)
             * .increment(1); writeReadEnds(context, curr, next2); } else {
             * writeSingleRecord(context, curr); } }
             */
        }

        reader.close();
        timer.stop();
        LogPrinter.printSecondsElaped(log, "MarkDupMapper",
                timer.elapsed(TimeUnit.SECONDS));
    }

    private void writeSingleRecord(Context context, SAMRecord record)
            throws IOException, InterruptedException {

        DupSamWritable dsw = new DupSamWritable(record.getReferenceIndex(),
                getCoordinate(record), record.getReadUnmappedFlag());

        context.write(dsw, new Text(record.getSAMString()));
        context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
    }

    private void writeReadEnds(Context context, SAMRecord one, SAMRecord two)
            throws IOException, InterruptedException {
        int oneCoordinate = getCoordinate(one);
        int twoCoordinate = getCoordinate(two);

        int sequence;
        int coordinate;

        int oneReferenceIndex = one.getReferenceIndex();
        int twoReferenceIndex = two.getReferenceIndex();

        // WHAT AABOUT STRAND>>
        if (twoReferenceIndex > oneReferenceIndex
                || (twoReferenceIndex == oneReferenceIndex && twoCoordinate >= oneCoordinate)) {
            sequence = oneReferenceIndex;
            coordinate = oneCoordinate;
        } else {
            sequence = twoReferenceIndex;
            coordinate = twoCoordinate;
        }
        boolean bothUnmapped = one.getReadUnmappedFlag()
                && two.getReadUnmappedFlag();
        writeToContext(context, sequence, coordinate, bothUnmapped, one);
        writeToContext(context, sequence, coordinate, bothUnmapped, two);
        context.getCounter(MapperRecords.ARE_RECORDS_PAIRED).increment(2);
    }

    private void writeToContext(Context context, int sequence, int coordinate,
            boolean bothUnmapped, SAMRecord record) throws IOException,
            InterruptedException {
        DupSamWritable dsw = new DupSamWritable(sequence, coordinate,
                bothUnmapped);
        context.write(dsw, new Text(record.getSAMString()));
        context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
    }

    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }
}
