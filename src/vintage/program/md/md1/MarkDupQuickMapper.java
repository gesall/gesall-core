package vintage.program.md.md1;

import hdfs.DupSamWritable;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

@SuppressWarnings("deprecation")
public class MarkDupQuickMapper extends
        Mapper<Text, Text, DupSamWritable, Text> {

    static enum MapperRecords {
        BAM_RECORDS
    }

    String pathStr;

    @Override
    public void map(Text key, Text value, Context context) {
        this.pathStr = value.toString();
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
    }

    public void doWork(Context context) throws IOException,
            InterruptedException {
        // open file

        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path path = new Path(pathStr);
        FSDataInputStream in = fs.open(path);

        SAMFileReader reader = new SAMFileReader(in);
        
        PeekingIterator<SAMRecord> it = Iterators.peekingIterator(reader
                .iterator());

        while(it.hasNext()) {
            SAMRecord rec = it.next();
            if (it.hasNext()) {
                SAMRecord rec2 = it.peek();
                rec2.getAlignmentStart();
            }
            
            DupSamWritable dsw = null; // TODO new DupSamWritable(rec.getReferenceIndex(), getCoordinate(rec));
            context.write(dsw, new Text(rec.toString()));
            context.getCounter(MapperRecords.BAM_RECORDS).increment(1);
        }
        reader.close();
    }
    
    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }

    
    
}
