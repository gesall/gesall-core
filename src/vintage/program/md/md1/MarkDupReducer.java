package vintage.program.md.md1;

import hdfs.DupSamWritable;
import hdfs.SamInputStream;
import hdfs.config.CmdLineArgs;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

@SuppressWarnings("deprecation")
public class MarkDupReducer extends
        Reducer<DupSamWritable, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        REDUCE_KEYS, SAM_RECORDS, SAM_ACTUAL
    }

    String             headerFileName;
    SamInputStream     samFile;
    FSDataOutputStream reducerOutput;
    SAMFileWriter      reducerWriter;

    int                count;
    final int          countMax = 100000;
    StringBuilder      samRecords;
    int                flushCount;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samFile = new SamInputStream(conf, headerFileName);
        count = 0;
        flushCount = 0;
        samRecords = new StringBuilder();
    }

    @Override
    public void reduce(DupSamWritable key, Iterable<Text> values,
            Context context) throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.REDUCE_KEYS).increment(1);

        boolean isUnmapped = false;
        if (key.getSequence() == -1 && key.getCoordinate() == 0) {
            isUnmapped = true;
        }

        for (Text value : values) {
            context.getCounter(ReducerRecords.SAM_RECORDS).increment(1);
            String samRecord = new String(value.toString());
            samRecords.append(samRecord);
            ++count;

            if (isUnmapped) {
                if (count > countMax) {
                    doWork(context);
                }

            }

        }
        if (count > countMax) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (count > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }
    }

    private void doWork(Context context) throws IOException {
        flushCount++;
        samFile.add(samRecords);

        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        SAMFileReader sortSamInReader = samFile.getSamFileReader();
        ByteArrayOutputStream sortSamOutWriter = new ByteArrayOutputStream();
        new SortSam().instanceMainWithoutExit(sortSamArgv, sortSamInReader,
                sortSamOutWriter);
        sortSamOutWriter.flush();

        // **GC**
        samFile.clear();
        samRecords = new StringBuilder();

        byte[] markDupInBytes = sortSamOutWriter.toByteArray();

        String[] markDupArgv = CmdLineArgs.getMarkDupArgv();
        markDupArgv[4] = markDupArgv[4]
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + flushCount + ".log";
        ByteArrayOutputStream markDupOutWriter = new ByteArrayOutputStream();
        new MarkDuplicates().instanceMainWithoutExitWithMulReader(markDupArgv,
                markDupInBytes, markDupOutWriter);
        markDupOutWriter.flush();

        byte[] markDupOutBytes = markDupOutWriter.toByteArray();
        InputStream markDupOutStream = new ByteArrayInputStream(markDupOutBytes);
        SAMFileReader markDupOutReader = new SAMFileReader(markDupOutStream);

        if (reducerWriter == null) {
            SAMFileHeader reducerOutHeader = markDupOutReader.getFileHeader();
            reducerOutHeader.setSortOrder(SortOrder.unsorted);
            createSingletonWriter(context, reducerOutHeader);
        }

        for (SAMRecord record : markDupOutReader) {
            reducerWriter.addAlignment(record);
        }
        markDupOutReader.close();

        samFile.clear();
        count = 0;
        samRecords = new StringBuilder();

    }

    private void createSingletonWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        String outputBamName = "hdfs://yeeha.yeeha:9000/user/aroy/md_out/reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + ".bam";
        Path path = new Path(outputBamName);
        reducerOutput = fs.create(path);

        reducerWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                reducerOutput);

    }
}
