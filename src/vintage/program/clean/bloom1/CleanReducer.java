package vintage.program.clean.bloom1;

import hdfs.SamInputStream;
import hdfs.bloom.GenomicPositionKey;
import hdfs.config.CmdLineArgs;
import hdfs.config.PropertyReader;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import picard.sam.FixMateInformation;

import com.google.common.base.Stopwatch;
import com.google.common.hash.BloomFilter;

@SuppressWarnings("deprecation")
public class CleanReducer extends
        Reducer<Text, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SHUFFLED_SAM_IDS, OUTPUT_BAM_RECORDS, BLOOM_INSERTS
    }

    private static final Log        log                = LogFactory
                                                               .getLog(CleanReducer.class);

    String                          headerFileName;
    SamInputStream                  samFile;
    FSDataOutputStream              reducerOutput;
    SAMFileWriter                   reducerWriter;

    // records to hold in memory
    int                             recordsInMemory;
    final int                       MAX_RECORDS_MEMORY = 100000;
    StringBuilder                   samRecords;

    // records to write in a output file
    // influenced by io.sort.mb of the next mapper
    // final long MAX_RECORDS_FILE = 10000000;
    // Before new partitioning function in Mark Duplicates 5000000
    // final long MAX_RECORDS_FILE = 5000000;

    // final long MAX_RECORDS_FILE = 5000000; // best value
    // = 20000000; for 2GB output files
    long                            MAX_RECORDS_FILE;
    int                             fileCount          = 0;
    long                            recordsInFile;
    boolean                         toFlush;

    String[]                        fixMateInfoArgs;

    // time
    Stopwatch                       reducerTimer;
    Stopwatch                       fixMateTimer;

    BloomFilter<GenomicPositionKey> bloomFilter;

    @Override
    public void setup(Context context) throws IOException {
        reducerTimer = Stopwatch.createUnstarted();
        fixMateTimer = Stopwatch.createUnstarted();

        Configuration conf = context.getConfiguration();
        MAX_RECORDS_FILE = conf.getInt("numRecordsInOutputFile", -1);

        fixMateInfoArgs = CmdLineArgs.getFixMateArgv();
        headerFileName = conf.get("BamInput");
        // String headerAppendStr =
        // "@RG\tID:gesall\tPL:ILLUMINA\tPU:dummy\tLB:gesall\tSM:full\tCN:GDL\n";
        String headerAppendStr = getHeaderAppendString(conf);
        samFile = new SamInputStream(conf, headerFileName, headerAppendStr);
        recordsInMemory = 0;
        recordsInFile = 0;
        toFlush = false;
        samRecords = new StringBuilder();
        
        int expectedInsertions = 25*1000000; //(20 * 1000000) / context.getNumReduceTasks();
        bloomFilter = BloomFilter.create(new GenomicPositionKey(),
                expectedInsertions);

    }

    private void flushBloomFilter(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path bloomFilePath = new Path(getBloomFilterFileName(context));
        FSDataOutputStream bloomOutStream = fs.create(bloomFilePath);
        bloomFilter.writeTo(bloomOutStream);
        IOUtils.closeStream(bloomOutStream);
    }

    private String getBloomFilterFileName(Context context) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "b-" + context.getTaskAttemptID().getTaskID().getId()
                + "-" + context.getTaskAttemptID().getId() + ".bloom";
        return outputDir + "/" + fileName;
    }

    private String getHeaderAppendString(Configuration conf) {
        PropertyReader properties = new PropertyReader(conf);
        StringBuilder sb = new StringBuilder();
        sb.append("@RG\tID:" + properties.getValue("pg.addrepl.RGID"));
        sb.append("\tPL:" + properties.getValue("pg.addrepl.RGPL"));
        sb.append("\tPU:" + properties.getValue("pg.addrepl.RGPU"));
        sb.append("\tLB:" + properties.getValue("pg.addrepl.RGLB"));
        sb.append("\tSM:" + properties.getValue("pg.addrepl.RGSM"));
        sb.append("\tCN:" + properties.getValue("pg.addrepl.RGCN"));
        sb.append("\n");
        return sb.toString();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.SHUFFLED_SAM_IDS).increment(1);
        for (Text value : values) {
            String samRecord = new String(value.toString());
            samRecords.append(samRecord);
            ++recordsInMemory;
        }
        if (recordsInMemory > MAX_RECORDS_MEMORY) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (recordsInMemory > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }

        flushBloomFilter(context);
        
        log.info("[CPRO] [ReduceTask] "
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId());
        LogPrinter.printSecondsElaped(log, "FixMateInformation",
                fixMateTimer.elapsed(TimeUnit.SECONDS));
        LogPrinter.printSecondsElaped(log, "CleanQuickReducer",
                reducerTimer.elapsed(TimeUnit.SECONDS));
    }

    private void doWork(Context context) throws IOException,
            InterruptedException {

        reducerTimer.start();

        samFile.add(samRecords);
        SAMFileReader fixMateInput = samFile.getSamFileReader();

        byte[] fixMateOutput = runFixMateInfo(fixMateInput);

        // write BAM file
        InputStream inputStream = new ByteArrayInputStream(fixMateOutput);
        SAMFileReader fixMateOutputReader = new SAMFileReader(inputStream);
        if (reducerWriter == null || recordsInFile > MAX_RECORDS_FILE) {

            if (reducerWriter != null) {
                reducerWriter.close();
            }
            if (reducerOutput != null) {
                reducerOutput.close();
            }

            Configuration conf = context.getConfiguration();
            FileSystem fs = FileSystem.get(conf);
            String outputBamName = getNextFileName(context, fileCount);

            Path path = new Path(outputBamName);
            reducerOutput = fs.create(path);

            reducerWriter = new SAMFileWriterFactory().makeBAMWriter(
                    fixMateOutputReader.getFileHeader(), true, reducerOutput);

            recordsInFile = 0;
            ++fileCount;
        }

        for (SAMRecord record : fixMateOutputReader) {
            reducerWriter.addAlignment(record);
            // can include numbers from killed jobs
            context.getCounter(ReducerRecords.OUTPUT_BAM_RECORDS).increment(1);

            // Save for BloomFilter
            writeToBloomFilter(record, context);

        }
        fixMateOutputReader.close();

        recordsInFile += recordsInMemory;
        cleanUp();

        reducerTimer.stop();

    }

    private void writeToBloomFilter(SAMRecord record, Context context) throws IOException {
        if (record.getMateUnmappedFlag()) {
            String reference = record.getReferenceName();
            int unclippedPos = getCoordinate(record);
            GenomicPositionKey key = new GenomicPositionKey(reference,
                    unclippedPos);
            bloomFilter.put(key);
            context.getCounter(ReducerRecords.BLOOM_INSERTS).increment(1);

        }

    }

    private int getCoordinate(SAMRecord rec) {
        int retVal = rec.getReadNegativeStrandFlag() ? rec.getUnclippedEnd()
                : rec.getUnclippedStart();
        return retVal;
    }

    private byte[] runFixMateInfo(SAMFileReader input) throws IOException {
        ByteArrayOutputStream outputWriter = new ByteArrayOutputStream();

        fixMateTimer.start();
        new FixMateInformation().instanceMainWithoutExit(fixMateInfoArgs,
                input, outputWriter);
        fixMateTimer.stop();
        outputWriter.flush();
        byte[] output = outputWriter.toByteArray();
        return output;
    }

    private String getNextFileName(Context context, int fileCount) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + fileCount + ".bam";
        return outputDir + "/" + fileName;
    }

    private void cleanUp() {
        samFile.clear();
        recordsInMemory = 0;
        samRecords = new StringBuilder();
    }

}
