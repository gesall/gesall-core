package vintage.program.clean.clean1;

import hdfs.SamInputStream;
import hdfs.config.CmdLineArgs;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import picard.sam.FixMateInformation;

@SuppressWarnings("deprecation")
public class OldCleanReducer extends
        Reducer<Text, Text, IntWritable, BytesWritable> {
    static enum ReducerRecords {
        SAM_RECORDS, PAIRS, FM_RECORDS
    }

    String         headerFileName;
    SamInputStream samFile;
    FSDataOutputStream reducerOutput;
    SAMFileWriter  reducerWriter;

    int            count;
    final int      countMax = 100000;
    StringBuilder  samRecords;
    int            flushCount;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samFile = new SamInputStream(conf, headerFileName);
        count = 0;
        flushCount = 0;
        samRecords = new StringBuilder();
    }

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.PAIRS).increment(1);
        for (Text value : values) {
            String samRecord = new String(value.toString());
            samRecords.append(samRecord);
            ++count;
        }
        if (count > countMax) {
            doWork(context);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
        reducerWriter.close();
        reducerOutput.close();
    }

    private void doWork(Context context) throws IOException,
            InterruptedException {
        flushCount++;
        samFile.add(samRecords);

        String[] argv1 = CmdLineArgs.getFixMateArgv();
        SAMFileReader samReaderInput1 = samFile.getSamFileReader();
        ByteArrayOutputStream bamWriterInput1 = new ByteArrayOutputStream();

        new FixMateInformation().instanceMainWithoutExit(argv1,
                samReaderInput1, bamWriterInput1);

        bamWriterInput1.flush();
        /*
        byte[] bytes1 = bamWriterInput1.toByteArray();
        
        InputStream inputStream2 = new ByteArrayInputStream(bytes1);
        SAMFileReader testReader1 = new SAMFileReader(inputStream2);
        for (SAMRecord record : testReader1) {
            record.getAlignmentStart();
            context.getCounter(ReducerRecords.FM_RECORDS).increment(1);
        }
        context.write(new IntWritable(flushCount), new BytesWritable(bytes1));
        testReader1.close();
        */
        // write BAM file

        byte[] bytes2 = bamWriterInput1.toByteArray();
        InputStream inputStream3 = new ByteArrayInputStream(bytes2);
        SAMFileReader testReader2 = new SAMFileReader(inputStream3);
        if (reducerWriter == null) {
            Configuration conf = new Configuration();
            FileSystem fs = FileSystem.get(conf);
            String outputBamName = "hdfs://yeeha.yeeha:9000/user/aroy/clean_out/reduce-"
                    + context.getTaskAttemptID().getTaskID().getId() + ".bam";
            Path path = new Path(outputBamName);
            reducerOutput = fs.create(path);

            reducerWriter = new SAMFileWriterFactory().makeBAMWriter(
                    testReader2.getFileHeader(), true, reducerOutput);
        }
       
        for (SAMRecord record : testReader2) {
            reducerWriter.addAlignment(record);
        }
        testReader2.close();

        cleanUp();

    }

    private void cleanUp() {
        samFile.clear();
        count = 0;
        samRecords = new StringBuilder();
    }

}
