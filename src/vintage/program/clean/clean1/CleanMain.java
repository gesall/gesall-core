package vintage.program.clean.clean1;

import hdfs.BamInputFormat;
import hdfs.config.PropertyReader;
import hdfs.utils.BamPathFilter;
import hdfs.utils.FSMethods;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * MR program to perform AddOrReplaceReadGroups, CleanSam in mapper and
 * FixMateInformation in reducer.
 * 
 * Run with -h for details.
 * 
 */
public class CleanMain extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String propertyFile = null;
        String inputPath = null;
        String outputPath = null;
        boolean debugMode = false;
        int numReducers = -1;
        int numRecordsInOutputFile = -1;
        try {
            options = parser.parse(args);
            propertyFile = (String) options.valueOf("p");
            inputPath = (String) options.valueOf("i");
            outputPath = (String) options.valueOf("o");
            numReducers = (Integer) options.valueOf("r");
            numRecordsInOutputFile = (Integer) options.valueOf("n");
            debugMode = options.has("d");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        // Set configuration parameters
        Configuration conf = getConf();
        PropertyReader properties = new PropertyReader(conf);
        properties.setProperties(propertyFile);
        String aBamFile = FSMethods.getAnyBamFile(conf, inputPath);
        conf.set("BamInput", aBamFile);
        conf.set("OutputDir", outputPath);
        conf.setBoolean("DebugMode", debugMode);
        conf.setInt("numRecordsInOutputFile", numRecordsInOutputFile);
        conf.set("mapreduce.job.user.classpath.first", "true");

        /*
        conf.setBoolean("mapreduce.map.output.compress", true);
        conf.set("mapreduce.map.output.compress.codec",
                "org.apache.hadoop.io.compress.SnappyCodec");
        */
        
        // Set job parameters
        Job job = Job.getInstance(conf);
        job.setJobName("Picard Clean 1");
        job.setJarByClass(CleanMain.class);

        FileSystem fs = FileSystem.get(conf);
        FileStatus[] statuses = fs.listStatus(new Path(inputPath),
                new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setInputFormatClass(BamInputFormat.class);
        job.setMapperClass(CleanMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setReducerClass(CleanQuickReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(numReducers);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("p:i:o:r:n:dh*");
        parser.accepts("p", "Property file on local disk").withRequiredArg().required()
                .ofType(String.class);
        parser.accepts("i", "Input BAM directory on HDFS").withRequiredArg().required()
                .ofType(String.class);
        parser.accepts("o", "Output directory on HDFS").withRequiredArg().required()
                .ofType(String.class);
        parser.accepts("r", "Number of reducers").withRequiredArg().required()
                .ofType(Integer.class);
        parser.accepts("n", "Number of records per output file").withRequiredArg().required()
        .ofType(Integer.class);
        parser.accepts("d", "Debug mode");
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new CleanMain(), args);
        System.exit(res);
    }

}
