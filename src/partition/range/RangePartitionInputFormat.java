package partition.range;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class RangePartitionInputFormat extends
        FileInputFormat<RangePartitionKey, Text> {

    @Override
    public RecordReader<RangePartitionKey, Text> createRecordReader(
            InputSplit split, TaskAttemptContext context) throws IOException,
            InterruptedException {
        return new RangePartitionRecordReader();
    }

}
