package partition.range;

import hdfs.MemoryBamManager;
import htsjdk.samtools.BAMIndexer;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.ValidationStringency;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

//import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class RangePartitionReducer extends
        Reducer<RangePartitionKey, BytesWritable, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SHUFFLED_KEYS, SHUFFLED_BAM_RECORDS, OUTPUT_BAM_RECORDS
    }

    private static final Log log                = LogFactory
                                                        .getLog(RangePartitionReducer.class);

    String                   headerFileName;
    FSDataOutputStream       reducerOutput;
    SAMFileWriter            reducerWriter;

    // records to hold in memory
    int                      recordsInMemory;
    final int                MAX_RECORDS_MEMORY = 100000;

    MemoryBamManager         inMemoryBam;

    // Min - Max chr:pos for a reducer output
    private SpanMaintainer   span;

    @Override
    public void setup(Context context) throws IOException {
        // reducerTimer = Stopwatch.createUnstarted();
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        recordsInMemory = 0;
        inMemoryBam = new MemoryBamManager(conf, headerFileName,
                MAX_RECORDS_MEMORY + 1000);
        createSingletonWriter(context);
        span = new SpanMaintainer();
    }

    @Override
    public void reduce(RangePartitionKey key, Iterable<BytesWritable> values,
            Context context) throws InterruptedException, IOException {
        span.addSpan(key); // send each key for maintaining chr spans

        context.getCounter(ReducerRecords.SHUFFLED_KEYS).increment(1);
        for (BytesWritable value : values) {
            context.getCounter(ReducerRecords.SHUFFLED_BAM_RECORDS)
                    .increment(1);
            byte[] record = value.copyBytes();
            inMemoryBam.addRecord(record);
            ++recordsInMemory;
        }
        if (recordsInMemory > MAX_RECORDS_MEMORY) {
            doWork(context);
        }
    }

    private void doWork(Context context) throws IOException,
            InterruptedException {
        // reducerTimer.start();
        SamReader reader = inMemoryBam.getSamReader();
        for (SAMRecord record : reader) {
            reducerWriter.addAlignment(record);
            context.getCounter(ReducerRecords.OUTPUT_BAM_RECORDS).increment(1);
        }
        // cleanup
        inMemoryBam.clear();
        recordsInMemory = 0;
        // reducerTimer.stop();
    }

    private void createSingletonWriter(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        SAMFileHeader header = MemoryBamManager.getHeader(conf, headerFileName);
        header.setSortOrder(SortOrder.coordinate);

        FileSystem fs = FileSystem.get(conf);
        String outputBamName = getFileName(context);
        Path path = new Path(outputBamName);
        reducerOutput = fs.create(path);
        reducerWriter = new SAMFileWriterFactory().makeBAMWriter(header, true,
                reducerOutput);
    }

    private String getFileName(Context context) {
        String suffix = "sorted";
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        return outputDir + "/" + fileName;
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        if (recordsInMemory > 0) {
            doWork(context);
        }
        if (reducerWriter != null) {
            reducerWriter.close();
        }
        if (reducerOutput != null) {
            reducerOutput.close();
        }

        // reducerTimer.start();
        // Stopwatch indexTimer = Stopwatch.createStarted();
        buildIndex(context);
        // indexTimer.stop();
        // reducerTimer.stop();

        writeSpan(context);

        log.info("[CPRO] [ReduceTask] "
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId());
        // LogPrinter.printSecondsElaped(log, "RangePartitionReducer",
        // reducerTimer.elapsed(TimeUnit.SECONDS));
        // LogPrinter.printSecondsElaped(log, "RangePartitionIndexer",
        // indexTimer.elapsed(TimeUnit.SECONDS));

    }

    private void writeSpan(Context context) throws IOException {
        String bamFileName = getFileName(context);
        String spanFileName = bamFileName + ".span";
        span.writeToFile(context.getConfiguration(), spanFileName);
    }

    private void buildIndex(Context context) throws IOException {
        String bamFileName = getFileName(context);
        String bamIndexFileName = bamFileName + ".bai";

        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        Path bamFilePath = new Path(bamFileName);
        FSDataInputStream in = fs.open(bamFilePath);
        DataInputStream dis = new DataInputStream(in);
        SAMFileReader reader = new SAMFileReader(dis);
        reader.setValidationStringency(ValidationStringency.LENIENT);
        Path bamIndexPath = new Path(bamIndexFileName);
        FSDataOutputStream out = fs.create(bamIndexPath);
        DataOutputStream dout = new DataOutputStream(out);

        BAMIndexer.createIndex(reader, dout);
        dout.close();
        out.close();
    }

}
