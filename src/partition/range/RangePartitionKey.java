package partition.range;

import htsjdk.samtools.SAMRecord;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

/**
 * Range partitioning keys
 * 
 */
public class RangePartitionKey implements WritableComparable<RangePartitionKey> {

    IntWritable     refIndex;
    IntWritable     alignmentStart;
    BooleanWritable negativeStrandFlag;
    Text            readName;

    public RangePartitionKey() {
        this.refIndex = new IntWritable();
        this.alignmentStart = new IntWritable();
        this.negativeStrandFlag = new BooleanWritable();
        this.readName = new Text();
    }
    
    public IntWritable getRefIndex() {
        return refIndex;
    }
    
    public IntWritable getPosition() {
        return alignmentStart;
    }
    
    public RangePartitionKey(SAMRecord record) {
        this.refIndex = new IntWritable(record.getReferenceIndex());
        this.alignmentStart = new IntWritable(record.getAlignmentStart());
        this.negativeStrandFlag = new BooleanWritable(record.getReadNegativeStrandFlag());
        this.readName = new Text(record.getReadName());
    }
    
    @Override
    public void readFields(DataInput in) throws IOException {
        refIndex.readFields(in);
        alignmentStart.readFields(in);
        negativeStrandFlag.readFields(in);
        readName.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        refIndex.write(out);
        alignmentStart.write(out);
        negativeStrandFlag.write(out);
        readName.write(out);
    }

    // Hadoop IO implementation of comparator is same as
    // SAMRecordCoordinateComparator which returns
    // -1, 0, 1
    @Override
    public int compareTo(RangePartitionKey other) {
        int cmp = refIndex.compareTo(other.refIndex);
        if (cmp != 0) {
            return cmp;
        }
        cmp = alignmentStart.compareTo(other.alignmentStart);
        if (cmp != 0) {
            return cmp;
        }
        cmp = negativeStrandFlag.compareTo(other.negativeStrandFlag);
        if (cmp != 0) {
            return cmp;
        }
        cmp = readName.compareTo(other.readName);
        return cmp;
    }

}
