package partition.range;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import hdfs.BamInputFormat;
import hdfs.config.PropertyReader;
import hdfs.utils.BamPathFilter;
import hdfs.utils.FSMethods;
import hdfs.utils.LogPrinter;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Range partition read data
 * 
 */
public class RangePartitionMain extends Configured implements Tool {

    private static final Log log = LogFactory.getLog(RangePartitionMain.class);

    @Override
    public int run(String[] args) throws Exception {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String propertyFile = null;
        String inputPath = null;
        String outputPath = null;
        boolean debugMode = false;
        int numReducers = -1;
        double probKey = -1;
        int numSamples = -1;
        int numSplits = -1;
        try {
            options = parser.parse(args);
            propertyFile = (String) options.valueOf("p");
            inputPath = (String) options.valueOf("i");
            outputPath = (String) options.valueOf("o");
            numReducers = (Integer) options.valueOf("r");
            debugMode = options.has("d");
            probKey = (Double) options.valueOf("a1");
            numSamples = (Integer) options.valueOf("a2");
            numSplits = (Integer) options.valueOf("a3");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        // Set configuration parameters
        Configuration conf = getConf();
        PropertyReader properties = new PropertyReader(conf);
        properties.setProperties(propertyFile);
        String aBamFile = FSMethods.getAnyBamFile(conf, inputPath);
        conf.set("BamInput", aBamFile);
        conf.set("OutputDir", outputPath);
        conf.setBoolean("DebugMode", debugMode);
        conf.set("mapreduce.job.user.classpath.first", "true");

        // Set job parameters
        Job job = Job.getInstance(conf);
        job.setJobName("Range partitoning records");
        job.setJarByClass(RangePartitionMain.class);
        job.setNumReduceTasks(numReducers);

        FileSystem fs = FileSystem.get(conf);
        FileStatus[] statuses = fs.listStatus(new Path(inputPath),
                new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setInputFormatClass(BamInputFormat.class);
        job.setMapperClass(RangePartitionMapper.class);
        job.setMapOutputKeyClass(RangePartitionKey.class);
        job.setMapOutputValueClass(BytesWritable.class);
        job.setReducerClass(RangePartitionReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);

        // Range partitioner
        // Stopwatch subTimer = Stopwatch.createStarted();
        long t1 = System.nanoTime();

        job.setPartitionerClass(TotalOrderPartitioner.class);
        // Set input format temporarily
        job.setInputFormatClass(RangePartitionInputFormat.class);
        // key select probability, maximum number of samples, maximum number of
        // splits to sample
        // 0.1, 100000, 100 <- deployment configuration
        // 0.05, 100000, 100
        // 0.1, 100, 10 <- testing
        InputSampler.Sampler<RangePartitionKey, Text> sampler = new InputSampler.RandomSampler<RangePartitionKey, Text>(
                probKey, numSamples, numSplits);
        
        InputSampler.writePartitionFile(job, sampler);
        String partitionFile = TotalOrderPartitioner.getPartitionFile(conf);
        URI partitionUri = new URI(partitionFile);
        job.addCacheFile(partitionUri);

        long t2 = System.nanoTime();
        LogPrinter.printSecondsElaped(log, "RangePartitionSampling",
                TimeUnit.NANOSECONDS.toSeconds(t2 - t1));

        // reset input format
        job.setInputFormatClass(BamInputFormat.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("p:i:o:r:dh*");
        parser.accepts("p", "Property file on local disk").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("i", "Input BAM directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("o", "Output directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("r", "Number of reducers").withRequiredArg().required()
                .ofType(Integer.class);
        parser.accepts("d", "Debug mode");
        
        parser.accepts("a1", "Key selection probability").withRequiredArg().required()
        .ofType(Double.class);
        parser.accepts("a2", "Maximum number of samples").withRequiredArg().required()
        .ofType(Integer.class);
        parser.accepts("a3", "Maximum number of splits to sample").withRequiredArg().required()
        .ofType(Integer.class);
        
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(),
                new RangePartitionMain(), args);
        System.exit(res);
    }

}
