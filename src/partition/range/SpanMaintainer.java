package partition.range;

import hdfs.MemoryBamManager;
import htsjdk.samtools.SAMFileHeader;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class SpanMaintainer {

    SpanKey       end;
    List<SpanKey> spans;
    Set<Integer>  references;

    public SpanMaintainer() {
        spans = new ArrayList<SpanKey>();
        references = new HashSet<Integer>();
    }

    public void addSpan(RangePartitionKey ikey) {
        SpanKey key = new SpanKey(ikey.getRefIndex().get(), ikey.getPosition()
                .get());
        int refIndex = key.getRefIndex();
        if (references.contains(refIndex)) {
            end = key;
        } else {
            if (end != null) {
                spans.add(end);
            }
            end = null;
            spans.add(key);
            references.add(refIndex);
        }
    }

    private void writeString(Writer writer, String chr, int start, int end, int chrLength)
            throws IOException {
        writer.write(chr + ":" + start + "-" + end + "-" + chrLength + "\n");
    }

    @SuppressWarnings("unused")
    private void writeSpan(Writer writer) throws IOException {
        for (int i = 0; i < spans.size(); ++i) {
            writer.write("S" + "\t" + spans.get(i).getRefIndex() + "\t"
                    + spans.get(i).getPosition() + "\n");
        }
    }

    public void writeToFile(Configuration conf, String spanFileName)
            throws IOException {
        if (end != null) {
            spans.add(end);
        }

        FileSystem fs = FileSystem.get(conf);
        Path spanFilePath = new Path(spanFileName);
        FSDataOutputStream out = fs.create(spanFilePath);
        PrintWriter writer = new PrintWriter(out);

        String headerFileName = conf.get("BamInput");
        SAMFileHeader header = MemoryBamManager.getHeader(conf, headerFileName);

        // writeSpan(writer);

        for (int i = 0; i < spans.size(); ++i) {
            SpanKey currentSpan = spans.get(i);
            int currentChrIndex = currentSpan.getRefIndex();
            String chrName;
            int startPosition = -1;
            int endPosition = -1;
            int chrLength = header.getSequence(currentChrIndex).getSequenceLength();
            if (i == spans.size() - 1) {
                // this is the last span
                // print from current span's chr start position to the span's
                // position;
                chrName = header.getSequence(currentChrIndex).getSequenceName();
                startPosition = 0;
                endPosition = currentSpan.getPosition();
            } else {
                // find if next span is of same chr
                SpanKey nextSpan = spans.get(i + 1);
                int nextChrIndex = nextSpan.getRefIndex();
                boolean sameChr = (currentChrIndex == nextChrIndex);
                if (sameChr) {
                    // if next span has same chr
                    // print the range from this span's position to next span's
                    // position
                    // increment i to skip next span
                    chrName = header.getSequence(currentChrIndex)
                            .getSequenceName();
                    startPosition = currentSpan.getPosition();
                    endPosition = nextSpan.getPosition();
                    i = i + 1; // extra increment
                } else {
                    // if next span has different chr
                    // print from current span's position to end of this chr
                    chrName = header.getSequence(currentChrIndex)
                            .getSequenceName();
                    startPosition = currentSpan.getPosition();
                    endPosition = header.getSequence(currentChrIndex)
                            .getSequenceLength();
                }
            }
            // System.out.println(chrName + ":" + startPosition + "-" +
            // endPosition);
            writeString(writer, chrName, startPosition, endPosition, chrLength);
        }
        writer.close();
        out.close();
    }

}

class SpanKey {

    private int refIndex;
    private int position;

    public SpanKey(int oRefIndex, int oPosition) {
        setRefIndex(oRefIndex);
        setPosition(oPosition);
    }

    public int getRefIndex() {
        return refIndex;
    }

    public void setRefIndex(int refIndex) {
        this.refIndex = refIndex;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}