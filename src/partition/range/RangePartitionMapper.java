package partition.range;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.BAMRecordEncoder;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class RangePartitionMapper extends
        Mapper<LongWritable, BytesWritable, RangePartitionKey, BytesWritable> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_BAM_RECORDS
    }

    private static final Log      log                  = LogFactory
                                                               .getLog(RangePartitionMapper.class);

    private BamRecordEnumeration  samEnum;
    private String                headerFileName;
    private List<BamBlockWrapper> bamBlocks;

    private final int             recordLengthEstimate = 300;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samEnum = new BamRecordEnumeration(context.getConfiguration(),
                headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {

        Stopwatch timer = Stopwatch.createStarted();
        SAMFileReader reader = samEnum.getSAMFileReader(bamBlocks);
        for (SAMRecord record : reader) {
            context.getCounter(MapperRecords.INPUT_BAM_RECORDS).increment(1);
            if (record.getReadUnmappedFlag()) { // ignoring unmapped reads for now
                continue;
            }
            
            /*
            if (record.getReferenceName().contains("GL") || record.getReferenceName().contains("MT")) {
                continue;
            }
            */

            RangePartitionKey key = new RangePartitionKey(record);
            BAMRecordEncoder encoder = new BAMRecordEncoder(record,
                    recordLengthEstimate);
            byte[] recordBytes = encoder.encode();
            BytesWritable value = new BytesWritable(recordBytes);
            context.write(key, value);
            context.getCounter(MapperRecords.SHUFFLED_BAM_RECORDS).increment(1);
        }
        
        timer.stop();
        LogPrinter.printSecondsElaped(log, "RangePartitionMapper",
                timer.elapsed(TimeUnit.SECONDS));
    }

}
