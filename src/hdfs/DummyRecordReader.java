package hdfs;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class DummyRecordReader extends RecordReader<Text, Text>{

    private String key = null;
    private String value = null;
    private boolean returned = false;
    
    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)
            throws IOException, InterruptedException {
        final Path file = ((FileSplit) genericSplit).getPath();
        key = file.getName();
        value = file.toUri().getPath();
    }
    
    @Override
    public void close() throws IOException {        
    }

    @Override
    public Text getCurrentKey() throws IOException,
            InterruptedException {
        returned = true;
        return new Text(key);
    }

    @Override
    public Text getCurrentValue() throws IOException,
            InterruptedException {
        returned = true;
        return new Text(value);
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return 0;
    }


    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        return (!returned);
    }

}
