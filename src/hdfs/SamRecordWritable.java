package hdfs;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class SamRecordWritable implements WritableComparable<SamRecordWritable> {

    // TODO: Change key text to bytes(?) for performance
    private Text key;  // read group
    private Text value;

    public SamRecordWritable(String key, String value) {
        this.key = new Text(key);
        this.value = new Text(value);
    }

    public Text getKey() {
        return key;
    }

    public Text getValue() {
        return value;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        key.readFields(in);
        value.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        key.write(out);
        value.write(out);
    }

    @Override
    public int hashCode() {
        return key.hashCode() * 163 + value.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SamRecordWritable) {
            SamRecordWritable sr = (SamRecordWritable) o;
            return key.equals(sr.getKey()) && value.equals(sr.getValue());
        }
        return false;
    }

    @Override
    public String toString() {
        return key + "\t" + value;
    }

    @Override
    public int compareTo(SamRecordWritable sr) {
        int cmp = key.compareTo(sr.getKey());
        if (cmp != 0) {
            return cmp;
        }
        return value.compareTo(sr.getValue());
    }

}
