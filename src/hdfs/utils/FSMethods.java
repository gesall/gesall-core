package hdfs.utils;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class FSMethods {

    public static String getAnyBamFile(Configuration conf, String bamDirectory)
            throws IOException {
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatuses = fs.listStatus(new Path(bamDirectory),
                new BamPathFilter(conf));
        String aBamFile = "";
        if (fileStatuses.length > 0) {
            aBamFile = fileStatuses[0].getPath().toUri().toString();
        }
        return aBamFile;
    }
    
}
