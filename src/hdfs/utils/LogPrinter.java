package hdfs.utils;

import org.apache.commons.logging.Log;

public class LogPrinter {

    public static void printSecondsElaped(Log log, String funcName, long time) {
        log.info("[CPRO] [TIME] [" + funcName + "] [in seconds]: " + time);
    }

    public static void printDoubleValue(Log log, String name, double value) {
        log.info("[CPRO] [VALUE] [" + name + "]: " + value);
    }

}
