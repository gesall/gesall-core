package hdfs.utils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class MarkDuplicatesShuffleKey implements WritableComparable<MarkDuplicatesShuffleKey> {

    public IntWritable     sequence;
    public IntWritable     coordinate;
    public BooleanWritable bothUnmapped;
    // true for paired duplicates and optical duplicates
    // false for fragment duplicates
    public BooleanWritable forPairedDups;

    public MarkDuplicatesShuffleKey() {
        this.sequence = new IntWritable();
        this.coordinate = new IntWritable();
        this.bothUnmapped = new BooleanWritable();
        this.forPairedDups = new BooleanWritable();
    }

    public MarkDuplicatesShuffleKey(int sequence, int coordinate, boolean bothUnmapped,
            boolean isForPairedDups) {
        this.sequence = new IntWritable(sequence);
        this.coordinate = new IntWritable(coordinate);
        this.bothUnmapped = new BooleanWritable(bothUnmapped);
        this.forPairedDups = new BooleanWritable(isForPairedDups);
    }

    public int getSequence() {
        return sequence.get();
    }

    public int getCoordinate() {
        return coordinate.get();
    }

    public boolean areBothUnmapped() {
        return bothUnmapped.get();
    }

    public boolean isForPairedDups() {
        return forPairedDups.get();
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        sequence.readFields(in);
        coordinate.readFields(in);
        bothUnmapped.readFields(in);
        forPairedDups.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        sequence.write(out);
        coordinate.write(out);
        bothUnmapped.write(out);
        forPairedDups.write(out);
    }

    @Override
    public int compareTo(MarkDuplicatesShuffleKey mdKey) {
        int cmp = coordinate.compareTo(mdKey.coordinate);
        if (cmp == 0) {
            cmp = sequence.compareTo(mdKey.sequence);
            if (cmp == 0) {
                cmp = bothUnmapped.compareTo(mdKey.bothUnmapped);
                if (cmp == 0) {
                    cmp = forPairedDups.compareTo(mdKey.forPairedDups);
                }
            }
        }
        return cmp;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MarkDuplicatesShuffleKey) {
            MarkDuplicatesShuffleKey mdKey = (MarkDuplicatesShuffleKey) o;
            return coordinate.equals(mdKey.coordinate)
                    && sequence.equals(mdKey.sequence)
                    && bothUnmapped.equals(mdKey.bothUnmapped)
                    && forPairedDups.equals(mdKey.forPairedDups);
        }
        return false;
    }

    @Override
    public String toString() {
        return sequence + ":" + coordinate + ":" + bothUnmapped + ":"
                + forPairedDups;
    }

    @Override
    public int hashCode() {
        return (sequence.hashCode() * 257) + (coordinate.hashCode() * 163)
                + (bothUnmapped.hashCode() * 17) + forPairedDups.hashCode();
    }

}
