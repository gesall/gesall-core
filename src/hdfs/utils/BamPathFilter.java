package hdfs.utils;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;

/**
 * Returns only BAM files in the input directoy.
 */
public class BamPathFilter implements PathFilter {

    FileSystem fileSystem;

    public BamPathFilter(Configuration conf) {
        try {
            fileSystem = FileSystem.get(conf);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public boolean accept(Path path) {
        try {
            if (fileSystem.isDirectory(path)) {
                return true;
            }
            return path.getName().endsWith(".bam");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
