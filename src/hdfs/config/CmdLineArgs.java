package hdfs.config;

import org.apache.hadoop.conf.Configuration;

/**
 * The set of command-line arguments for the workflow steps.
 */
public class CmdLineArgs {

    public static String[] getAddRplArgs(Configuration conf) {
        PropertyReader properties = new PropertyReader(conf);
        String[] argv = new String[9];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "VALIDATION_STRINGENCY=LENIENT";
        argv[3] = "RGID=" + properties.getValue("pg.addrepl.RGID");
        argv[4] = "RGLB=" + properties.getValue("pg.addrepl.RGLB");
        argv[5] = "RGPU=" + properties.getValue("pg.addrepl.RGPU");
        argv[6] = "RGPL=" + properties.getValue("pg.addrepl.RGPL");
        argv[7] = "RGSM=" + properties.getValue("pg.addrepl.RGSM");
        argv[8] = "RGCN=" + properties.getValue("pg.addrepl.RGCN");

        return argv;
    }

    public static String[] getCleanSamArgv() {
        String[] argv = new String[3];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "VALIDATION_STRINGENCY=LENIENT";

        return argv;
    }

    public static String[] getFixMateArgv() {
        String[] argv = new String[4];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "VALIDATION_STRINGENCY=LENIENT";
        argv[3] = "ASSUME_SORTED=TRUE";

        return argv;
    }

    public static String[] getMarkDupArgv() {
        String[] argv = new String[5];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "VALIDATION_STRINGENCY=LENIENT";
        argv[3] = "ASSUME_SORTED=TRUE";
        argv[4] = ""; // "METRICS_FILE=" Completed by the calling code

        return argv;
    }

    public static String[] getSortSamArgv() {
        String[] argv = new String[4];
        argv[0] = "INPUT=dummy";
        argv[1] = "OUTPUT=dummy";
        argv[2] = "SORT_ORDER=coordinate";
        argv[3] = "VALIDATION_STRINGENCY=LENIENT";
        return argv;
    }

}
