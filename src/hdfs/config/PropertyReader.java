package hdfs.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;

public class PropertyReader {

    Configuration conf;

    public PropertyReader(Configuration conf) {
        this.conf = conf;
    }

    // can also do conf.addResource
    public void setProperties(String fileName) throws IOException {
        Properties props = new Properties();
        FileInputStream in = new FileInputStream(fileName);
        props.load(in);
        in.close();
        for (String key : props.stringPropertyNames()) {
            conf.set(key, props.getProperty(key));
        }
    }

    public String getValue(String key) {
        return conf.get(key);
    }

}
