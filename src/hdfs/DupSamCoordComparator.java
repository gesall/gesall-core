package hdfs;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class DupSamCoordComparator extends WritableComparator {

    public DupSamCoordComparator() {
        super(DupSamWritable.class, true);
    }
    
    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        DupSamWritable one = (DupSamWritable) b;
        DupSamWritable two = (DupSamWritable) a;
        int refIndex1 = one.sequence.get();
        int refIndex2 = two.sequence.get();
        if (refIndex1 == -1) {
            return (refIndex2 == -1 ? 0 : 1);
        } else if (refIndex2 == -1) {
            return -1;
        }
        int cmp = refIndex1 - refIndex2;
        if (cmp != 0) {
            return cmp;
        }
        // XXX temp
        return -1;
        // return two.alignmentStart.get() - one.alignmentStart.get();
    }
    
    
    
}
