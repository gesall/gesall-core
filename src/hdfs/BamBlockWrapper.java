package hdfs;

/**
 * Wrapper over the bytes of a BAM block.
 */
public class BamBlockWrapper {

    long   startPosition;
    byte[] compressedBlock;

    public BamBlockWrapper(long startPosition, byte[] compressedBlock) {
        this.startPosition = startPosition;
        this.compressedBlock = compressedBlock;
    }

    public BamBlockWrapper(byte[] compressedBlock) {
        this.compressedBlock = compressedBlock;
    }

    public long getStartPosition() {
        return startPosition;
    }

    public byte[] getBamBlock() {
        return this.compressedBlock;
    }

}
