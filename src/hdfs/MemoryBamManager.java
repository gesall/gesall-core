package hdfs;

import htsjdk.samtools.BAMHeaderEncoder;
import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.BlockCompressedOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import bam.utils.BamFileUtils;

@SuppressWarnings("deprecation")
public class MemoryBamManager {

    private final byte[] headerBytes;
    private final int    approxCapacity;

    private List<byte[]> records;
    private int          recordLength;

    public MemoryBamManager(byte[] headerBytes) {
        this.headerBytes = headerBytes;
        this.approxCapacity = 100;
        clear();
    }

    public MemoryBamManager(Configuration conf, String headerFileName,
            int approxCapacity) throws IOException {
        SAMFileHeader header = getHeader(conf, headerFileName);
        BAMHeaderEncoder headerEncoder = new BAMHeaderEncoder(header);
        this.headerBytes = headerEncoder.encode();
        this.approxCapacity = approxCapacity;
        clear();
    }
    
    public MemoryBamManager(Configuration conf, String headerFileName,
            int approxCapacity, SAMReadGroupRecord readGroup)
            throws IOException {
        SAMFileHeader header = getHeader(conf, headerFileName);
        header.addReadGroup(readGroup);
        BAMHeaderEncoder headerEncoder = new BAMHeaderEncoder(header);
        this.headerBytes = headerEncoder.encode();
        this.approxCapacity = approxCapacity;
        clear();
    }

    public void addRecord(byte[] record) {
        records.add(record);
        recordLength += record.length;
    }

    public SamReader getSamReader() throws IOException {
        byte[] bam = getBamFile();
        InputStream is = new ByteArrayInputStream(bam);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(is));
        return reader;
    }

    public SAMFileReader getSamFileReader() throws IOException {
        byte[] bam = getBamFile();
        InputStream is = new ByteArrayInputStream(bam);
        SAMFileReader reader = new SAMFileReader(is);
        return reader;
    }

    private byte[] getBamFile() throws IOException {
        byte[] binaryBam = getBinaryBamFile();
        ByteArrayOutputStream baos = new ByteArrayOutputStream(binaryBam.length);
        BlockCompressedOutputStream bcos = new BlockCompressedOutputStream(
                baos, null, 0);
        bcos.write(binaryBam);
        bcos.close();
        byte[] bam = baos.toByteArray();
        return bam;
    }

    public void clear() {
        this.records = new ArrayList<byte[]>(approxCapacity);
        this.recordLength = 0;
    }

    private byte[] getBinaryBamFile() {
        int concatLength = headerBytes.length + recordLength;
        byte[] binaryBam = new byte[concatLength];

        int offset = 0;
        System.arraycopy(headerBytes, 0, binaryBam, offset, headerBytes.length);
        offset += headerBytes.length;
        for (int i = 0; i < records.size(); ++i) {
            byte[] current = records.get(i);
            System.arraycopy(current, 0, binaryBam, offset, current.length);
            offset += current.length;
        }
        return binaryBam;
    }

    public static SAMFileHeader getHeader(Configuration conf, String headerFileName)
            throws IOException {
        FileSystem fs = FileSystem.get(conf);
        Path headerFilePath = new Path(headerFileName);
        FSDataInputStream in = fs.open(headerFilePath);
        DataInputStream dis = new DataInputStream(in);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(dis));
        SAMFileHeader header = reader.getFileHeader();
        return header;
    }

    @SuppressWarnings("unused")
    private SAMFileHeader getHeader2(Configuration conf, String headerFileName)
            throws IOException {
        byte[] cHeader = getHeaderBlock(conf, headerFileName);
        InputStream fileInputStream = new ByteArrayInputStream(cHeader);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(fileInputStream));
        SAMFileHeader header = reader.getFileHeader();
        return header;
    }

    private byte[] getHeaderBlock(Configuration conf, String headerFileName)
            throws IOException {
        FileSystem fs = FileSystem.get(conf);
        Path headerFilePath = new Path(headerFileName);
        FSDataInputStream in = fs.open(headerFilePath);
        DataInputStream dis = new DataInputStream(in);
        byte[] compressedHeader = BamFileUtils.readCompressedBlock(dis);
        dis.close();
        return compressedHeader;
    }

}
