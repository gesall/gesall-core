package hdfs.partition.range;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.BamRecordReader;
import hdfs.bloom.GenomicPositionKey;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

@SuppressWarnings("deprecation")
public class SamHdfsRecordReader extends RecordReader<GenomicPositionKey, Text> {

    private SAMRecordIterator    samRecordIterator;
    private BamRecordReader      bamChunksReader;
    private BamRecordEnumeration bamChunkToSamConverter;

    private GenomicPositionKey   key;
    private Text                 value;

    @Override
    public void close() throws IOException {
        this.bamChunksReader.close();
    }

    @Override
    public GenomicPositionKey getCurrentKey() throws IOException,
            InterruptedException {
        return this.key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return this.value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return this.bamChunksReader.getProgress();
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context)
            throws IOException, InterruptedException {
        this.bamChunksReader = new BamRecordReader();
        this.bamChunksReader.initialize(split, context);
        Configuration conf = context.getConfiguration();
        String headerFileName = conf.get("BamInput");
        this.bamChunkToSamConverter = new BamRecordEnumeration(conf,
                headerFileName);
        
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        if (samRecordIterator == null || !samRecordIterator.hasNext()) {
            if (this.bamChunksReader.nextKeyValue()) {
                BytesWritable bamChunkBytes = this.bamChunksReader
                        .getCurrentValue();
                BamBlockWrapper bbw = new BamBlockWrapper(
                        bamChunkBytes.getBytes());
                List<BamBlockWrapper> bamChunks = new ArrayList<BamBlockWrapper>();
                bamChunks.add(bbw);
                SAMFileReader samFileReader = this.bamChunkToSamConverter
                        .getSAMFileReader(bamChunks);
                this.samRecordIterator = samFileReader.iterator();
            }
        }

        if (samRecordIterator != null && samRecordIterator.hasNext()) {
            SAMRecord samRecord = samRecordIterator.next();
            this.key = new GenomicPositionKey(samRecord.getReferenceName(),
                    samRecord.getAlignmentStart());
            this.value = new Text(samRecord.getSAMString());
            return true;
        }

        return false;
    }

}
