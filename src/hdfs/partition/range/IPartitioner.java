package hdfs.partition.range;

import htsjdk.samtools.SAMRecord;

public interface IPartitioner<K, V> {

    public KeyValuePair<K, V> getKVPair(SAMRecord record);

}
