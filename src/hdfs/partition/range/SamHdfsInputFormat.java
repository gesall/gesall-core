package hdfs.partition.range;

import hdfs.bloom.GenomicPositionKey;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class SamHdfsInputFormat extends
        FileInputFormat<GenomicPositionKey, Text> {

    @Override
    public RecordReader<GenomicPositionKey, Text> createRecordReader(
            InputSplit split, TaskAttemptContext context) throws IOException,
            InterruptedException {
        return new SamHdfsRecordReader();
    }

}
