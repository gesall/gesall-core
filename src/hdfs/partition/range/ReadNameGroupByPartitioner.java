package hdfs.partition.range;

import org.apache.hadoop.io.Text;

import htsjdk.samtools.SAMRecord;

public class ReadNameGroupByPartitioner implements IPartitioner<Text, Text> {

    @Override
    public KeyValuePair<Text, Text> getKVPair(SAMRecord record) {
        return new KeyValuePair<Text, Text>(new Text(record.getReadName()), new Text(
                record.getSAMString()));
    }

}
