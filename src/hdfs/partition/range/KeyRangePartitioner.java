package hdfs.partition.range;

import org.apache.hadoop.io.Text;

import hdfs.bloom.GenomicPositionKey;
import htsjdk.samtools.SAMRecord;

public class KeyRangePartitioner implements
        IPartitioner<GenomicPositionKey, Text> {

    /*
     * 
     * 1 (_)----_ 2 *----(_) 3 (_)----* 4 (*)----* ():key, _:mapped, *:unmapped,
     * -:pair
     */
    @Override
    public KeyValuePair<GenomicPositionKey, Text> getKVPair(SAMRecord record) {
        // TODO: Temporary condition
        if (record.getReadUnmappedFlag() && record.getMateUnmappedFlag()) {
            String reference = record.getReadName();
            int position = record.getReadName().hashCode();
            GenomicPositionKey key = new GenomicPositionKey(reference, position);
            Text value = new Text(record.getSAMString());
            return new KeyValuePair<GenomicPositionKey, Text>(key, value);
        }
        
        GenomicPositionKey key = null;
        Text value = new Text(record.getSAMString());
        if (record.getFirstOfPairFlag()) {
            // First of pair
            if (!record.getReadUnmappedFlag()) {
                key = getRecordKey(record);
            } else if (!record.getMateUnmappedFlag()) {
                key = getMateKey(record);
            } else {
                key = getRecordKey(record);
            }
        } else {
            // Second of pair
            if (!record.getMateUnmappedFlag()) {
                key = getMateKey(record);
            } else if (!record.getReadUnmappedFlag()) {
                key = getRecordKey(record);
            } else {
                key = getMateKey(record);
            }

        }

        /*
         * String reference; int position; if (record.getFirstOfPairFlag() &&
         * record.getReadUnmappedFlag()) { reference =
         * record.getReferenceName(); position = record.getAlignmentStart(); }
         * else { reference = record.getMateReferenceName(); position =
         * record.getMateAlignmentStart(); } GenomicPositionKey key = new
         * GenomicPositionKey(reference, position);
         */
        return new KeyValuePair<GenomicPositionKey, Text>(key, value);
    }

    private GenomicPositionKey getRecordKey(SAMRecord record) {
        String reference = record.getReferenceName();
        int position = record.getAlignmentStart();
        return new GenomicPositionKey(reference, position);
    }

    private GenomicPositionKey getMateKey(SAMRecord record) {
        String reference = record.getMateReferenceName();
        int position = record.getMateAlignmentStart();
        return new GenomicPositionKey(reference, position);
    }

}
