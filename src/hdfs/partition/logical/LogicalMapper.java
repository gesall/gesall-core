package hdfs.partition.logical;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import htsjdk.samtools.SAMRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

public class LogicalMapper extends
        Mapper<LongWritable, BytesWritable, NullWritable, NullWritable> {

    static enum MapperRecords {
        BAM_RECORDS
    }

    BamRecordEnumeration  samEnum;
    String                headerFileName;
    List<BamBlockWrapper> bamBlocks;
    long addedBlocks;
    
    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        headerFileName = conf.get("BamInput");
        samEnum = new BamRecordEnumeration(conf, headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
        addedBlocks = 0;
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
        ++addedBlocks;
        if (addedBlocks == 1000) {
            doWork(context);
        }
    }

    private void doWork(Context context) {
        if (bamBlocks.isEmpty())
            return;
        
        samEnum.initializeEnumeration(bamBlocks);
        while (samEnum.hasMoreElements()) {
            SAMRecord record = samEnum.nextElement();
            record.getAlignmentStart();
            context.getCounter(MapperRecords.BAM_RECORDS).increment(1);
        }
        samEnum.close();
        bamBlocks.clear();
        addedBlocks = 0;

    }
    
    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        doWork(context);
    }
}
