package hdfs.partition.logical;

import org.apache.hadoop.hdfs.DFSUtil;
import org.apache.hadoop.hdfs.StorageType;
import org.apache.hadoop.hdfs.server.blockmanagement.BlockPlacementPolicyDefault;
import org.apache.hadoop.hdfs.server.blockmanagement.DatanodeDescriptor;
import org.apache.hadoop.hdfs.server.blockmanagement.DatanodeStorageInfo;
import org.apache.hadoop.net.Node;
import org.apache.hadoop.net.NodeBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class LogicalPlacementPolicy extends BlockPlacementPolicyDefault {


    public DatanodeStorageInfo[] chooseTarget(String srcPath,
                                              int numOfReplicas,
                                              Node writer,
                                              List<DatanodeStorageInfo> chosen,
                                              boolean returnChosenNodes,
                                              Set<Node> excludedNodes,
                                              long blocksize,
                                              StorageType storageType) {

        List<DatanodeStorageInfo> results = new ArrayList<DatanodeStorageInfo>();
        int pathHashCode = Math.abs(srcPath.hashCode());
        int dataNodesCount = clusterMap.getNumOfLeaves();
        int dataNodeIndex = pathHashCode % dataNodesCount;
        System.out.println("LogicalPolicy");
        System.out.println("srcPath: "+srcPath);
        System.out.println("hash: "+pathHashCode+" , nodeCount: "+dataNodesCount + " , nodeIndex: "+dataNodeIndex);

        // just select random here TODO
        
        DatanodeDescriptor chosenNode =
                (DatanodeDescriptor) clusterMap.getLeaves(NodeBase.ROOT).get(dataNodeIndex);

        DatanodeStorageInfo firstChosen = null;
        /*
        final DatanodeStorageInfo[] storages = DFSUtil.shuffle(
                chosenNode.getStorageInfos());
        int i;
        for (i = 0; i < storages.length; i++) {
            if (firstChosen == null) {
                firstChosen = storages[i];
                break;
            }
        }
        results.add(firstChosen);
        return results.toArray(new DatanodeStorageInfo[results.size()]);
    */
        return null;
    }

    public DatanodeStorageInfo[] chooseTarget(String src,
                                              int numOfReplicas, Node writer,
                                              Set<Node> excludedNodes,
                                              long blocksize,
                                              List<DatanodeDescriptor> favoredNodes,
                                              StorageType storageType) {
        // This class does not provide the functionality of placing
        // a block in favored datanodes. The implementations of this class
        // are expected to provide this functionality

        return chooseTarget(src, numOfReplicas, writer,
                new ArrayList<DatanodeStorageInfo>(numOfReplicas), false,
                excludedNodes, blocksize, storageType);
    }


}
