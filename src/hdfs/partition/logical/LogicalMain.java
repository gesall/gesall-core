package hdfs.partition.logical;

import hdfs.LogicalBlockBamInputFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class LogicalMain {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("BamInput", args[0]);

        Job job = Job.getInstance(conf);
        job.setJobName("Logical test");
        job.setJarByClass(LogicalMain.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setInputFormatClass(LogicalBlockBamInputFormat.class);
        job.setMapperClass(LogicalMapper.class);
        job.setNumReduceTasks(0);

        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }


}
