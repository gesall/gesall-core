package hdfs.partition.logical;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.TextInputFormat;

public class MapredLogicalTextInputFormat extends TextInputFormat {

    @Override
    public boolean isSplitable(FileSystem fs, Path file) {
        return false;
    }

}
