package hdfs;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import bam.utils.BamFileUtils;

@SuppressWarnings("deprecation")
public class SamInputStream {

    private final String  originalHeaderString;
    private StringBuilder samRecords;

    private byte[]        samInBytes;

    public SamInputStream(Configuration conf, String headerFileName) throws IOException {
        this(conf, headerFileName, "");
    }

    public SamInputStream(Configuration conf, String headerFileName, String headerAppendString)
            throws IOException {
        byte[] cHeader = getHeader(conf, headerFileName);
        this.originalHeaderString = getHeaderString(cHeader, headerAppendString);
        this.samRecords = new StringBuilder();
    }

    public void add(String records) {
        this.samRecords.append(records);
    }

    public void add(StringBuilder records) {
        this.samRecords.append(records.toString());
    }

    public void clear() {
        this.samRecords = new StringBuilder();
        this.samInBytes = new byte[0];

    }

    public byte[] getSamInBytes() throws IOException {
        if (this.samInBytes == null || this.samInBytes.length == 0) {
            StringBuilder samFileBuilder = new StringBuilder(
                    originalHeaderString);
            samFileBuilder.append(samRecords.toString());
            String samFile = samFileBuilder.toString();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(samFile.getBytes(Charset.forName("UTF-8")));
            outputStream.flush();
            byte[] bytes = outputStream.toByteArray();
            this.samInBytes = bytes;
        }
        return this.samInBytes;
    }

    public SamReader getSamReader() throws IOException {
        byte[] bytes = getSamInBytes();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(inputStream));
        return reader;
    }

    public SAMFileReader getSamFileReader() throws IOException {
        byte[] bytes = getSamInBytes();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        SAMFileReader reader = new SAMFileReader(inputStream);
        return reader;
    }

    private String getHeaderString(byte[] cHeader, String headerAppendString) {
        InputStream fileInputStream = new ByteArrayInputStream(cHeader);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(fileInputStream));
        SAMFileHeader header = reader.getFileHeader();
        String headerString = header.getTextHeader();
        headerString = headerString + headerAppendString;
        return headerString;

        /*
         * // create the read group we'll be using String RGID = "gesall";
         * String RGLB = "gesall"; String RGPU = "dummy"; String RGPL =
         * "ILLUMINA"; String RGSM = "full"; String RGCN = "GDL"; final
         * SAMReadGroupRecord rg = new SAMReadGroupRecord(RGID);
         * rg.setLibrary(RGLB); rg.setPlatform(RGPL); rg.setSample(RGSM);
         * rg.setPlatformUnit(RGPU); if (RGCN != null)
         * rg.setSequencingCenter(RGCN);
         * header.setReadGroups(Arrays.asList(rg));
         */
        // System.out.println("HEADERSTR");
        // System.out.println(headerStr);
        // String appendStr =
        // "@RG\tID:gesall\tPL:ILLUMINA\tPU:dummy\tLB:gesall\tSM:full\tCN:GDL\n";
        // XXX remove for MarkDup
    }

    private byte[] getHeader(Configuration conf, String headerFile) throws IOException {
        FileSystem fs = FileSystem.get(conf);

        // FileSystem fs = FileSystem.newInstance(conf); // TODO

        Path headerFilePath = new Path(headerFile);
        FSDataInputStream in = fs.open(headerFilePath);
        DataInputStream dis = new DataInputStream(in);
        byte[] compressedHeader = BamFileUtils.readCompressedBlock(dis);
        dis.close();
        return compressedHeader;
    }

}
