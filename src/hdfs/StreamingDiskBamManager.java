package hdfs;

import java.io.IOException;

import htsjdk.samtools.BAMHeaderEncoder;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.util.BlockCompressedOutputStream;

public class StreamingDiskBamManager {

    BlockCompressedOutputStream output;

    public StreamingDiskBamManager(SAMFileHeader header, String outputFile)
            throws IOException {
        output = openBamStream(outputFile);
        writeHeader(header);
    }

    private BlockCompressedOutputStream openBamStream(String fileName) {
        int compressionLevel = 5;
        BlockCompressedOutputStream bcos = new BlockCompressedOutputStream(
                fileName, compressionLevel);
        return bcos;
    }

    private void writeHeader(SAMFileHeader header) throws IOException {
        BAMHeaderEncoder headerEncoder = new BAMHeaderEncoder(header);
        byte[] headerBytes = headerEncoder.encode();
        output.write(headerBytes);
    }

    public void addRecord(byte[] record) throws IOException {
        output.write(record);
    }

    public void close() throws IOException {
        output.flush();
        output.flush();
        output.close();
    }

}
