package hdfs;

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import bam.utils.BamFileUtils;

/**
 * Enumerates over SAM records present in a BAM blocks. The BAM block,
 * containing the header only, is also supplied.
 */
@SuppressWarnings("deprecation")
public class BamRecordEnumeration implements Enumeration<SAMRecord> {

    private byte[]            header;
    private SAMRecordIterator iterator;

    public BamRecordEnumeration(Configuration conf, String headerFileName) throws IOException {
        this.header = readHeader(conf, headerFileName);
    }

    // TODO: change BamBlockWrapper to byte[]
    public void initializeEnumeration(List<BamBlockWrapper> bamBlocks) {
        byte[] compressedFile = getMergedBytes(bamBlocks);
                // XXX getMergedBytes(bamBlocks);
        
        InputStream fileInputStream = new ByteArrayInputStream(compressedFile);
        // SeekableStream fileInputStream = new
        // ByteArraySeekableStream(compressedFile);
        SamReader reader = SamReaderFactory.make()
                .validationStringency(ValidationStringency.LENIENT)
                .samRecordFactory(DefaultSAMRecordFactory.getInstance())
                .open(SamInputResource.of(fileInputStream));
        this.iterator = reader.iterator();
    }

    public SAMFileReader getSAMFileReader(List<BamBlockWrapper> bamBlocks) {
        byte[] compressedFile = getMergedBytes(bamBlocks);
        InputStream fileInputStream = new ByteArrayInputStream(compressedFile);
        SAMFileReader reader = new SAMFileReader(fileInputStream);
        return reader;
    }
    
    
    private byte[] readHeader(Configuration conf, String headerFile) throws IOException {
        FileSystem fs = FileSystem.get(conf);

        Path headerFilePath = new Path(headerFile);
        FSDataInputStream in = fs.open(headerFilePath);
        DataInputStream dis = new DataInputStream(in);
        byte[] compressedHeader = BamFileUtils.readCompressedBlock(dis);
        dis.close();
        return compressedHeader;
    }

    private byte[] getMergedBytes(List<BamBlockWrapper> bamBlocks) {
        int totalBytes = 0;
        totalBytes += header.length;
        //System.out.println("Header length: " + header.length);
        for (BamBlockWrapper bamBlock : bamBlocks) {
            totalBytes += bamBlock.getBamBlock().length;
            //System.out.println("Block length: " + bamBlock.getBamBlock().length);
        }
        //System.out.println("Total length: " + totalBytes);
        byte[] compressedFile = new byte[totalBytes];
        int fileOffset = 0;
        System.arraycopy(header, 0, compressedFile, fileOffset, header.length);
        fileOffset += header.length;

        for (BamBlockWrapper bamBlock : bamBlocks) {
            byte[] bamBytes = bamBlock.getBamBlock();
            System.arraycopy(bamBytes, 0, compressedFile, fileOffset,
                    bamBytes.length);
            fileOffset += bamBytes.length;
        }
        return compressedFile;
    }

//    private byte[] getUncompressedBytes(List<BamBlockWrapper> bamBlocks) {
//
//        int totalBytes = 0;
//        byte[] uHeader = BamFileUtils.getUncompressedBlock(header);
//
//        totalBytes += uHeader.length;
//        System.out.println("UHeader length: " + uHeader.length);
//        for (BamBlockWrapper bamBlock : bamBlocks) {
//            byte[] uBlock = BamFileUtils.getUncompressedBlock(bamBlock
//                    .getBamBlock());
//            totalBytes += uBlock.length;
//            System.out.println("UBlock length: " + uBlock.length);
//        }
//        System.out.println("Total Ulength: " + totalBytes);
//        byte[] uFile = new byte[totalBytes];
//        int fileOffset = 0;
//        System.arraycopy(uHeader, 0, uFile, fileOffset, uHeader.length);
//        fileOffset += uHeader.length;
//
//        for (BamBlockWrapper bamBlock : bamBlocks) {
//            byte[] uBlock = BamFileUtils.getUncompressedBlock(bamBlock
//                    .getBamBlock());
//
//            System.arraycopy(uBlock, 0, uFile, fileOffset, uBlock.length);
//            fileOffset += uBlock.length;
//        }
//        return uFile;
//    }

    public void close() {
        iterator.close();
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public SAMRecord nextElement() {
        return iterator.next();
    }

}
