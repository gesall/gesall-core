package hdfs.bloom;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import com.google.common.hash.Funnel;
import com.google.common.hash.PrimitiveSink;

public class GenomicPositionKey implements
        WritableComparable<GenomicPositionKey>, Funnel<GenomicPositionKey> {

    private static final long serialVersionUID = 1L;

    Text        reference;
    IntWritable position;

    public GenomicPositionKey() {
        this.reference = new Text();
        this.position = new IntWritable();
    }

    public GenomicPositionKey(String reference, int position) {
        this.reference = new Text(reference);
        this.position = new IntWritable(position);
    }

    public String getReference() {
        return reference.toString();
    }

    public int getPosition() {
        return position.get();
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        reference.readFields(in);
        position.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        reference.write(out);
        position.write(out);
    }

    @Override
    public int hashCode() {
        return reference.hashCode() * 163 + position.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof GenomicPositionKey) {
            GenomicPositionKey gpk = (GenomicPositionKey) o;
            return reference.equals(gpk.reference)
                    && position.equals(gpk.position);
        }
        return false;
    }

    @Override
    public int compareTo(GenomicPositionKey gpk) {
        int cmp = reference.compareTo(gpk.reference);
        if (cmp != 0) {
            return cmp;
        }
        return position.compareTo(gpk.position);
    }

    @Override
    public String toString() {
        return reference.toString() + "\t" + position.get();
    }

    @Override
    public void funnel(GenomicPositionKey from, PrimitiveSink into) {
        into.putUnencodedChars(from.getReference()).putInt(from.getPosition());
    }

}
