package hdfs.bloom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.IOUtils;

import com.google.common.hash.BloomFilter;

public class BloomFilesLoader {

    Configuration conf;
    String        inputPath;

    public BloomFilesLoader(Configuration conf, String inputPath) {
        this.conf = conf;
        this.inputPath = inputPath;
    }

    public BloomFilter<GenomicPositionKey> getCombinedBloomFilter()
            throws IOException {
        BloomFilter<GenomicPositionKey> finalBloomFilter = null;
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatuses = fs.listStatus(new Path(inputPath),
                new BloomPathFilter(conf));
        for (FileStatus fileStatus : fileStatuses) {
            FSDataInputStream in = fs.open(fileStatus.getPath());
            BloomFilter<GenomicPositionKey> current = BloomFilter.readFrom(in,
                    new GenomicPositionKey());
            in.close();
            if (finalBloomFilter != null) {
                finalBloomFilter.putAll(current);
            } else {
                finalBloomFilter = current;
            }
        }
        return finalBloomFilter;
    }

    public List<BloomFilter<GenomicPositionKey>> getBloomFilterList()
            throws IOException {
        List<BloomFilter<GenomicPositionKey>> bloomFilters = new ArrayList<BloomFilter<GenomicPositionKey>>();
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatuses = fs.listStatus(new Path(inputPath),
                new BloomPathFilter(conf));
        for (FileStatus fileStatus : fileStatuses) {
            FSDataInputStream in = fs.open(fileStatus.getPath());
            BloomFilter<GenomicPositionKey> current = BloomFilter.readFrom(in,
                    new GenomicPositionKey());
            in.close();
            bloomFilters.add(current);
        }
        return bloomFilters;
    }

    public void writeCombinedBloomFilter(String outputFileName)
            throws IOException {
        FileSystem fs = FileSystem.get(conf);
        Path bloomFilePath = new Path(outputFileName);
        FSDataOutputStream bloomOutStream = fs.create(bloomFilePath);
        BloomFilter<GenomicPositionKey> combinedBloomFilter = getCombinedBloomFilter();
        combinedBloomFilter.writeTo(bloomOutStream);
        IOUtils.closeStream(bloomOutStream);
    }

}

class BloomPathFilter implements PathFilter {
    FileSystem fileSystem;

    public BloomPathFilter(Configuration conf) {
        try {
            fileSystem = FileSystem.get(conf);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public boolean accept(Path path) {
        try {
            if (fileSystem.isDirectory(path)) {
                return true;
            }
            return path.getName().endsWith(".bloom");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}