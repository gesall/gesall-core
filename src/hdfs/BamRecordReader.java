package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

// input will be a list of bam blocks (can be 1, or more than 1)
// fetch header, use sam library to call program
public class BamRecordReader extends RecordReader<LongWritable, BytesWritable> {

    private BamBlockEnumeration bamBlocks;
    private BamBlockWrapper     currentBlock;

    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)
            throws IOException, InterruptedException {
        FileSplit split = (FileSplit) genericSplit;
        final Path file = ((FileSplit) genericSplit).getPath();
        Configuration job = context.getConfiguration();

        final FileSystem fs = file.getFileSystem(job);
        FSDataInputStream fileIn = fs.open(file);

        long start = split.getStart();
        long length = split.getLength();

        fileIn.seek(start);
        bamBlocks = new BamBlockEnumeration(fileIn, start, length);
    }

    @Override
    public void close() throws IOException {
        if (bamBlocks != null) {
            bamBlocks.close();
        }
    }

    @Override
    public LongWritable getCurrentKey() throws IOException,
            InterruptedException {
        LongWritable currentKey = null;
        if (currentBlock != null) {
            currentKey = new LongWritable(currentBlock.getStartPosition());
        }
        return currentKey;
    }

    @Override
    public BytesWritable getCurrentValue() throws IOException,
            InterruptedException {
        BytesWritable currentValue = null;
        if (currentBlock != null) {
            currentValue = new BytesWritable(currentBlock.getBamBlock());
        }
        return currentValue;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (bamBlocks != null) {
            return bamBlocks.getProgress();
        }
        return 0;
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        if (!bamBlocks.hasMoreElements()) {
            return false;
        }
        currentBlock = bamBlocks.nextElement();
        return true;
    }

}
