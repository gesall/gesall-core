package hdfs;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class DupSamWritable implements WritableComparable<DupSamWritable> {

    IntWritable     sequence;
    IntWritable     coordinate;
    BooleanWritable bothUnmapped;

    public DupSamWritable() {
        this.sequence = new IntWritable();
        this.coordinate = new IntWritable();
        this.bothUnmapped = new BooleanWritable();
    }

    public DupSamWritable(int sequence, int coordinate, boolean bothUnmapped) {
        this.sequence = new IntWritable(sequence);
        this.coordinate = new IntWritable(coordinate);
        this.bothUnmapped = new BooleanWritable(bothUnmapped);
    }

    public int getSequence() {
        return sequence.get();
    }

    public int getCoordinate() {
        return coordinate.get();
    }

    public boolean areBothUnmapped() {
        return bothUnmapped.get();
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        sequence.readFields(in);
        coordinate.readFields(in);
        bothUnmapped.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        sequence.write(out);
        coordinate.write(out);
        bothUnmapped.write(out);
    }
    
    @Override
    public int compareTo(DupSamWritable dsw) {
        int cmp = coordinate.compareTo(dsw.coordinate);
        if (cmp == 0) {
            cmp = sequence.compareTo(dsw.sequence);
            if (cmp == 0) {
                cmp = bothUnmapped.compareTo(dsw.bothUnmapped);
            }
        }
        return cmp;
        /*
         * int refIndex1 = this.sequence.get(); int refIndex2 =
         * dsw.sequence.get(); if (refIndex1 == -1) { return (refIndex2 == -1 ?
         * 0 : 1); } else if (refIndex2 == -1) { return -1; } int cmp =
         * refIndex1 - refIndex2; if (cmp != 0) { return cmp; } return
         * this.alignmentStart.get() - dsw.alignmentStart.get();
         */
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DupSamWritable) {
            DupSamWritable dsw = (DupSamWritable) o;
            return coordinate.equals(dsw.coordinate)
                    && sequence.equals(dsw.sequence)
                    && bothUnmapped.equals(dsw.bothUnmapped);
        }
        return false;
    }

    @Override
    public String toString() {
        return sequence + ":" + coordinate + ":" + bothUnmapped;
    }

    @Override
    public int hashCode() {
        return (sequence.hashCode() * 257) + (coordinate.hashCode() * 163)
                + (bothUnmapped.hashCode());
    }

}
