package hdfs;

import java.io.IOException;
import java.util.Enumeration;

import org.apache.hadoop.fs.FSDataInputStream;

import bam.utils.BamConstants;
import bam.utils.BamFileUtils;

/**
 * Enumerates over BAM blocks from a HDFS input chunk. All BAM blocks starting
 * in [start,end] are returned.
 */
public class BamBlockEnumeration implements Enumeration<BamBlockWrapper> {

    // this stream is seekable
    private FSDataInputStream fileIn;
    private long              start;
    private long              end;

    public BamBlockEnumeration(FSDataInputStream fileIn, long start, long length)
            throws IOException {
        this.fileIn = fileIn;
        this.start = start;
        this.end = start + length;
        initialize();
    }

    // move to the start of first Bam block
    private void initialize() throws IOException {

        long bytesSkipped = 0;

        if (start == 0) {
            // so it passes the header block
            fileIn.seek(1);
            bytesSkipped = 1;
        } else {
            fileIn.seek(start);
        }

        try {
            bytesSkipped += seekToFirstBamBlock();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // if (bytesSkipped < 0) {
        // no records found, reached eof
        // }
        if (bytesSkipped > (end - start)) {
            // should not arise
            // BAM default block size 64KB, HDFS default block size 64MB
            // TODO: LOG message
            String errMsg = "No record or EOF found in split, start=" + start
                    + ", end=" + end + ", skip=" + bytesSkipped;
            // throw new IOException("No record or EOF found in split.");
            throw new IOException(errMsg);
        }

    }

    private long seekToFirstBamBlock() throws IOException {
        byte[] window = new byte[BamConstants.GZIP_IDENTIFIER_LENGTH];
        long bytesSkipped = 0;

        // search for first gzip header in split
        fileIn.readFully(window, 0, BamConstants.GZIP_IDENTIFIER_LENGTH);
        bytesSkipped += BamConstants.GZIP_IDENTIFIER_LENGTH;

        while (!checkHeaderIdentifier(window)) {
            // read one more byte and check for header again
            leftShiftWindow(window);
            bytesSkipped += 1;
        }

        // seek back to start of BAM block
        fileIn.seek(fileIn.getPos() - BamConstants.GZIP_IDENTIFIER_LENGTH);
        bytesSkipped -= BamConstants.GZIP_IDENTIFIER_LENGTH;

        return bytesSkipped;
    }

    /*
     * Shifts array one position to left. Fills in vacant position with new
     * byte.
     */
    private void leftShiftWindow(byte[] window) throws IOException {
        System.arraycopy(window, 1, window, 0,
                BamConstants.GZIP_IDENTIFIER_LENGTH - 1);
        fileIn.readFully(window, BamConstants.GZIP_IDENTIFIER_LENGTH - 1, 1);
    }

    private boolean checkHeaderIdentifier(byte[] window) {
        for (int i = 0; i < BamConstants.GZIP_IDENTIFIER_LENGTH; ++i) {
            if (window[i] != BamConstants.gzipIdentifier[i])
                return false;
        }
        return true;
    }

    /*
     * Assumes that read position is aligned to record start
     */
    private BamBlockWrapper nextBamBlock() throws IOException {
        long blockStartPos = fileIn.getPos();
        byte[] compressedBlock = BamFileUtils.readCompressedBlock(fileIn);
        BamBlockWrapper bamBlock = new BamBlockWrapper(blockStartPos,
                compressedBlock);
        return bamBlock;
    }

    public void close() throws IOException {
        if (fileIn != null)
            fileIn.close();
    }

    public float getProgress() throws IOException {
        if (start == end) {
            return 0.0f;
        } else {
            return Math.min(1.0f, (fileIn.getPos() - start)
                    / (float) (end - start));
        }

    }

    @Override
    public boolean hasMoreElements() {
        long currentPos = -1;
        try {
            currentPos = fileIn.getPos();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return (currentPos < this.end);
    }

    @Override
    public BamBlockWrapper nextElement() {
        BamBlockWrapper bamBlock = null;
        try {
            bamBlock = nextBamBlock();
        } catch (IOException e) {
            // throw new RuntimeException(e);
            String errMsg;
            try {
                errMsg = "next call err, pos: " + fileIn.getPos() + ", end: "
                        + end;
                throw new RuntimeException(errMsg);

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return bamBlock;
    }

}
