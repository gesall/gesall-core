package experiment.collect.sam;

import hdfs.config.PropertyReader;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AllBamReducer extends
        Reducer<IntWritable, Text, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SHUFFLED_SAM_RECORDS, OUTPUT_BAM_RECORDS
    }

    String markDupLocalLogDir;
    PrintWriter writer;
    
    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");
        String name = markDupLocalLogDir+"/"+getNextFileName(context, "allbam");
        writer = new PrintWriter(name, "UTF-8");
        
    }

    @Override
    public void reduce(IntWritable key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.SHUFFLED_SAM_RECORDS).increment(1);
        for (Text value : values) {
            writer.println(value.toString()+"\n");
        }
        
    }
    
    public void cleanup(Context context) throws IOException,
    InterruptedException {
        writer.close();
    }
    
    private String getNextFileName(Context context, String suffix) {
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        return fileName;
    }

  




}