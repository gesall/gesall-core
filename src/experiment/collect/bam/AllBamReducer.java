package experiment.collect.bam;

import hdfs.config.PropertyReader;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class AllBamReducer extends
        Reducer<IntWritable, BytesWritable, NullWritable, NullWritable> {

    static enum ReducerRecords {
        SHUFFLED_SAM_RECORDS, OUTPUT_BAM_RECORDS
    }

    String           markDupLocalLogDir;
    FileOutputStream writer;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");
        String name = markDupLocalLogDir + "/"
                + getNextFileName(context, "allbam");
        writer = new FileOutputStream(name);

    }

    @Override
    public void reduce(IntWritable key, Iterable<BytesWritable> values,
            Context context) throws IOException, InterruptedException {
        context.getCounter(ReducerRecords.SHUFFLED_SAM_RECORDS).increment(1);
        for (BytesWritable value : values) {
            byte[] record = value.copyBytes();

            writer.write(record);
        }

    }

    public void cleanup(Context context) throws IOException,
            InterruptedException {
        writer.close();
    }

    private String getNextFileName(Context context, String suffix) {
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        return fileName;
    }

}