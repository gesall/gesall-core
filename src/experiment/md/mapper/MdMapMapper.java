package experiment.md.mapper;

import hdfs.BamBlockWrapper;
import hdfs.BamRecordEnumeration;
import hdfs.config.CmdLineArgs;
import hdfs.config.PropertyReader;
import hdfs.utils.LogPrinter;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

import com.google.common.base.Stopwatch;

@SuppressWarnings("deprecation")
public class MdMapMapper extends
        Mapper<LongWritable, BytesWritable, NullWritable, NullWritable> {

    static enum MapperRecords {
        INPUT_BAM_RECORDS, SHUFFLED_SAM_RECORDS
    }

    private static final Log      log = LogFactory.getLog(MdMapMapper.class);

    private BamRecordEnumeration  samEnum;
    private String                headerFileName;
    private List<BamBlockWrapper> bamBlocks;

    String                        markDupLocalLogDir;

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");

        headerFileName = conf.get("BamInput");
        samEnum = new BamRecordEnumeration(context.getConfiguration(),
                headerFileName);
        bamBlocks = new ArrayList<BamBlockWrapper>();
    }

    @Override
    public void map(LongWritable key, BytesWritable value, Context context) {
        BamBlockWrapper bamBlock = new BamBlockWrapper(key.get(),
                value.getBytes());
        bamBlocks.add(bamBlock);
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {

        Stopwatch timer = Stopwatch.createStarted();

        SAMFileReader sortSamInput = samEnum.getSAMFileReader(bamBlocks);

        byte[] sortSamOutput = runSortSam(sortSamInput);

        byte[] mdupOutput = runMarkDup(sortSamOutput, context);

        // shuffle records
        InputStream inputStream = new ByteArrayInputStream(mdupOutput);
        SAMFileReader samFileReader = new SAMFileReader(inputStream);

        String outputBamName = getNextFileName(context);
        Path path = new Path(outputBamName);
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        FSDataOutputStream reducerOutput = fs.create(path);
        SAMFileWriter reducerWriter = new SAMFileWriterFactory().makeBAMWriter(
                samFileReader.getFileHeader(), true, reducerOutput);

        for (SAMRecord record : samFileReader) {
            reducerWriter.addAlignment(record);

            context.getCounter(MapperRecords.SHUFFLED_SAM_RECORDS).increment(1);
        }
        samFileReader.close();
        reducerWriter.close();
        reducerOutput.close();

        timer.stop();
        LogPrinter.printSecondsElaped(log, "MdMapMapper",
                timer.elapsed(TimeUnit.SECONDS));
    }

    private String getNextFileName(Context context) {
        Configuration conf = context.getConfiguration();
        String outputDir = conf.get("OutputDir");
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + "mdmap" + ".bam";
        return outputDir + "/" + fileName;
    }

    private byte[] runSortSam(SAMFileReader input) throws IOException {
        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        ByteArrayOutputStream sortSamOutWriter = new ByteArrayOutputStream();
        Stopwatch subTimer = Stopwatch.createStarted();

        new SortSam().instanceMainWithoutExit(sortSamArgv, input,
                sortSamOutWriter);
        sortSamOutWriter.flush();
        subTimer.stop();
        LogPrinter.printSecondsElaped(log, "SortSam",
                subTimer.elapsed(TimeUnit.SECONDS));
        byte[] output = sortSamOutWriter.toByteArray();
        return output;
    }

    private byte[] runMarkDup(byte[] input, Context context) throws IOException {
        Stopwatch subTimer = Stopwatch.createStarted();
        String[] markDupArgv = CmdLineArgs.getMarkDupArgv();

        markDupArgv[4] = "METRICS_FILE=" + markDupLocalLogDir + "/markdup-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + ".log";
        ByteArrayOutputStream markDupOutWriter = new ByteArrayOutputStream();
        new MarkDuplicates().instanceMainWithoutExitWithMulReader(markDupArgv,
                input, markDupOutWriter);
        markDupOutWriter.flush();
        subTimer.stop();
        LogPrinter.printSecondsElaped(log, "MarkDup",
                subTimer.elapsed(TimeUnit.SECONDS));
        byte[] output = markDupOutWriter.toByteArray();
        return output;
    }

}
