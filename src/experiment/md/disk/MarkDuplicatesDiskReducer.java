package experiment.md.disk;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import hdfs.MemoryBamManager;
import hdfs.StreamingDiskBamManager;
import hdfs.config.CmdLineArgs;
import hdfs.config.PropertyReader;
import hdfs.utils.LogPrinter;
import hdfs.utils.MarkDuplicatesShuffleKey;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileReader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;
    
import com.google.common.base.Stopwatch;

import picard.sam.MarkDuplicates;
import picard.sam.SortSam;

@SuppressWarnings("deprecation")
public class MarkDuplicatesDiskReducer
        extends
        Reducer<MarkDuplicatesShuffleKey, BytesWritable, NullWritable, NullWritable> {

    static enum ReducerRecords {
        OUT_RECORDS
    }

    String                   headerFileName;
    String                   markDupLocalLogDir;
    String                   outputFile;

    StreamingDiskBamManager  writer;

    private static final Log log = LogFactory
                                         .getLog(MarkDuplicatesDiskReducer.class);

    @Override
    public void setup(Context context) throws IOException {
        Configuration conf = context.getConfiguration();
        PropertyReader props = new PropertyReader(conf);
        markDupLocalLogDir = props.getValue("pg.local.temp.dir");
        headerFileName = conf.get("BamInput");

        outputFile = getBAMFileName(context, markDupLocalLogDir, "input1");

        SAMFileHeader header = MemoryBamManager.getHeader(conf, headerFileName);
        writer = new StreamingDiskBamManager(header, outputFile);
    }

    @Override
    public void reduce(MarkDuplicatesShuffleKey key,
            Iterable<BytesWritable> values, Context context)
            throws IOException, InterruptedException {
        for (BytesWritable value : values) {
            byte[] record = value.copyBytes();
            writer.addRecord(record);
        }
    }

    @Override
    public void cleanup(Context context) throws IOException,
            InterruptedException {
        writer.close();
        // -- At this point we have BAM file on local disk

        // reader iterator here
        // time taken till here = 1h1m run4

        // call SortSam
        String[] sortSamArgv = CmdLineArgs.getSortSamArgv();
        String in1 = outputFile;
        sortSamArgv[0] = "INPUT=" + in1;
        String out1 = getBAMFileName(context, markDupLocalLogDir, "out1");
        sortSamArgv[1] = "OUTPUT=" + out1;

        Stopwatch sortTimer = Stopwatch.createStarted();
        new SortSam().instanceMain(sortSamArgv);
        sortTimer.stop();
        // delete SortSam input
        executeCommand("rm -f " + in1);

        // call MarkDuplicates
        String[] markDupArgv = CmdLineArgs.getMarkDupArgv();
        String in2 = out1;
        markDupArgv[0] = "INPUT=" + in2;
        String out2 = getBAMFileName(context, markDupLocalLogDir, "out2");
        markDupArgv[1] = "OUTPUT=" + out2;
        markDupArgv[4] = "METRICS_FILE=" + markDupLocalLogDir + "/markdup-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + ".log";

        Stopwatch mdupTimer = Stopwatch.createStarted();
        new MarkDuplicates().instanceMain(markDupArgv);
        mdupTimer.stop();

        // delete MarkDup input
        executeCommand("rm -f " + in2);

        // write output on HDFS?
        SAMFileReader outReader = new SAMFileReader(new File(out2));
        SAMFileHeader outHeader = outReader.getFileHeader();
        SAMFileWriter hdfsWriter = createHdfsWriter(context, outHeader);
        for (SAMRecord record : outReader) {
            hdfsWriter.addAlignment(record);
            context.getCounter(ReducerRecords.OUT_RECORDS).increment(1);
        }
        outReader.close();
        hdfsWriter.close();

        // delete all BAM files
        executeCommand("rm -f " + out2);

        LogPrinter.printSecondsElaped(log, "DiskSortSam",
                sortTimer.elapsed(TimeUnit.SECONDS));
        LogPrinter.printSecondsElaped(log, "DiskMarkDuplicates",
                mdupTimer.elapsed(TimeUnit.SECONDS));

    }

    private SAMFileWriter createHdfsWriter(Context context, SAMFileHeader header)
            throws IOException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        String outputDir = conf.get("OutputDir");
        String outputBamName = getBAMFileName(context, outputDir, "mdupout");
        Path path = new Path(outputBamName);
        FSDataOutputStream pipedOutput = fs.create(path);
        SAMFileWriter pipedWriter = new SAMFileWriterFactory().makeBAMWriter(
                header, true, pipedOutput);
        return pipedWriter;
    }

    private String executeCommand(String command) {
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(new String[] { "sh", "-c", command });
            //
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    private String getBAMFileName(Context context, String dir, String suffix) {
        String fileName = "reduce-"
                + context.getTaskAttemptID().getTaskID().getId() + "-"
                + context.getTaskAttemptID().getId() + "-" + suffix + ".bam";
        String name = dir + "/" + fileName;
        return name;
    }

}
