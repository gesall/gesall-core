package bam.utils;

import htsjdk.samtools.util.BlockCompressedStreamConstants;

/**
 * Different constants related to BAM file format.
 */
public class BamConstants {

    public static final int    GZIP_IDENTIFIER_LENGTH = 4;

    public static final byte[] gzipIdentifier         = {
            BlockCompressedStreamConstants.GZIP_ID1,
            (byte) BlockCompressedStreamConstants.GZIP_ID2,
            BlockCompressedStreamConstants.GZIP_CM_DEFLATE,
            BlockCompressedStreamConstants.GZIP_FLG  };

}
