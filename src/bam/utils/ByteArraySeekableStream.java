package bam.utils;

import java.io.IOException;

import htsjdk.samtools.seekablestream.SeekableStream;

public class ByteArraySeekableStream extends SeekableStream {

    private byte[] bytes;
    private int    readPosition;
    private int    totalLength;

    public ByteArraySeekableStream(byte[] bytes) {
        this.bytes = bytes;
        this.readPosition = 0;
        this.totalLength = bytes.length;
    }

    @Override
    public long length() {
        return this.totalLength;
    }

    @Override
    public long position() throws IOException {
        return this.readPosition;
    }

    @Override
    public void seek(long position) throws IOException {
        this.readPosition = (int) position;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (eof()) {
            return -1;
        }
        int bytesToRead = Math
                .min(length, this.totalLength - this.readPosition);
        System.arraycopy(bytes, this.readPosition, buffer, offset, bytesToRead);
        this.readPosition += bytesToRead;
        return bytesToRead;
    }

    @Override
    public void close() throws IOException {
        this.bytes = null;
    }

    @Override
    public boolean eof() throws IOException {
        return (this.readPosition >= this.totalLength);
    }

    @Override
    public String getSource() {
        return "gesall-rad.bam";
    }

    @Override
    public int read() throws IOException {
        if (eof()) {
            return -1;
        }
        int retVal = (int) bytes[this.readPosition];
        this.readPosition += 1;
        return retVal;
    }

}
