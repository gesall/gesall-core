package bam.utils;

/**
 * Utility methods for byte[]
 * 
 */
public class ByteArrayUtils {

    public static int unpackInt16(final byte[] buffer, final int offset) {
        return ((buffer[offset] & 0xFF) | ((buffer[offset + 1] & 0xFF) << 8));
    }

    public static int unpackInt32(final byte[] buffer, final int offset) {
        return ((buffer[offset] & 0xFF) | ((buffer[offset + 1] & 0xFF) << 8) | ((buffer[offset + 2] & 0xFF) << 16) | ((buffer[offset + 3] & 0xFF) << 24));
    }

    public static String unpackUtf8String(byte[] buffer, int offset, int length) {
        byte[] text = new byte[length];
        System.arraycopy(buffer, offset, text, 0, length);
        return new String(text);
    }

}
