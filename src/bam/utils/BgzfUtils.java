package bam.utils;

import htsjdk.samtools.SAMFormatException;
import htsjdk.samtools.util.BlockCompressedStreamConstants;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.CRC32;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class BgzfUtils {

    private final Inflater inflater = new Inflater(true); // GZIP mode
    private final CRC32 crc32 = new CRC32();
    private boolean checkCrcs = true;

    /**
     * Allows the caller to decide whether or not to check CRCs on when
     * uncompressing blocks.
     */
    public void setCheckCrcs(final boolean check) {
        this.checkCrcs = check;
    }

    /**
     * Decompress GZIP-compressed data
     * 
     * @param uncompressedBlock
     *            must be big enough to hold decompressed output.
     * @param compressedBlock
     *            compressed data starting at offset 0
     * @param compressedLength
     *            size of compressed data, possibly less than the size of the
     *            buffer.
     */
    public void unzipBlock(byte[] uncompressedBlock, byte[] compressedBlock, int compressedLength) {
        try {
            ByteBuffer byteBuffer = ByteBuffer.wrap(compressedBlock, 0, compressedLength);
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

            // Validate GZIP header
            if (byteBuffer.get() != BlockCompressedStreamConstants.GZIP_ID1
                    || byteBuffer.get() != (byte) BlockCompressedStreamConstants.GZIP_ID2
                    || byteBuffer.get() != BlockCompressedStreamConstants.GZIP_CM_DEFLATE
                    || byteBuffer.get() != BlockCompressedStreamConstants.GZIP_FLG) {
                throw new SAMFormatException("Invalid GZIP header");
            }
            // Skip MTIME, XFL, OS fields
            byteBuffer.position(byteBuffer.position() + 6);
            if (byteBuffer.getShort() != BlockCompressedStreamConstants.GZIP_XLEN) {
                throw new SAMFormatException("Invalid GZIP header");
            }
            // Skip blocksize subfield intro
            byteBuffer.position(byteBuffer.position() + 4);
            // Read ushort
            final int totalBlockSize = (byteBuffer.getShort() & 0xffff) + 1;
            if (totalBlockSize != compressedLength) {
                throw new SAMFormatException("GZIP blocksize disagreement");
            }

            // Read expected size and CRD from end of GZIP block
            final int deflatedSize = compressedLength - BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH
                    - BlockCompressedStreamConstants.BLOCK_FOOTER_LENGTH;
            byteBuffer.position(byteBuffer.position() + deflatedSize);
            int expectedCrc = byteBuffer.getInt();
            int uncompressedSize = byteBuffer.getInt();
            inflater.reset();

            // Decompress
            inflater.setInput(compressedBlock, BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH, deflatedSize);
            final int inflatedBytes = inflater.inflate(uncompressedBlock, 0, uncompressedSize);
            if (inflatedBytes != uncompressedSize) {
                throw new SAMFormatException("Did not inflate expected amount");
            }

            // Validate CRC if so desired
            if (this.checkCrcs) {
                crc32.reset();
                crc32.update(uncompressedBlock, 0, uncompressedSize);
                final long crc = crc32.getValue();
                if ((int) crc != expectedCrc) {
                    throw new SAMFormatException("CRC mismatch");
                }
            }
        } catch (DataFormatException e) {
            throw new RuntimeException(e);
        }
    }

}
