package bam.utils;

import htsjdk.samtools.util.BlockCompressedStreamConstants;

import java.io.DataInputStream;
import java.io.IOException;

public class BamFileUtils {

    public static byte[] readCompressedBlock(DataInputStream in)
            throws IOException {
        // compressed block array
        byte[] blockBuffer = new byte[BlockCompressedStreamConstants.MAX_COMPRESSED_BLOCK_SIZE];

        // read header
        in.readFully(blockBuffer, 0,
                BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH);

        // read size of block from header
        int blockLength = ByteArrayUtils.unpackInt16(blockBuffer,
                BlockCompressedStreamConstants.BLOCK_LENGTH_OFFSET) + 1;
        if (blockLength < BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH
                || blockLength > blockBuffer.length) {
            throw new IOException("Unexpected compressed block length: "
                    + blockLength);
        }

        // read the rest of block after header
        int remaining = blockLength
                - BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH;
        in.readFully(blockBuffer,
                BlockCompressedStreamConstants.BLOCK_HEADER_LENGTH, remaining);

        // write compressed block to value
        byte[] compressedBlock = new byte[blockLength];
        System.arraycopy(blockBuffer, 0, compressedBlock, 0, blockLength);
        return compressedBlock;
    }

    public static byte[] getUncompressedBlock(byte[] compressedBlock) {
        // get uncompressed block length
        int blockLength = compressedBlock.length;
        int uncompressedLength = ByteArrayUtils.unpackInt32(compressedBlock,
                blockLength - 4);

        // uncompress block
        byte[] uncompressed = new byte[uncompressedLength];
        BgzfUtils gunzip = new BgzfUtils();
        gunzip.unzipBlock(uncompressed, compressedBlock, blockLength);

        return uncompressed;
    }

}
