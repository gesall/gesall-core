# Hadoop Deployment for Gesall Alignment Jar
The first step in the Hadoop Deployment Pipeline is running this alignment jar which takes as input FASTQ Files and outputs multiple BAM Files.
The `clean` and the `markduplicate` jars, operate on these generated BAM Files.

## Pre-processing

Step 1
Preprocess
/srv/data1/users/aroy/VLDB16DATA/fastq

FASTQ files compute-1-10
/state/partition3/aroy/gesall/data/fastq/60

Before running the jar we need to the raw FASTQ Files in a distributed manner across all machines as the process is highly memory intensive.

###TODO

## Hadoop Configuration

Current hadoop configuration for running aligment jar can be found in : `/share/apps/hadoop/etc/hadoop_align_latest`. To setup your own configuration :

###TODO
