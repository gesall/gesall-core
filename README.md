## About

This project contains the different software layers of the Gesall big data platform for genome data analysis.

#### Code Layout
1. Runtime and Storage layers are in `hdfs.*` packages.
2. Data Partitioning schemes (with MapReduce wrappers) are in `program.{alignment|clean|md}.latest` packages.
3. Error Diagnosis programs are in `correctness.*` packages.

## Automated Building
Before starting make a note the path of the base directory where the gesall-core, gesall-htsjdk, gesall-picard and gesall-libs is present.

#### Pre-requisites
To build the gesall-core code base, these things needs to be ensured first.
1. gesall-htsjdk, gesall-picard, gesall-libs folders are present parallel to gesall-core
2. gesall-htsjdk and gesall-picard needs to have been built first! The build process picks up jar files from the dist/ directory of these two modules.
3. Make sure your JAVA_HOME environment variables are setup, so as to use standard java commands (eg: jar, ant etc.)

#### Update hadoop home dir in the build.xml files
In the ant-build/ folder we need to update the path of the haddop home directory if we are building from scratch.
Update the value of the variable `hadoophome` in `build_clean.xml` and `build_md.xml`

#### Run bash script
After going into the gesall-core dir simply run 'build.sh' with this path:

```
$> bash ./build.sh -dir=<path-to-your-base-dir> 
```

#### What it does?
1. Replaces the path of your base directory in the Ant buils files to pickup the dependencies.
2. Uses ant-build/build_clean.xml and ant-build/build_md.xml, to build Runnable jar files for the program.clean.latest and program.md.latest module respectively.
3. Use precompiled java files present in build/build_alignment/ directory to package them into a jar file. 
Note that this jar is not Runnable, it contains compressed pre-compiled java classes.

#### Output format
The dist/ direcotry contains the 3 jar files.
These can now be used with the Hadoop Infrastructure. 
Note that the jar itself does *not* contain any hadoop libraries. It has to be used inside a existing Hadoop Installation.

### Building using Eclipse IDE

##### Import the code
(1.) Import the code from `gesall-core` repository into Eclipse.

(2.) Add `gesall-htsjdk` and `gesall-picard` Eclipse projects to dependencies in `Project->Properties->Java Build Path->Projects`
**OR Alternatively**

(2.) First build the `gesall-htsjdk` and `gesall-picard` individually (using thier respective ant build scripts) and add the appropriate jar files out of the ones that were generated:
Specifically, Add `gesall-htsjdk/dist/htsjdk-xxx.jar` and `gesall-picard/dist/picard-xxx.jar` to the Eclipse Build Path.

(3.) Add all the other external JAR files from `gesall-libs` into `Project->Properties->Java Build Path->Libraries`.

##### Building the jars
In order to build using an IDE, we have to add the Hadoop jars as well. Hadoop jars are NOT provided in `gesall-libs` or other directories.
1. Add approriate Apache Hadoop Jars, from an external source of your choosing. Note the Hadoop version.
2. Use `File->Export->Runnable JAR` option with library handling set to `Extract required libraries into generated JAR`.
3. Coose the Main-Class which should be the entrypoint for the module in the dropdown `Launch Configuration` menu.
4. Repeat Steps 2 and 3 for each module you need to get a runnable jar for, choosing the appropriate Main .java file.
5. This will create a self-contained JAR files.

Note: Apache Hadoop JAR files which you have added, should be of the same version as the deployment Hadoop cluster.

### License

All rights reserved by the authors.