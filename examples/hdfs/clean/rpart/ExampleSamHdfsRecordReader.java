package hdfs.clean.rpart;

import hdfs.bloom.GenomicPositionKey;
import hdfs.partition.range.SamHdfsInputFormat;
import hdfs.utils.BamPathFilter;
import hdfs.utils.FSMethods;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import vintage.program.clean.clean1.CleanMain;

public class ExampleSamHdfsRecordReader extends Configured implements Tool {

    public static class ExampleMapper extends
            Mapper<GenomicPositionKey, Text, NullWritable, NullWritable> {

        static enum MapperRecords {
            INPUT_SAM_RECORDS
        }

        @Override
        public void map(GenomicPositionKey key, Text value, Context context) {
            context.getCounter(MapperRecords.INPUT_SAM_RECORDS).increment(1);
        }

    }

    @Override
    public int run(String[] args) throws Exception {
        // parse arguments
        OptionParser parser = getOptionsParser();
        OptionSet options = null;
        String inputPath = null;
        String outputPath = null;
        try {
            options = parser.parse(args);
            inputPath = (String) options.valueOf("i");
            outputPath = (String) options.valueOf("o");
        } catch (OptionException e) {
            System.out.println(e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
        }

        // Set configuration parameters
        Configuration conf = getConf();
        String aBamFile = FSMethods.getAnyBamFile(conf, inputPath);
        conf.set("BamInput", aBamFile);

        // Set job parameters
        Job job = Job.getInstance(conf);
        job.setJobName("Sam Records Counter");
        job.setJarByClass(CleanMain.class);

        FileSystem fs = FileSystem.get(conf);
        FileStatus[] statuses = fs.listStatus(new Path(inputPath),
                new BamPathFilter(conf));
        for (FileStatus status : statuses) {
            FileInputFormat.addInputPath(job, status.getPath());
        }
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setInputFormatClass(SamHdfsInputFormat.class);
        job.setMapperClass(ExampleMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(0);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static OptionParser getOptionsParser() {
        OptionParser parser = new OptionParser("i:o:h*");
        parser.accepts("i", "Input BAM directory on HDFS").withRequiredArg()
                .required().ofType(String.class);
        parser.accepts("o", "Output directory on HDFS").withRequiredArg().required()
        .ofType(String.class);
        parser.accepts("h", "Displays this help message").forHelp();
        return parser;
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new ExampleSamHdfsRecordReader(), args);
        System.exit(res);
    }


}
