package hdfs.clean.bloom;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.io.output.ByteArrayOutputStream;

import hdfs.bloom.GenomicPositionKey;

import com.google.common.hash.BloomFilter;

public class ExampleBloomFilter {

    public static void main(String[] args) throws Exception {

        int expectedInsertions = 15 * 1000000;
        BloomFilter<GenomicPositionKey> bf1 = BloomFilter.create(
                new GenomicPositionKey(), expectedInsertions);
        
        bf1.put(new GenomicPositionKey("chr1", 100));
        bf1.put(new GenomicPositionKey("chr2", 100));
        
        BloomFilter<GenomicPositionKey> bf2 = BloomFilter.create(
                new GenomicPositionKey(), expectedInsertions);
        bf2.put(new GenomicPositionKey("chr1", 100));
        bf2.put(new GenomicPositionKey("chr3", 100));

        System.out.println("BF1");
        System.out.println(bf1.mightContain(new GenomicPositionKey("chr1", 100)));
        System.out.println(bf1.mightContain(new GenomicPositionKey("chr3", 100)));

        System.out.println("BF2");
        System.out.println(bf2.mightContain(new GenomicPositionKey("chr3", 100)));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bf1.writeTo(out);
        
        InputStream in = new ByteArrayInputStream(out.toByteArray());
        
        BloomFilter<GenomicPositionKey> bf3 = BloomFilter.readFrom(in, new GenomicPositionKey());
        System.out.println("BF3");
        System.out.println(bf3.mightContain(new GenomicPositionKey("chr1", 100)));
        System.out.println(bf3.mightContain(new GenomicPositionKey("chr3", 400)));

        bf3.putAll(bf2);
        System.out.println("BF3");
        System.out.println(bf3.mightContain(new GenomicPositionKey("chr1", 100)));
        System.out.println(bf3.mightContain(new GenomicPositionKey("chr3", 100)));

    }

}
